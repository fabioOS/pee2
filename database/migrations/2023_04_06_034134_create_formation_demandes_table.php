<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormationDemandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formation_demandes', function (Blueprint $table) {
            /**
             * _____________table attributes_________________
             * etudiant_id
             * formation_id
             * statut=default(0)--1,2
             */
            $table->bigIncrements('id');
            $table->bigInteger('etudiant_id')->unsigned()->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->bigInteger('formation_id')->unsigned()->constrained('formations')->onUpdate('cascade')->onDelete('restrict');
            $table->tinyInteger('statut')->default(0);//0=non traité, 1=accepté, 2=refusé
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formation_demandes');
    }
}
