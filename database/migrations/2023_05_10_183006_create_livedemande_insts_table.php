<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivedemandeInstsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('livedemande_insts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom',250);
            $table->string('competence',200);
            $table->text('encadrements');
            $table->string('email',100);
            $table->string('contact',30);
            $table->text('commentaire')->nullable();
            $table->string('statut',1)->default('0');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('livedemande_insts');
    }
}
