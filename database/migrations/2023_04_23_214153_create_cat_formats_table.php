<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatFormatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_formats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('catformation_id')->unsigned()->constrained('catformations')->onUpdate('cascade')->onDelete('restrict');
            $table->bigInteger('formation_id')->unsigned()->constrained('formations')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_formats');
    }
}
