<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtudiantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etudiants', function (Blueprint $table) {
            $table->bigIncrements('id');
            //civilite/niveau/formation/datenaiss/lieunaiss/nationalite/adresse/ville
            $table->bigInteger('user_id')->unsigned()->constrained('users')->onUpdate('cascade')->onDelete('restrict');
            $table->string('civilite',20)->nullable();
            $table->string('niveau',20)->nullable();
            $table->text('formation')->nullable();
            $table->date('datenaiss')->nullable();
            $table->text('lieunaiss')->nullable();
            $table->string('nationalite',50)->nullable();
            $table->text('adresse')->nullable();
            $table->string('ville',100)->nullable();
            $table->string('contact1',20);
            $table->string('contact2',20)->nullable();
            $table->string('etablissement')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etudiants');
    }
}
