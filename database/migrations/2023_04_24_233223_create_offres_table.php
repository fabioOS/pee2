<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offres', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('slug');
            $table->string('poste');
            $table->integer('type');
            $table->integer('cat_id');
            $table->longText('metier')->nullable();
            $table->string('niveau')->nullable();
            $table->string('experience')->nullable();
            $table->string('lieu')->nullable();
            $table->date('datepublie');
            $table->date('datelimite');
            $table->longText('description');
            $table->tinyInteger('statut')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offres');
    }
}
