<!DOCTYPE html>
<html lang="fr">
	<head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<title>PEE</title>

		<!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/fronts/img/favicon.png')}}">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="{{asset('assets/fronts/css/bootstrap.min.css')}}">

		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="{{asset('assets/fronts/plugins/fontawesome/css/fontawesome.min.css')}}">
		<link rel="stylesheet" href="{{asset('assets/fronts/plugins/fontawesome/css/all.min.css')}}">

		<!-- Main CSS -->
		<link rel="stylesheet" href="{{asset('assets/fronts/css/style.css')}}">
	</head>
    <body class="error-page">

		<!-- Main Wrapper -->
        <div class="main-wrapper">

			<div class="error-box">
				<div class="error-logo">
					<a href="./">
						<img src="{{asset('assets/fronts/img/logo.png')}}" style="width:380px;" class="img-fluid-" alt="Logo">
					</a>
				</div>
				<div class="error-box-img">
					<img src="{{asset('assets/fronts/img/error-02.png')}}" alt="" class="img-fluid" >
				</div>
				<h3 class="h2 mb-3"> Oops! Interdiction d'accès</h3>
				<p class="h4 font-weight-normal">Vous ne disposez pas des droits d'accès à cette page !</p>
				<a href="./" class="btn btn-primary">Retour à l'accueil</a>
			</div>

        </div>
		<!-- /Main Wrapper -->

		<!-- jQuery -->
		<script src="{{asset('assets/fronts/js/jquery-3.6.0.min.js')}}"></script>

		<!-- Bootstrap Core JS -->
		<script src="{{asset('assets/fronts/js/bootstrap.bundle.min.js')}}"></script>

		<!-- Custom JS -->
		<script src="{{asset('assets/fronts/js/script.js')}}"></script>

	</body>
</html>
