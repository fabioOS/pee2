@extends('web.main')
@section('title', "PEE | Formations")

@section('css')
@endsection

@section('js')
<script>
    //document ready
    $(document).ready(function() {
        $( ".filterObj" ).prop( "checked", false );
        $('#f_search .filterObj').each(function(index, element){
            $(this).click(function() {
                var data = $('#f_search').serialize();
                //console.log(serial);
                var url = $('#f_search').attr('url');
                var container = $('#f_container');
                return Query_GET(url,data,container);
            });
        });

        //
        function Query_GET(url,data,container) {
            $.ajax({
                type: "GET",
                url: url,
                data: data,
                beforeSend: function() {
                    //container.html('<div class="text-center"><div class="spinner-border text-primary" role="status"><span class="sr-only">Chargement...</span></div></div>');
                    container.html('<div class="text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
                },
                success: function(data) {
                    container.html(data);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
        ////
        $('.btnAction').each(function(index, element){
            $(this).click(function() {
                $(this).hide();
                var container = $('.feedback:eq('+index+')');
                container.delay( 800 ).html('<div class="text-center mb-4"><i class="fa fa-spinner fa-spin fa-3x fa-fw text-secondary"></i></div>');
                var data = '';
                var url  = $(this).attr('ref');
                console.log(url);
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    dataType: "json",
                    cache: false,
                    beforeSend: function() {
                        container.html('<div class="text-center m-6"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
                    },
                    success: function(data) {
                        if(data.status == 'error')
                            container.html('<div class="text-center m-6"><i class="fa fa-times-circle fa-3x text-danger"></i> <span class="mb-4">'+data.message+'</span> </div>');
                        else
                            container.html('<div class="text-center m-6"><i class="fa fa-check-circle fa-3x text-success"></i> </div>');
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            });
        });
    });

</script>
@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">Formations</li>
                            <li class="breadcrumb-item active" aria-current="page">Toutes les Formations</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Course -->
<section class="course-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-9" id="f_container">

                @if ($grid==1)
                    @include('web.formations.data_grid')
                @else
                    @include('web.formations.data_list')
                @endif

            </div>
            <div class="col-lg-3 theiaStickySidebar">
                <form action="get" action="javascript:void(0)" url="{{route("w.formation.search")}}" id="f_search">
                    <input type="hidden" name="grid" value="{{$grid}}">
                    <div class="filter-clear">
                        <div class="clear-filter d-flex align-items-center">
                            <h4><i class="feather-filter"></i>Filtrer</h4>
                            <div class="clear-text">
                                <p>{{-- Effacer --}}</p>
                            </div>
                        </div>
                        <!-- Search Filter -->
                        <div class="card search-filter categories-filter-blk">
                            <div class="card-body">
                                <div class="filter-widget mb-0">
                                    <div class="categories-head d-flex align-items-center">
                                        <h4>Par Catégories</h4>
                                        <i class="fas fa-angle-down"></i>
                                    </div>
                                    @foreach ($categs as $cag )
                                        <div>
                                            <label class="custom_check">
                                                <input class="filterObj" type="checkbox" name="by_cat" value="{{$cag->id}}">
                                                <span class="checkmark"></span>
                                                {{$cag->libelle}}
                                                ({{DB::table('cat_formats')->where('catformation_id',$cag->id)->join('formations','formations.id','=','cat_formats.formation_id')->where('formations.statut',1)->where('formations.datecloture', '>=', date('Y-m-d'))->count()}})
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- /Search Filter -->

                        <!-- Search Filter -->
                        <div class="card search-filter">
                            <div class="card-body">
                                <div class="filter-widget mb-0">
                                    <div class="categories-head d-flex align-items-center">
                                        <h4>Par Formateurs</h4>
                                        <i class="fas fa-angle-down"></i>
                                    </div>
                                    @foreach ($profs as $prof)
                                        <div>
                                            <label class="custom_check">
                                                <input class="filterObj" type="checkbox" name="by_instruct" value="{{$prof->id}}">
                                                <span class="checkmark"></span>
                                                {{$prof->name}}
                                                ({{DB::table('formations')->where('professeur_id',$prof->id)->where('statut',1)->where('datecloture', '>=', date('Y-m-d'))->count()}})
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- /Search Filter -->

                        <!-- Search Filter -->
                        <div class="card search-filter ">
                            <div class="card-body">
                                <div class="filter-widget mb-0">
                                    <div class="categories-head d-flex align-items-center">
                                        <h4>Par Prix</h4>
                                        <i class="fas fa-angle-down"></i>
                                    </div>
                                    <div>
                                        <label class="custom_check custom_one">
                                            <input class="filterObj" type="radio" name="by_cost" value="all" >
                                            <span class="checkmark"></span> Tout ({{DB::table('formations')->where('statut',1)->where('datecloture', '>=', date('Y-m-d'))->count()}})
                                        </label>
                                    </div>
                                    <div>
                                        <label class="custom_check custom_one">
                                            <input class="filterObj" type="radio" name="by_cost" value="free" >
                                            <span class="checkmark"></span>  Gratuit ({{DB::table('formations')->where('statut',1)->where('datecloture', '>=', date('Y-m-d'))->where('prix',0)->count()}})
                                        </label>
                                    </div>
                                    <div>
                                        <label class="custom_check custom_one mb-0">
                                            <input class="filterObj" type="radio" name="by_cost" value="no-free">
                                            <span class="checkmark"></span>  Payant ({{DB::table('formations')->where('statut',1)->where('datecloture', '>=', date('Y-m-d'))->where('prix','>',0)->count()}})
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Search Filter -->

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- /Course -->


@endsection
