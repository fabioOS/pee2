<!-- Header -->
<header class="header {{!Route::is('web') ? 'header-page' : ''}}">
    <div class="header-fixed">
        <nav class="navbar navbar-expand-lg header-nav scroll-sticky">
            <div class="container">
                <div class="navbar-header">
                    <a id="mobile_btn" href="javascript:void(0);">
                        <span class="bar-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </a>
                    <a href="{{route('web')}}" class="navbar-brand logo">
                        <img src="{{asset('assets/fronts/img/logo.png')}}" class="img-fluid" alt="Logo">
                    </a>
                </div>
                <div class="main-menu-wrapper">
                    <div class="menu-header">
                        <a href="{{route('web')}}" class="menu-logo">
                            <img src="{{asset('assets/fronts/img/logo.png')}}" class="img-fluid" alt="Logo">
                        </a>
                        <a id="menu_close" class="menu-close" href="javascript:void(0);">
                            <i class="fas fa-times"></i>
                        </a>
                    </div>
                    <ul class="main-nav">
                        <li class="has-submenu {{Route::is('web') ? 'active' : ''}}">
                            <a class="" href="{{route('web')}}">Accueil </a>
                        </li>
                        <li class="has-submenu {{Route::is('w.formation') ? 'active' : ''}}">
                            <a href="{{route('w.formation')}}">Formations </a>
                        </li>
                        <li class="has-submenu {{Route::is('w.offre') ||Route::is('w.offreItem') || Route::is('w.offre.type') ? 'active' : ''}}">
                            <a href="{{route('w.offre.type','emplois')}}">Offres <i class="fas fa-chevron-down"></i></a>
                            <ul class="submenu first-submenu">
                                <li><a href="{{route('w.offre.type','emplois')}}">Emplois</a></li>
                                <li><a href="{{route('w.offre.type','stages')}}">Stages</a></li>
                                <li><a href="{{route('w.offre.type','financement')}}">Financement</a></li>
                            </ul>
                        </li>
                        <li class="has-submenu {{Route::is('w.promo') ? 'active' : ''}}">
                            <a href="{{route('w.promo')}}">Nos réalisations </a>
                        </li>
                        <li class="has-submenu {{Route::is('w.news') || Route::is('w.newsItem') ? 'active' : ''}}">
                            <a href="{{route('w.news')}}">Actualités </a>
                        </li>
                        <li class="has-submenu {{Route::is('w.contact') ? 'active' : ''}}">
                            <a href="{{route('w.contact')}}">Contact </a>
                        </li>
                        @if (Auth::check())
                            <li class="login-link">
                                <a href="javascript:void();" class="dropdown-toggle">
                                    <span class="user-img">
                                        @if (Auth::user()->img != '')
                                            <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @else
                                            <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @endif
                                        <span class="status online">Bienvenue {{substr(ucfirst(Auth::user()->name),0,7)}}...</span>
                                        <h6></h6>
                                    </span>
                                </a>
                                <ul class="submenu first-submenu">
                                    <a class="p-1 mt-2" href="{{route('ens.home')}}"><i class="feather-home me-1"></i> Tableau de bord</a>
                                    <a class="p-1 m-1" href="{{route('ens.profiles')}}"><i class="fa fa-user-cog me-1"></i> Profil </a>
                                    <a class="p-1 mb-2" href="{{route('quicklogout')}}"><i class="feather-log-out me-1"></i> Déconnexion</a>
                                </ul>
                            </li>
                        @else
                            <li class="login-link">
                                <a href="{{route('login')}}">Connexion</a>
                            </li>
                        @endif
                    </ul>
                </div>


                <ul class="nav header-navbar-rht">
                    @if (Auth::check())
                    @php
                        $total_notif=0;
                        $demandF = App\Notif::where('user_id',Auth::user()->id)->where('lu',0)->get();
                        $total_notif += count($demandF);
                    @endphp
                    <li class="nav-item noti-nav">
                        <a href="javascript:void();" class="dropdown-toggle" data-bs-toggle="dropdown">
                            <img src="{{asset('assets/fronts/img/icon/notification.svg')}}" alt="notif">
                            @if($total_notif>0)<span class="badge badge-xs badge-danger">{{$total_notif}}</span>@endif
                        </a>
                        <div class="notifications dropdown-menu dropdown-menu-right">
                            <div class="noti-content">
                                <ul class="notification-list">
                                    @if($demandF->count()>0)
                                    @foreach ($demandF as $df)
                                        <li class="notification-message">
                                            <div class="media d-flex">
                                                <div>
                                                    <a href="{{$df->lien}}" class="avatar">
                                                        <em class="icon-user-follow fa-2x text-danger"></em>
                                                    </a>
                                                </div>
                                                <div class="media-body">
                                                    <h6><a href="{{$df->lien}}"><span>{{$df->message}}</span> </a></h6>
                                                    <a href="{{$df->lien}}" class="btn btn-accept">Voir les détails</a>
                                                    <!-- date calcul -->
                                                    @php
                                                        $date = $df->created_at;
                                                        $now = date('Y-m-d H:i:s');
                                                        $diff = abs(strtotime($now) - strtotime($date));
                                                        $years = floor($diff / (365*60*60*24));
                                                        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                                        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                                                        $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
                                                        $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
                                                        $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));
                                                    @endphp
                                                    {{-- <p>{{$df->created_at->diffForHumans(date('Y-m-d'))}}</p> --}}
                                                    <p>Il y a {{$days}} jours {{$hours}} heures {{$minutes}} minutes</p>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                    @else
                                        <li class="notification-message">
                                            <div class="media d-flex">
                                                <div class="media-body">
                                                    Vous n'avez aucune notification
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </li>
                        <li class="nav-item user-nav">
                            <a href="javascript:void();" class="dropdown-toggle" data-bs-toggle="dropdown">
                                <span class="user-img">
                                    @if (Auth::user()->img != '')
                                            <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @else
                                            <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @endif
                                    <span class="status online"></span>
                                </span>
                            </a>
                            <div class="users dropdown-menu dropdown-menu-right" data-popper-placement="bottom-end" >
                                <div class="user-header">
                                    <div class="avatar avatar-sm">
                                        @if (Auth::user()->img != '')
                                            <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @else
                                            <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                        @endif
                                    </div>
                                    <div class="user-text">
                                        <h6>{{ucwords(Auth::user()->name)}}</h6>
                                        <p class="text-muted mb-0">{{Auth::user()->roles->libelle}}</p>
                                    </div>
                                </div>
                                @if (Auth::user()->role_id == 3)
                                    <a class="dropdown-item" href="{{route('ens.home')}}"><i class="feather-home me-1"></i> Tableau de bord</a>
                                    <a class="dropdown-item" href="{{route('ens.profiles')}}"><i class="feather-user me-1"></i> Profil </a>
                                @elseif (Auth::user()->role_id == 4)
                                    <a class="dropdown-item" href="{{route('etu.home')}}"><i class="feather-home me-1"></i> Tableau de bord</a>
                                    <a class="dropdown-item" href="{{route('etu.profiles')}}"><i class="feather-user me-1"></i> Profil </a>
                                @else
                                    <a class="dropdown-item" href="{{route('home')}}"><i class="feather-home me-1"></i> Tableau de bord</a>
                                    <a class="dropdown-item" href="{{route('profiles')}}"><i class="feather-user me-1"></i> Profil </a>
                                @endif
                                <a class="dropdown-item" href="{{route('quicklogout')}}"><i class="feather-log-out me-1"></i> Déconnexion</a>
                            </div>
                        </li>

                    @else
                        <li class="nav-item">
                            <a class="nav-link header-sign" href="{{route('login')}}">Connexion</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link header-login" href="{{route('register')}}">Inscription</a>
                        </li>
                    @endif


                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- /Header -->
