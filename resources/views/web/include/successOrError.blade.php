@if(Session::has('success'))
    <div class="container">
        <div class="alert alert-success alert-dismissible fade show text-center mx-auto" role="alert">
            <strong>Succès !</strong> {!!Session::get('success')!!}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    </div>

@endif

@if(Session::has('error'))
<div class="container">
    <div class="alert alert-danger alert-dismissible fade show text-center mx-auto" role="alert">
        <strong>Alerte !</strong> {!!Session::get('error')!!}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</div>
@endif

@if($errors->any())
<div class="container">
    <div class="alert alert-danger alert-dismissible fade show text-center mx-auto" role="alert">
        <strong>Alerte !</strong> <br>
            @foreach($errors->all() as $errorr)
                {!! $errorr !!}<br/>
            @endforeach
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</div>
@endif

