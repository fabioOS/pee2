@extends('web.main')
@section('title', "PEE | Demande")

@section('css')
@endsection

@section('js')
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        var onloadCallback = function() {
            grecaptcha.render('reCAPTCHA-Container', {
                'sitekey' : "{{ env('CAPTCHA_KEY') }}",
                'theme' : 'light'
            });
        };
        function formatNumber(input) {
            input.value = input.value.replace(/[^\d]/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "");
        }
    </script>

@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">
                                Demande de partenariat
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<section class="course-content ">

    @include('web.include.successOrError')

    <div class="container">
        <div class="row">
            <div class="col-lg-5 text-center mx-auto">
                <div class="title-sec">
                    <h2>Envoyer-nous votre demande de partenariat </h2>
                    <p>Veuillez renseigner correctement le formulaire, puis le soumettre.</p>
                </div>
            </div>
        </div>

        <!-- Plan Type -->
        <div class="row">
            <!-- <div class="col-lg-1"></div> -->
            <div class="col-lg-2 col-md-3 plan-box- p-4">
            </div>
            <div class="col-lg-8">
                <div class="plan-box loginbox">
                    <form action="{{route('w.demande.send')}}" method="post" class="col-md-12">
                        @csrf
                        <input type="hidden" name="type" value="{{$type}}">
                        <div class="col-lg-12 col-md-12">
                            <h4>Formulaire de demande de partenariat</h4>
                            <div class="news-letter border border-top border-light mb-4"></div>
                                <p>Veuillez renseigner le formulaire puis l'envoyer. </p>
                                <p text-muted>Les champs marqués ( <span class="text-danger">*</span> ) sont obligatoires</p> <br>
                                <!-- Form contact -->
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="nom">Nom et Prénom (<span class="text-danger">*</span>)</label>
                                <input required type="text" min="6" max="250" name="nom" id="nom" class="form-control" placeholder="Nom et Prénom">
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="entreprise">Nom de l'entreprise (<span class="text-danger">*</span>)</label>
                                <input required type="text" min="2" max="150" name="entreprise" id="entreprise" class="form-control" placeholder="Entreprise">
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="domaine">Domaine d'activité (<span class="text-danger">*</span>)</label>
                                <input required type="text" max="100" name="domaine" id="domaine" class="form-control" placeholder="Entreprise">
                            </div>

                            <div class="form-group">
                                <label for="email">Email (<span class="text-danger">*</span>)</label>
                                <input required type="email" min="6" max="100" name="email" id="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="email">Contact (<span class="text-danger">*</span>)</label>
                                <input required type="text" min="6" max="30" name="contact" id="contact" class="form-control" placeholder="Contact" onkeyup="formatNumber(this)">
                            </div>
                            <div class="form-group">
                                <label for="commentaire">Commentaire </label>
                                <textarea class="form-control" name="commentaire" id="commentaire" rows="5" placeholder="Ajouter un commentaire"></textarea>
                            </div>
                            <div class="col-md-12" id="reCAPTCHA-Container" style="margin-top: 10px;">Chargement de Recaptcha...</div>
                            <div id="reCAPTCHA-error" class="text-red">&nbsp;</div>

                            <div class="form-group col-md-12" style="text-align:left;">
                                <button type="submit" class="btn btn-primary">Envoyer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- /Plan Type -->

    </div>
</section>

@endsection
