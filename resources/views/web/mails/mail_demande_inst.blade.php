<html>
<head>
    <style>
        @media  only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media  only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>
<body style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #ffffff; color: #74787E; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;">

<table cellpadding="0" width="100%" cellspacing="0" border="0" id="backgroundTable" class="bgBody">
    <tbody><tr>
        <td>
            <table cellpadding="0" width="620" class="container" align="center" cellspacing="0" border="0">
                <tbody><tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                            <tbody><tr>
                                <td class="movableContentContainer bgItem">

                                    <div class="movableContent">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                            <tbody><tr height="40">
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="200" valign="top">&nbsp;</td>
                                                <td width="200" valign="top" align="center">
                                                    <div class="contentEditableContainer contentImageEditable">
                                                        <div class="contentEditable" align="center">
                                                            <img src="{{asset('assets/fronts/img/logo.png')}}" height="70" alt="Logo" data-default="placeholder">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="200" valign="top">&nbsp;</td>
                                            </tr>
                                            <tr height="25">
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                                <td width="200">&nbsp;</td>
                                            </tr>
                                            </tbody></table>
                                    </div>

                                    <div class="movableContent">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container" style="font-size: 20px">
                                            <tbody>
                                            <tr>
                                                <td width="10">&nbsp;</td>
                                                <td width="400" align="center">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align="left">
                                                        <h5>{!!$text!!}</h5>
                                                        <table cellpadding="4" cellspacing="4"  style="max-width: 100%;">
                                                            <tr style="margin-bottom: 4px;">
                                                                <td style="border:1px solid #ecece9; width:200px;$" align="left" class="textContent">
                                                                    <p><strong>Nom et prénom</strong> </p>
                                                                </td>
                                                                <td style="border:1px solid #ecece9; width:300px;" align="left" class="textContent">
                                                                    <p>{{$nom}}</p>
                                                                </td>
                                                            </tr>
                                                            <tr style="margin-bottom: 4px;">
                                                                <td style="border:1px solid #ecece9; width:200px;$" align="left" class="textContent">
                                                                    <p><strong>Compétence</strong> </p>
                                                                </td>
                                                                <td style="border:1px solid #ecece9; width:300px;" align="left" class="textContent">
                                                                    <p>{{$competence}}</p>
                                                                </td>
                                                            </tr>
                                                            <tr style="margin-bottom: 4px;">
                                                                <td style="border:1px solid #ecece9; width:200px;$" align="left" class="textContent">
                                                                    <p><strong>Encadrement envisagé</strong> </p>
                                                                </td>
                                                                <td style="border:1px solid #ecece9; width:300px;" align="left" class="textContent">
                                                                    @php
                                                                        if(count($encadrement)>0) {
                                                                            foreach ($encadrement as $key => $value) {
                                                                                if($value != null && $value != '') {
                                                                                    $cat = DB::table('catformations')->selectRaw('libelle')->where('id', $value)->pluck('libelle')->first();
                                                                                    if($cat != null) echo "<p>$cat</p>";
                                                                                }
                                                                            }
                                                                        }
                                                                    @endphp
                                                                </td>
                                                            </tr>
                                                            <tr style="margin-bottom: 4px;">
                                                                <td style="border:1px solid #ecece9; width:200px;$" align="left" class="textContent">
                                                                    <p><strong>Email</strong> </p>
                                                                </td>
                                                                <td style="border:1px solid #ecece9; width:300px;" align="left" class="textContent">
                                                                    <p>{{$email}}</p>
                                                                </td>
                                                            </tr>
                                                            <tr style="margin-bottom: 4px;">
                                                                <td style="border:1px solid #ecece9; width:200px;$" align="left" class="textContent">
                                                                    <p><strong>Contact</strong> </p>
                                                                </td>
                                                                <td style="border:1px solid #ecece9; width:300px;" align="left" class="textContent">
                                                                    <p>{{$contact}}</p>
                                                                </td>
                                                            </tr>
                                                            <tr style="margin-bottom: 4px;">
                                                                <td style="border:1px solid #ecece9; width:200px;$" align="left" class="textContent">
                                                                    <p><strong>Commentaire</strong> </p>
                                                                </td>
                                                                <td style="border:1px solid #ecece9; width:300px;" align="left" class="textContent">
                                                                    <p>{{$commentaire}}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <p style="font-size:10px"> Envoyé le {{date('d/m/Y h:i:s')}} </p>
                                                        <br>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="10">&nbsp;</td>
                                            </tr>
                                            </tbody></table>
                                    </div>
                                    <div class="movableContent">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                            <tbody><tr>
                                                <td width="100%" colspan="2" style="padding-top:65px;">
                                                    <hr style="height:1px;border:none;color:#333;background-color:#ddd;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="60%" height="70" valign="middle" style="padding-bottom:20px;">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable" align="left">
                                                            <span style="font-size:11px;color:#555;font-family:Helvetica, Arial, sans-serif;line-height:200%;">
                                                                © {{date('Y')}} {{env('APP_NAME')}}. Tous droits réservés. <br>
                                                                <a target="_blank" href="{{route('web')}}" style="color:#555;">Aller au site</a>
                                                            </span>
                                                            <br>
                                                            <span style="font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;"></span>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody></table>
                                    </div>


                                </td>
                            </tr>
                            </tbody></table>




                    </td></tr></tbody></table>

        </td>
    </tr>
    </tbody></table>



</body></html>
