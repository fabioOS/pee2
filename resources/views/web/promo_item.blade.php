@extends('web.main')
@section('title', $promo->titre)

@section('css')
<style>
    .blog-image{
        height: 300px;
    }
</style>
@endsection

@section('js')
@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item"><a href="{{route('w.promo')}}">Promotions</a></li>
                            <li class="breadcrumb-item" aria-current="page">{{$promo->titre}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<section class="course-content">

    @include('web.include.successOrError')

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <!-- Blog Post -->
                <div class="blog">

                    @if($promo->img)
                        <div class="blog-image">
                            <a href="#"><img class="img-fluid" src="{{asset('promos/'.$promo->img)}}" alt="{{$promo->titre}}"></a>
                        </div>
                    @endif


                    <div class="blog-info clearfix">
                        <div class="post-left">
                            <ul>
                                @if($promo->auteur)
                                <li>
                                    <div class="post-author">
                                        <a href="#"><i class="fa fa-user m-r-5"></i> <span> {{$promo->auteur}}</span></a>
                                    </div>
                                </li>@endif
                                <li><img class="img-fluid" src="{{asset('assets/fronts/img/icon/icon-22.svg')}}" alt="">{{date('d/m/Y',strtotime($promo->date))}}</li>
                            </ul>
                        </div>
                    </div>
                    <h3 class="blog-title"><a href="#">{{$promo->titre}}</a></h3>
                    <div class="blog-content">
                        {!! $promo->description !!}
                    </div>
                </div>
                <!-- /Blog Post -->

            </div>

        </div>
    </div>
</section>

@endsection
