@extends('web.main')
@section('title', "PEE | Pôle de l'Etudiant Entrepreneur")

@section('css')
@endsection

@section('js')
@endsection

@section('content')
<!-- Home Banner -->
<section class="home-slide d-flex align-items-center">

    @include('web.include.successOrError')

    <div class="container">

        <div class="row ">
            <div class="col-md-7">
                <div class="home-slide-face aos" data-aos="fade-up">
                    <div class="home-slide-text ">
                        <h5>Pôle de l'Etudiant Entrepreneur</h5>
                        <h1>Formations Coaching-Incubation Éducation Financière</h1>
                        <!-- <p >La plateforme qui vous offre des formations adéquates à votre insertion proffessionnelle</p> -->
                    </div>
                    <div class="banner-content">
                        <form class="form" action="{{route('w.search')}}" method="GET">
                            <div class="form-inner">
                                <div class="input-group">
                                    <i class="fa-solid fa-magnifying-glass search-icon"></i>
                                    <input type="text" name="q" required class="form-control" placeholder="Effectuer une recherche, formations, offres, etc">
                                    <span class="drop-detail">
                                        <select name="type" class="form-select select" required>
                                            <option value="0">Rechercher dans ...</option>
                                            <option value="1">Formations</option>
                                            <option value="2">Offres d'emploi</option>
                                            <option value="3">Offres de stage</option>
                                            <option value="4">Offres de financement</option>
                                            <option value="5">Actualités</option>
                                        </select>
                                    </span>
                                    <button class="btn btn-primary sub-btn" type="submit"><i class="fas fa-arrow-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="trust-user">
                        <p>
                            La plateforme qui vous offre des formations adéquates à votre insertion professionnelle <br>
                            Plus de 2K étudiants-entrepreneurs nous font confiance<br>à travers la Côte d'Ivoire
                        </p>
                        {{-- <div class="trust-rating d-flex align-items-center">
                            <div class="rate-head">
                                <h2><span>500</span>+</h2>
                            </div>
                            <div class="rating d-flex align-items-center">
                                <h2 class="d-inline-block average-rating">4.4</h2>
                                <i class="fas fa-star filled"></i>
                                <i class="fas fa-star filled"></i>
                                <i class="fas fa-star filled"></i>
                                <i class="fas fa-star filled"></i>
                                <i class="fas fa-star filled"></i>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="col-md-5 d-flex align-items-center">
                <div class="girl-slide-img aos" data-aos="fade-up">
                    <img src="{{asset('assets/fronts/img/object.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Home Banner -->
<section class="section student-course">
    <div class="container">
        <div class="course-widget">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="course-full-width">
                        <div class="blur-border course-radius align-items-center aos" data-aos="fade-up">
                            <div class="online-course d-flex align-items-center">
                                <div class="course-img">
                                    <img src="{{asset('assets/fronts/img/pencil-icon.svg')}}" alt="">
                                </div>
                                <div class="course-inner-content">
                                    <h4><span>{{$dt['form']}}</span></h4>
                                    <p>Formations</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex">
                    <div class="course-full-width">
                        <div class="blur-border course-radius aos" data-aos="fade-up">
                            <div class="online-course d-flex align-items-center">
                                <div class="course-img">
                                    <img src="{{asset('assets/fronts/img/cources-icon.svg')}}" alt="">
                                </div>
                                <div class="course-inner-content">
                                    <h4><span>{{$dt['ins']}}</span></h4>
                                    <p>Formateurs</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-lg-3 col-md-6 d-flex">
                    <div class="course-full-width">
                        <div class="blur-border course-radius aos" data-aos="fade-up">
                            <div class="online-course d-flex align-items-center">
                                <div class="course-img">
                                    <img src="{{asset('assets/fronts/img/certificate-icon.svg')}}" alt="">
                                </div>
                                <div class="course-inner-content">
                                    <h4><span>{{$dt['cert']}}</span></h4>
                                    <p>Ceritificats</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-lg-4 col-md-6 d-flex">
                    <div class="course-full-width">
                        <div class="blur-border course-radius aos" data-aos="fade-up">
                            <div class="online-course d-flex align-items-center">
                                <div class="course-img">
                                    <img src="{{asset('assets/fronts/img/gratuate-icon.svg')}}" alt="">
                                </div>
                                <div class="course-inner-content">
                                    <h4><span>{{$dt['etu']}}</span></h4>
                                    <p>Étudients inscrits</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Home Banner -->

<!-- Top Categories -->
<section class="section how-it-works">
    <div class="container">
        <div class="section-header aos" data-aos="fade-up">
            <div class="section-sub-head">
                <span>&nbsp;</span>
                <h3>Nos Catégories de formations</h3>
            </div>
            {{-- <div class="all-btn all-category d-flex align-items-center">
                <a href="#" class="btn btn-primary">Toutes les Catégories</a>
            </div> --}}
        </div>
        <div class="section-text aos" data-aos="fade-up">
            {{-- <p>Nos catégories de formations les plus demandées.</p> --}}
        </div>
        <div class="owl-carousel instructors-course owl-theme aos" data-aos="fade-up" style="margin-top: 0; padding-top:0;">
        {{-- <div class="owl-carousel mentoring-course owl-theme aos" data-aos="fade-up"> --}}
            @foreach ($catformations as $cf)
                <div class="instructors-widget">
                    <div class="instructors-img ">
                        <a href="instructor-list.html">
                            <img class="img-fluid" alt="{{$cf->libelle}}" src="{{asset($cf->image)}}">
                        </a>
                    </div>
                    <div class="instructors-content text-center">
                        <h5><a>{{$cf->libelle}}</a></h5>
                        {{-- <p>Web Developer</p>
                        <div class="student-count d-flex justify-content-center">
                            <i class="fa-solid fa-user-group"></i>
                            <span>50 Students</span>
                        </div> --}}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!-- /Top Categories -->

<!-- Feature Course -->
<section class="section new-course">
    <div class="container">
        <div class="section-header aos" data-aos="fade-up">
            <div class="section-sub-head">
                <span>Nouveauté</span>
                <h2>Formations en vedette</h2>
            </div>
            <div class="all-btn all-category d-flex align-items-center">
                <a href="{{route('w.formation')}}" class="btn btn-primary">Toutes les Formations</a>
            </div>
        </div>
        <div class="section-text aos" data-aos="fade-up">
            <p class="mb-0"></p>
        </div>
        <div class="course-feature">
            <div class="row">
                @foreach ($formations as $item)
                    <div class="col-lg-4 col-md-6 d-flex">
                        <div class="course-box d-flex aos" data-aos="fade-up">
                            <div class="product">
                                <div class="product-img">
                                    <a href="{{route('w.formation.show',$item->slug)}}">
                                        @if ($item->img != null)
                                            <img class="img-fluid" src="{{asset($item->img)}}" alt="cover">
                                        @else
                                            <img class="img-fluid" src="{{asset('assets/images/big/img5.jpg')}}" alt="default">
                                        @endif
                                    </a>
                                    <div class="price">
                                        @if ($item->prix != null)
                                            @if ($item->prixpromo != null)
                                                <h3>{{number_format($item->prixpromo,0,'','.')}}<sup>F CFA</sup>  <span>{{number_format($item->prix,0,'','.')}}<sup>F CFA</sup></span></h3>
                                            @else
                                                <h3>{{number_format($item->prix,0,'','.')}}<sup>F CFA</sup> </h3>
                                            @endif
                                        @else
                                            <h3>Gratuit</h3>
                                        @endif
                                    </div>
                                </div>
                                <div class="product-content">
                                    <div class="course-group d-flex">
                                        <div class="course-group-img d-flex">
                                            <a href="javascript:void();">
                                                @if ($item->professeur->img != '')
                                                    <img src="{{asset('assets/userAvatar/'.$item->professeur->img)}}" alt="avatar" class="img-fluid">
                                                @else
                                                    <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="avatar" class="img-fluid">
                                                @endif
                                            </a>
                                            <div class="course-name">
                                                <h4><a href="javascript:void();" class="text-secondary">{{ucwords(substr($item->professeur->name,0,15))}}</a></h4>
                                                <p>Formateur</p>
                                            </div>
                                        </div>
                                        <div class="course-share d-flex align-items-center justify-content-center">
                                            @if (Auth::check())
                                                @if (App\Formation_favori::where('formation_id',$item->id)->where('etudiant_id',Auth::user()->id)->count() == 0)
                                                    <a title="Ajouter à mes favoris" href="javascript:void(0);" ref="{{route('etu.formations.savefavoris', encrypt($item->id))}}" class="btnAction"><i class="fa-regular fa-heart"></i></a>
                                                    <div class="feedback mb-2"></div>
                                                @else
                                                    <i class="fa fa-check-circle fa-3x text-success"></i>
                                                @endif
                                            @else
                                                <a title="Ajouter à mes favoris" href="{{route('login')}}?backto=formations"><i class="fa-regular fa-heart"></i></a>
                                            @endif
                                        </div>
                                    </div>
                                    <h3 class="title"><a href="{{route('w.formation.show',$item->slug)}}">{{substr($item->titre,0,100)}} ...</a></h3>
                                    <div class="course-info d-flex align-items-center">
                                        <div class="rating-img d-flex align-items-center">
                                            <p title="Date de début"><i class="fa fa-calendar" style="color:#e96525;"></i> <span class="text-muted">{{Illuminate\Support\Carbon::parse($item->debut)->format('d/m/Y')}}</span></p>
                                        </div>
                                        <div class="course-view d-flex align-items-center">
                                            <p title="Durée"><i class="fa fa-clock" style="color:#e96525;"></i> <span class="text-muted">{{$item->duree.' '.ucfirst($item->typedelai)}}</span></p>
                                        </div>
                                    </div>

                                    <div class="all-btn all-category d-flex align-items-center">
                                        <a href="{{route('w.formation.show',$item->slug)}}" class="btn btn-primary btn-block">Détails</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</section>
<!-- /Feature Course -->

<!-- Master Skill -->
<section class="section master-skill">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12">
                <div class="section-header aos" data-aos="fade-up">
                    <div class="section-sub-head">
                        <span>Nouveauté</span>
                        <h2>Maîtrisez des compétences entreprenariales pour piloter votre carrière</h2>
                    </div>
                </div>
                <div class="section-text aos" data-aos="fade-up">
                    <p><!-- Obtenez une certification, maîtrisez les compétences technologiques modernes et améliorez votre carrière, que vous débutiez ou que vous soyez un professionnel chevronné. 95 % des apprenants déclarent que notre contenu pratique a directement aidé leur carrière. --></p>
                </div>
                <div class="career-group aos" data-aos="fade-up">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 d-flex">
                            <div class="certified-group blur-border d-flex">
                                <div class="get-certified d-flex align-items-center">
                                    <div class="blur-box">
                                        <div class="certified-img ">
                                            <img src="{{asset('assets/fronts/img/icon/icon-1.svg')}}" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                    <p>Restez motivé avec des formateurs engagés</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 d-flex">
                            <div class="certified-group blur-border d-flex">
                                <div class="get-certified d-flex align-items-center">
                                    <div class="blur-box">
                                        <div class="certified-img ">
                                            <img src="{{asset('assets/fronts/img/icon/icon-2.svg')}}" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                    <p>Maîtriser les outils de pilotage de projet</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 d-flex">
                            <div class="certified-group blur-border d-flex">
                                <div class="get-certified d-flex align-items-center">
                                    <div class="blur-box">
                                        <div class="certified-img ">
                                            <img src="{{asset('assets/fronts/img/icon/icon-3.svg')}}" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                    <p>Obtenez votre certificat</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 d-flex">
                            <div class="certified-group blur-border d-flex">
                                <div class="get-certified d-flex align-items-center">
                                    <div class="blur-box">
                                        <div class="certified-img ">
                                            <img src="{{asset('assets/fronts/img/icon/icon-4.svg')}}" alt="" class="img-fluid">
                                        </div>
                                    </div>
                                    <p>Développez des compétences à votre façon à l'aide de notre laboratoire de formations pratiques</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-12 d-flex align-items-end">
                <div class="career-img aos" data-aos="fade-up">
                    <img src="{{asset('assets/fronts/img/join.png')}}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Master Skill -->

<!-- Share Knowledge -->
<section class="section share-knowledge">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="knowledge-img aos" data-aos="fade-up">
                    <img src="{{asset('assets/fronts/img/share.png')}}" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-md-6 d-flex align-items-center">
                <div class="join-mentor aos" data-aos="fade-up">
                    <h2>Vous-voulez illuminer votre potentiel d'entrepreneur ?</h2>
                    <p>Nous vous offrons la possibilité de vous faire encadrer et coacher par des experts. <a href="{{route('register')}}">Inscrivez-vous</a> pour participer à nos formations dédiées.</p>
                    <div class="all-btn all-category d-flex align-items-center">
                        <a href="{{route('register')}}" class="btn btn-primary">Inscrivez-vous</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Share Knowledge -->

<!-- Become An Formateur -->
<section class="section share-knowledge aos m-6" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 d-flex">
                <div class="student-mentor cube-instuctor ">
                    <h4>Devenir Formateur</h4>
                    <div class="row">
                        <div class="col-lg-7 col-md-12">
                            <div class="top-instructors">
                                <p>Vous êtes un passionné de l'entrepreneuriat des jeunes, et vous avez des compétences en encadrement. Inscrivez-vous et devenez encadreur chez nous !</p>
                                <a href="{{route('w.demande','inst')}}" class="btn btn-primary">S'inscrire</a>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-12">
                            <div class="mentor-img">
                                <img class="img-fluid" alt="" src="assets/fronts/img/become-02.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 d-flex">
                <div class="student-mentor yellow-mentor">
                    <h4>Devenir partenaire</h4>
                    <div class="row">
                        <div class="col-lg-8 col-md-12">
                            <div class="top-instructors">
                                <p>Vous souhaitez intégrer notre catalogue de partenaires afin d'accompagner les jeunes entrepreneurs. Enregistrez-vous et devenez partenaire</p>
                                <a href="{{route('w.demande','part')}}" class="btn btn-warning">S'enregistrer</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="mentor-img">
                                <img class="img-fluid" alt="" src="assets/fronts/img/become-01.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Become An Formateur -->

<!-- Latest Blog -->
<section class="section latest-blog">
    <div class="container">
        @if(count($actus)>0)
        <div class="section-header aos" data-aos="fade-up">
            <div class="section-sub-head feature-head text-center mb-0">
                <h2>Actualités récentes</h2>
                <div class="section-text aos" data-aos="fade-up">
                    <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget aenean accumsan bibendum gravida maecenas augue elementum et neque. Suspendisse imperdiet.</p>
                </div>
            </div>
        </div>
        <div class="owl-carousel blogs-slide owl-theme aos" data-aos="fade-up">
            @foreach($actus as $item)
            <div class="Formateurs-widget blog-widget">
                <div class="Formateurs-img">
                    <a href="{{route('w.newsItem',$item->slug)}}">
                        <img class="img-fluid" alt="{{$item->slug}}" src="{{asset('articles/'.$item->img)}}">
                    </a>
                </div>
                <div class="Formateurs-content text-center">
                    <h5><a href="{{route('w.newsItem',$item->slug)}}">{{$item->title}}</a></h5>
                    <p>{{$item->categories->implode('libelle',', ')}}</p>
                    <div class="student-count d-flex justify-content-center">
                        <i class="fa-solid fa-calendar-days"></i>
                        <span>{{date('d/m/Y',strtotime($item->date))}}</span>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
        @endif

        <div class="enroll-group aos" data-aos="fade-up">
            <div class="row ">
                <div class="col-lg-4 col-md-6">
                    <div class="total-course d-flex align-items-center">
                        <div class="blur-border">
                            <div class="enroll-img">
                                <img src="{{asset('assets/fronts/img/icon/icon-09.svg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="course-count">
                            <h3><span class="counterUp" >{{$catformations->count()}}</span></h3>
                            <p>CATÉGORIES DE FORMATION</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="total-course d-flex align-items-center">
                        <div class="blur-border">
                            <div class="enroll-img ">
                                <img src="{{asset('assets/fronts/img/icon/icon-07.svg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="course-count">
                            <h3><span class="counterUp">{{$dt['demand']}}</span></h3>
                            <p>ÉTUDIANTS SATISFAITS</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="total-course d-flex align-items-center">
                        <div class="blur-border">
                            <div class="enroll-img ">
                                <img src="{{asset('assets/fronts/img/icon/icon-08.svg')}}" alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="course-count">
                            <h3><span class="counterUp" >{{$dt['projet']}}</span></h3>
                            <p>PROJETS INCUBÉS</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Latest Blog -->

@if(count($temoignages)>0)
<!-- Users Love -->
<section class="section user-love">
    <div class="container">
        <div class="section-header white-header aos" data-aos="fade-up">
            <div class="section-sub-head feature-head text-center">
                <span>Témoignages</span>
                <h2>Découvrez des avis réels de nos étudiants/entrepreneurs.</h2>
            </div>
        </div>
    </div>
</section>
<!-- /Users Love -->

<!-- Say testimonial Four -->
<section class="testimonial-four">
    <div class="review">
        <div class="container">
            <div class="testi-quotes">
                <img src="{{asset('assets/fronts/img/qute.png')}}" alt="" >
            </div>
            <div class="mentor-testimonial lazy slider aos" data-aos="fade-up" data-sizes="50vw ">
                @foreach ($temoignages as $item)
                    <div class="d-flex justify-content-center">
                        <div class="testimonial-all d-flex justify-content-center">
                            <div class="testimonial-two-head text-center align-items-center d-flex">
                                <div class="testimonial-four-saying ">
                                    <div class="testi-right">
                                        <img src="{{asset('assets/fronts/img/qute-01.png')}}" alt="">
                                    </div>
                                    <p>{{$item->contenu}}</p>
                                    <div class="four-testimonial-founder">
                                        <div class="fount-about-img">
                                            <a href="#"><img src="{{asset('assets/temoignages/'.$item->photo)}}" alt="{{$item->nom}}" class="img-fluid"></a>
                                        </div>
                                        <h3><a href="#">{{$item->nom}}</a></h3>
                                        <span>{{$item->fonction}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</section>
<!-- /Say testimonial Four -->
@endif

@if(count($partnaires)>0)
<!-- Leading Companies -->
<section class="section lead-companies become-instructors">
    <h1>&nbsp;</h1>
    <div class="container">
        <div class="section-header aos" data-aos="fade-up">
            <div class="section-sub-head feature-head text-center">
                <span>Il nous font confiance</span>
                <h2>Nos partenaires institutionnels</h2>
            </div>
        </div>
        <div class="lead-group aos" data-aos="fade-up">
            <div class="lead-group-slider owl-carousel owl-theme">
                @foreach ($partnaires as $item)
                    <div class="item">
                        <div class="lead-img">
                            <a href="{{$item->lien?:'javascript:void(0)'}}" target="{{$item->lien?:'_blank'}}"><img class="img-fluid" alt="{{$item->titre}}" src="{{asset('assets/partenaires/'.$item->img)}}"></a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</section>
<!-- /Leading Companies -->
@endif

@endsection
