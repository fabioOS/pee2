@extends('web.main')
@section('title', "PEE | Actualités")

@section('css')
<style>
    .blog-image{
        height: 300px;
    }
</style>
@endsection

@section('js')
@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page"><a href="{{route('w.news')}}">Actualités</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<section class="course-content">

    @include('web.include.successOrError')

    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                @if(count($actus)>0)
                    @foreach ($actus as $item)
                    <div class="blog">
                        @if($item->type_article=='text')

                            @if($item->img)
                                <div class="blog-image">
                                    <a href="{{route('w.newsItem',$item->slug)}}"><img class="img-fluid" src="{{asset('articles/'.$item->img)}}" alt="{{$item->title}}"></a>
                                </div>
                            @endif
                        @else
                            <div class="blog-image">
                                <a href="{{route('w.newsItem',$item->slug)}}">
                                    <img class="img-fluid" src="{{$item->img}}" alt="{{$item->title}}">
                                </a>
                            </div>
                        @endif

                        <h3 class="blog-title"><a href="{{route('w.newsItem',$item->slug)}}">{{$item->title}}</a></h3>
                        <div class="blog-info clearfix">
                            <div class="post-left">
                                <ul>
                                    @if($item->auteur)
                                    <li>
                                        <div class="post-author">
                                            <a href="#"><i class="fa fa-user"></i> <span> {{$item->auteur}}</span></a>
                                        </div>
                                    </li>@endif
                                    <li><img class="img-fluid" src="{{asset('assets/fronts/img/icon/icon-22.svg')}}" alt="date de publication">{{date('d/m/Y',strtotime($item->date))}}</li>
                                    <li><img class="img-fluid" src="{{asset('assets/fronts/img/icon/icon-23.svg')}}" alt="categorie">{{$item->categories->implode('libelle',', ')}}</li>
                                </ul>
                            </div>
                        </div>

                        <div class="blog-content blog-read">
                            <p>
                                {!! Str::limit(strip_tags($item->des), 128, ' ...') !!}
                            </p>
                            <a href="{{route('w.newsItem',$item->slug)}}" class="read-more btn btn-primary">Lire Plus</a>
                        </div>
                    </div>
                    @endforeach

                    <!-- Blog pagination -->
                    @if($actus)
                        {{$actus->links('vendor.pagination.news')}}
                    @endif
                    <!-- /Blog pagination -->

                @else
                <p>Aucune actualité disponible pour le moment</p>
                @endif

            </div>

            <!-- Blog Sidebar -->
            <div class="col-lg-3 col-md-12 sidebar-right theiaStickySidebar">

                <!-- Search -->
                <div class="card search-widget blog-search blog-widget">
                    <div class="card-body">
                        <form class="search-form">
                            <div class="input-group">
                                <input type="text" placeholder="Recherche..." class="form-control">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /Search -->


                <!-- Archives Categories -->
                <div class="card category-widget blog-widget">
                    <div class="card-header">
                        <h4 class="card-title">Catégories</h4>
                    </div>
                    <div class="card-body">
                        <ul class="categories">
                            @foreach($cats as $cat)
                            <li><a href="{{route('w.newsCat',$cat->slug)}}"><i class="fas fa-angle-right"></i> {{$cat->libelle}} </a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>
            <!-- /Blog Sidebar -->

        </div>
    </div>
</section>

@endsection
