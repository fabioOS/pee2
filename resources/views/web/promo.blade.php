@extends('web.main')
@section('title', "PEE | Nos réalisations")

@section('css')
@endsection

@section('js')
@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">Nos réalisations</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<section class="course-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="row">
                    @foreach ($promos as $promo)
                        <div class="col-md-4 col-sm-12">
                            <!-- Blog Post -->
                            <div class="blog grid-modern">
                                <div class="blog-image">
                                    <a href="{{route('w.promo.item',$promo->slug)}}"><img class="img-fluid" src="{{asset('promos/'.$promo->img)}}" alt="{{$promo->slug}}"></a>
                                </div>
                                <div class="blog-modern-box">
                                    <h3 class="blog-title"><a class="text-black" href="{{route('w.promo.item',$promo->slug)}}">{{$promo->titre}}</a></h3>
                                    <div class="blog-info clearfix mb-0">
                                        <div class="post-left">
                                            <ul>
                                                <li class="text-black"><img class="img-fluid" src="{{asset('assets/fronts/img/icon/icon-22.svg')}}" alt="date de publication">{{date('d/m/Y',strtotime($promo->date))}}</li>
                                                <li class="text-black"><img class="img-fluid" src="{{asset('assets/fronts/img/icon/icon-24.svg')}}" alt="Auteur">{{$promo->auteur}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <!-- Blog pagination -->
                @if($promos)
                    {{$promos->links('vendor.pagination.news')}}
                @endif
                <!-- /Blog pagination -->

            </div>
        </div>
    </div>
</section>

@endsection
