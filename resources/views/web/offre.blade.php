@extends('web.main')
@section('title', "PEE | Offres {{isset($libel) ? $libel:''}}")

@section('css')
@endsection

@section('js')

<script>
    $(document).ready(function() {
        $( ".filterObj" ).prop( "checked", false );
        $('#f_search .filterObj').each(function(index, element){
            $(this).click(function() {
                var data = $('#f_search').serialize();
                var url = $('#f_search').attr('url');
                console.log(data, url);
                var container = $('#f_container');
                return Query_GET(url,data,container);
            });
        });


        function Query_GET(url,data,container) {
            $.ajax({
                type: "GET",
                url: url,
                data: data,
                beforeSend: function() {
                    //container.html('<div class="text-center"><div class="spinner-border text-primary" role="status"><span class="sr-only">Chargement...</span></div></div>');
                    container.html('<div class="text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
                },
                success: function(data) {
                    container.html(data);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }


    });
</script>
@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">Offres</li>
                            @isset($libel)
                            <li class="breadcrumb-item active" aria-current="page">Offres {{$libel}}</li>
                            @endisset
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<section class="course-content share-knowledge-">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">

                <div id="f_container">
                    <div class="all-course">
                        <div class="row">
                            @foreach ($offres as $item)
                            <div class="col-xl-12 col-lg-12 col-md-6 col-12">
                                <div class="course-box-three">
                                    <div class="course-three-item">
                                        <div class="course-three-content ">
                                            <div class="course-three-text">
                                                <a href="javascript:void();">
                                                    <p>{{$item->categorie->libelle}}</p>
                                                    <h3 class="title instructor-text">{{$item->poste}}</h3>
                                                </a>
                                            </div>

                                            <div class="student-counts-info d-flex align-items-center">
                                                <div class="students-three-counts d-flex align-items-center">
                                                    <p style="margin-left:0 ;"><span class="fa fa-calendar"> </span> &nbsp; Publiée le : <b>{{date('d/m/Y',strtotime($item->datepublie))}}</b></p>
                                                </div>
                                            </div>

                                            <div class="price-three-group d-flex align-items-center justify-content-between justify-content-between">
                                                <div class="price-three-view d-flex align-items-center">
                                                    <div class="course-price-three">
                                                        <a href="{{route('w.offreItem',$item->slug)}}" class="btn btn-action">Voir les détails</a>
                                                    </div>
                                                </div>
                                                <div class="price-three-time d-inline-flex align-items-center">
                                                    <i class="fa-regular fa-clock me-2"></i>
                                                    <span>Date limit : <b>{{date('d/m/Y',strtotime($item->datelimite))}}</b></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <!-- /pagination -->
                    @if($offres)
                        {{$offres->links('vendor.pagination.news')}}
                    @endif
                    <!-- /pagination -->
                </div>

            </div>
            <div class="col-lg-3 theiaStickySidebar">
                <div class="filter-clear">
                    <div class="clear-filter d-flex align-items-center">
                        <h4><i class="feather-filter"></i>Filtrer</h4>
                        <div class="clear-text">
                            <p><a href="{{route('w.offre.type','emplois')}}">EFFACER</a></p>
                        </div>
                    </div>

                    <form action="javascript:void(0)" url="{{route("w.offre.search")}}" id="f_search">
                        <div class="row mb-3">
                            <div class="col-lg-12">
                                <div class="show-filter add-course-info">
                                    <div class="row gx-2 align-items-center">
                                        <div class="col-md-12 col-item">
                                            <div class="search-group">
                                                <i class="feather-search"></i>
                                                <input type="text" name="search" value="" class="form-control filterObj" placeholder="Effectuer une recherche" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Search Filter -->
                        <div class="card search-filter ">
                            <div class="card-body">
                                @isset($libel)
                                <input type="hidden" name="type" value="{{$libel}}">
                                @endisset
                                <div class="filter-widget mb-0">
                                    <div class="categories-head d-flex align-items-center">
                                        <h4>Ordonner par date</h4>
                                        <i class="fas fa-angle-down"></i>
                                    </div>
                                    <div>
                                        <label class="custom_check custom_one">
                                            <input class="filterObj" type="radio" name="orderdate" value="publie">
                                            <span class="checkmark"></span>  Date de publication
                                        </label>
                                    </div>
                                    <div>
                                        <label class="custom_check custom_one mb-0">
                                            <input class="filterObj" type="radio" name="orderdate" value="limite" checked>
                                            <span class="checkmark"></span>  Date limite
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Search Filter -->

                        <!-- Search Filter -->
                        <div class="card search-filter">
                            <div class="card-body">
                                <div class="filter-widget mb-0">
                                    <div class="categories-head d-flex align-items-center">
                                        <h4>Trier par catégories</h4>
                                        <i class="fas fa-angle-down"></i>
                                    </div>
                                    @foreach ($cats as $item)
                                    <div>
                                        <label class="custom_check">
                                            <input class="filterObj" type="radio" name="by_cat" value="{{$item->id}}">
                                            <span class="checkmark"></span> {{$item->libelle}} ({{$item->offre->where('type',$item->idd)->count()}})
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- /Search Filter -->
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
