@extends('web.main')
@section('title', "PEE | Politique de confidentialité")

@section('css')
@endsection

@section('js')
@endsection

@section('content')

<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">Politique de confidentialité</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <h1 class="mb-0">Politique de confidentialité</h1>
            </div>
        </div>
    </div>
</div>

<div class="page-content">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="terms-content">
                    <div class="terms-text">
                        <h3>Date d'entrée en vigueur : <span>20 mai 2023</span></h3>
                        <p>La protection de votre vie privée est importante pour nous. Cette politique de confidentialité explique comment nous collectons, utilisons, divulguons et protégeons vos informations personnelles lorsque vous utilisez notre plateforme de formation hybride. Veuillez lire attentivement cette politique avant d'utiliser notre plateforme. En utilisant la plateforme, vous acceptez les pratiques décrites dans cette politique de confidentialité.</p>
                        <h3>Collecte des informations personnelles</h3>
                        <p>Lorsque vous vous inscrivez sur notre plateforme, nous pouvons collecter certaines informations personnelles vous concernant, telles que votre nom, votre adresse e-mail, votre numéro de téléphone et vos préférences d'apprentissage. Nous collectons ces informations afin de vous fournir un accès personnalisé à notre plateforme et de vous offrir une expérience d'apprentissage de qualité.</p>
                        <h3>Utilisation des informations</h3>
                        <p> Nous utilisons les informations personnelles que nous collectons pour administrer votre compte, vous fournir un accès aux cours et au contenu pédagogique, vous envoyer des notifications et des mises à jour importantes, améliorer nos services et personnaliser votre expérience sur la plateforme.</p>
                        <h3>Divulgation des informations</h3>
                        <p>
                            Nous ne vendons, ne louons ni ne partageons vos informations personnelles avec des tiers, sauf dans les cas suivants :
                        </p>
                        <ul>
                            <li>Lorsque cela est nécessaire pour fournir nos services, tels que la communication avec les formateurs ou les partenaires d'apprentissage.</li>
                            <li>Lorsque cela est exigé par la loi ou pour répondre à des demandes légales valides, telles que les assignations à comparaître, les ordonnances judiciaires ou les enquêtes gouvernementales.</li>
                            <li>Lorsque cela est nécessaire pour protéger nos droits, notre propriété ou notre sécurité, ainsi que ceux de nos utilisateurs ou du public.</li>
                        </ul>
                        <h3>Sécurité des informations</h3>
                        <p>Nous mettons en place des mesures de sécurité appropriées pour protéger vos informations personnelles contre tout accès, utilisation ou divulgation non autorisés. Cependant, veuillez noter qu'aucune méthode de transmission de données sur Internet ou de stockage électronique n'est totalement sécurisée. Nous ne pouvons donc pas garantir une sécurité absolue des informations.</p>
                        <h3>Cookies et technologies similaires</h3>
                        <p>Nous utilisons des cookies et d'autres technologies similaires pour collecter des informations et améliorer votre expérience sur notre plateforme. Les cookies sont de petits fichiers texte stockés sur votre appareil qui nous aident à reconnaître votre navigateur et à vous fournir des fonctionnalités personnalisées.</p>
                        <h3>Liens vers des sites tiers</h3>
                        <p>Notre plateforme peut contenir des liens vers des sites Web de tiers. Cette politique de confidentialité ne s'applique pas aux pratiques de confidentialité de ces sites. Nous vous encourageons à lire les politiques de confidentialité de ces sites tiers, car nous n'avons aucun contrôle sur leurs pratiques et politiques de confidentialité.</p>
                        <h3>Modifications de la politique de confidentialité</h3>
                        <p>Nous nous réservons le droit de modifier cette politique de confidentialité à tout moment. Toute modification de cette politique entrera en vigueur dès sa publication sur la plateforme. Nous vous encourageons à consulter régulièrement cette politique pour être informé des éventuelles modifications. Votre utilisation continue de la plateforme après la publication des modifications constitue votre acceptation de ces modifications.</p>
                        <h3>Accès et mise à jour de vos informations</h3>
                        <p>Vous avez le droit d'accéder, de corriger, de mettre à jour ou de supprimer les informations personnelles que nous avons collectées vous concernant. Vous pouvez le faire en vous connectant à votre compte sur la plateforme ou en nous contactant à l'adresse e-mail indiquée ci-dessous. Nous ferons de notre mieux pour répondre à votre demande dans les meilleurs délais.</p>
                        <h3>Conservation des informations</h3>
                        <p>Nous conserverons vos informations personnelles aussi longtemps que nécessaire pour atteindre les finalités décrites dans cette politique de confidentialité, à moins qu'une période de conservation plus longue ne soit requise ou autorisée par la loi.</p>
                        <h3>Protection des enfants</h3>
                        <p>Notre plateforme de formation hybride n'est pas destinée aux enfants de moins de 16 ans. Nous ne collectons pas sciemment d'informations personnelles auprès d'enfants de moins de 16 ans. Si nous découvrons que nous avons collecté des informations personnelles d'un enfant de moins de 16 ans sans le consentement parental vérifiable, nous les supprimerons immédiatement de nos systèmes.</p>
                        <h3>Contact</h3>
                        <p>Si vous avez des questions, des préoccupations ou des commentaires concernant notre politique de confidentialité, veuillez nous contacter à l'adresse e-mail suivante : <a href="mailto:infos@pee-ci.com">infos@pee-ci.com</a>.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
