@extends('web.main')
@section('title', "PEE | Contact")

@section('css')
@endsection

@section('js')
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        var onloadCallback = function() {
            grecaptcha.render('reCAPTCHA-Container', {
                'sitekey' : "{{ env('CAPTCHA_KEY') }}",
                'theme' : 'light'
            });
        };

    </script>

@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">Contact</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<section class="course-content ">

    @include('web.include.successOrError')

    <div class="container">
        <div class="row">
            <div class="col-lg-5 text-center mx-auto">
                <div class="title-sec">
                    <h2>Gardons le contact</h2>
                    <p>Envoyez-nous un message, un besoin ou une suggestion.</p>
                </div>
            </div>
        </div>

        <!-- Plan Type -->
        <div class="row">
            <!-- <div class="col-lg-1"></div> -->
            <div class="col-lg-4 col-md-6 plan-box p-4">

                <!-- Footer Widget -->
                <div class="footer-widget footer-contact loginbox">
                    <h4>Adresse</h4>
                    <div class="news-letter border border-top border-light mb-4"></div>
                    <div class="footer-contact-info">
                        <div class="footer-address">
                            <img src="{{asset('assets/fronts/img/icon/icon-20.svg')}}" alt="" class="img-fluid">
                            <p> Université Félix Houphouët-Boigny, Côcôdy<br> Abidjan, Côte d'Ivoire</p>
                        </div>
                        <p>
                            <img src="{{asset('assets/fronts/img/icon/cloud.svg')}}" alt="" class="img-fluid">
                            <a href="www.pee-ci.com" class="">www.pee-ci.com</a>
                        </p>
                        <p>
                            <img src="{{asset('assets/fronts/img/icon/icon-19.svg')}}" alt="" class="img-fluid">
                            <a href="mailto:infos@pee-ci.com">infos@pee-ci.com</a>
                        </p>
                        <p class="mb-0">
                            <img src="{{asset('assets/fronts/img/icon/icon-21.svg')}}" alt="" class="img-fluid">
                            <a href="tel:+225 01 73 50 57 57">+225 01 73 50 57 57</a>
                        </p>
                    </div>
                </div>
                <!-- /Footer Widget -->

            </div>
            <div class="col-lg-8">
                <div class="plan-box loginbox">
                    <form action="{{route('w.contact.send')}}" method="post" class="col-md-12">@csrf
                        <div class="col-lg-12 col-md-12">
                            <h4>Formulaire de contact</h4>
                            <div class="news-letter border border-top border-light mb-4"></div>
                            <p>Veuillez renseigner le formulaire puis l'envoyer. </p>
                            <p text-muted>Les champs marqués ( <span class="text-danger">*</span> ) sont obligatoires</p>
                            <!-- Form contact -->
                            <div class="form-group col-lg-12">
                                <label for="nom">Nom et Prénom (<span class="text-danger">*</span>)</label>
                                <input required type="text" name="nom" id="nom" class="form-control" placeholder="Nom">
                            </div>
                            <div class="form-group">
                                <label for="email">Email (<span class="text-danger">*</span>)</label>
                                <input required type="email" name="email" id="email" class="form-control" placeholder="email">
                            </div>
                            <div class="form-group">
                                <label for="objet">Objet (<span class="text-danger">*</span>)</label>
                                <input required type="text" name="objet" id="objet" class="form-control" placeholder="objet">
                            </div>
                            <div class="form-group">
                                <label for="message">Message (<span class="text-danger">*</span>)</label>
                                <textarea required class="form-control" name="message" id="message" rows="5" placeholder="Message"></textarea>
                            </div>

                            <div class="col-md-12" id="reCAPTCHA-Container" style="margin-top: 10px;">Chargement de Recaptcha...</div>
                            <div id="reCAPTCHA-error" class="text-red">&nbsp;</div>

                            <div class="form-group col-md-12" style="text-align:left;">
                                <button type="submit" class="btn btn-primary">Envoyer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- /Plan Type -->

    </div>
</section>

@endsection
