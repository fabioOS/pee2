@extends('web.main')
@section('title', "A propos de PEE")

@section('css')
@endsection

@section('js')

@endsection

@section('content')

<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">A propos de PEE</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="master-section-five- share-knowledge">
    <div class="container">
    <div class="master-five-vector">
        <img class="ellipse-right" src="{{asset('assets/fronts/img/bg/master-vector-left.svg')}}" alt="">
    </div>
        <div class="row">
            <div class="col-lg-6 col-sm-12" data-aos="fade-down">
                <div class="section-five-sub">
                    <div class="header-five-title">
                        <h2>A propos du Pôle de l'Etudiant Entrepreneur</h2>
                    </div>
                    <div class="career-five-content">
                        <p data-aos="fade-down">Get certified, master modern tech skills, and level up your career — whether you’re starting out or a seasoned pro. 95% .</p>
                        <p class="mb-0" data-aos="fade-down">Get certified, master modern tech skills, and level up your career — whether you’re starting out or a seasoned pro. 95% of eLearning learners report our hands-on content directly helped their careers.</p>
                    </div>
                    <a href="#" class="learn-more-five">En savoir plus</a>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12">
                <div class="row">
                    <div class="col-lg-6 col-sm-6" data-aos="fade-down">
                        <div class="skill-five-item">
                            <div class="skill-five-icon">
                                <img src="{{asset('assets/fronts/img/skills/skills-01.svg')}}" class="bg-warning" alt="Stay motivated">
                            </div>
                            <div class="skill-five-content">
                                <h3>Stay motivated with engaging instructors</h3>
                                <p>eLearning learners report our hands-on content directly helped their careers.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6" data-aos="fade-down">
                        <div class="skill-five-item">
                            <div class="skill-five-icon">
                                <img src="{{asset('assets/fronts/img/skills/skills-02.svg')}}" class="bg-info" alt="Stay motivated">
                            </div>
                            <div class="skill-five-content">
                                <h3>Keep up with in the latest in cloud</h3>
                                <p>eLearning learners report our hands-on content directly helped their careers.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6" data-aos="fade-down">
                        <div class="skill-five-item">
                            <div class="skill-five-icon">
                                <img src="{{asset('assets/fronts/img/skills/skills-03.svg')}}" class="bg-danger" alt="Stay motivated">
                            </div>
                            <div class="skill-five-content">
                                <h3>Get certified with 100+ certification courses</h3>
                                <p>eLearning learners report our hands-on content directly helped their careers.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6" data-aos="fade-down">
                        <div class="skill-five-item">
                            <div class="skill-five-icon">
                                <img src="{{asset('assets/fronts/img/skills/skills-04.svg')}}" class="bg-light-green" alt="Stay motivated">
                            </div>
                            <div class="skill-five-content">
                                <h3>Build skills your way, from labs to courses</h3>
                                <p>eLearning learners report our hands-on content directly helped their careers.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div></div></section>



<section class="share-knowledge-five">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-6 col-sm-12" data-aos="fade-down">
                <div class="section-five-sub">
                    <div class="header-five-title">
                        <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
                    </div>
                    <div class="career-five-content">
                        <p>High-definition video is video of higher resolution and quality than standard-definition. While there is no standardized meaning for high-definition, generally any video.While there is no standardized meaning for high-definition, generally any video.</p>
                    </div>
                    <div class="knowledge-list-five">
                        <ul>
                            <li>
                                <div class="knowledge-list-group">
                                    <img src="{{asset('assets/fronts/img/icon/award-new.svg')}}" alt="">
                                    <p>Best Courses</p>
                                </div>
                            </li>
                            <li class="mb-0">
                                <div class="knowledge-list-group">
                                    <img src="{{asset('assets/fronts/img/icon/award-new.svg')}}" alt="">
                                    <p>Best Courses</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <a href="#" class="learn-more-five">Commencez à enseigner aujourd'hui</a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12" data-aos="fade-down">
                <div class="joing-count-five text-center">
                    <img src="{{asset('assets/fronts/img/joing-us-skill.png')}}" alt="">
                    <div class="joing-count-five-one course-count">
                        <h3 class="joing-count-number"><span class="counterUp">55</span>K<span>+</span></h3>
                        <p class="joing-count-text">Formations</p>
                    </div>
                    <div class="joing-count-five-two course-count">
                        <h3 class="joing-count-number"><span class="counterUp">1</span>K</h3>
                        <p class="joing-count-text">Appreciations</p>
                    </div>
                    <div class="joing-count-five-three course-count">
                        <h3 class="joing-count-number"><span class="counterUp">100</span></h3>
                        <p class="joing-count-text">Pays</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="transform-section-five- share-knowledge">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-6 col-sm-12" data-aos="fade-down">
                <div class="joing-count-five">
                    <img src="{{asset('assets/fronts/img/education.png')}}" alt="">
                    <div class="transform-count-five-one course-count">
                        <h3 class="joing-count-number"><span class="counterUp">0</span></h3>
                        <p class="joing-count-text">Pays</p>
                    </div>
                    <div class="transform-count-five-two course-count">
                        <h3 class="joing-count-number"><span class="counterUp">0</span>K<span>+</span></h3>
                        <p class="joing-count-text">Cours</p>
                    </div>
                    <div class="transform-count-five-three course-count">
                        <h3 class="joing-count-number"><span class="counterUp">0</span>K<span>+</span></h3>
                        <p class="joing-count-text">Appreciations</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12" data-aos="fade-down">
                <div class="transform-access-content">
                    <div class="header-five-title">
                        <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut consequat mauris Lorem ipsum dolor sit amet</h2>
                    </div>
                    <div class="career-five-content">
                        <p class="mb-0">Create an account to receive our newsletter, course recommendations and promotions. Create an account to receive our newsletter, course recommendations and promotions. Create an account to receive our newsletter, course recommendations and promotions.</p>
                    </div>
                    <div class="knowledge-list-five">
                        <ul>
                            <li>
                                <div class="knowledge-list-group">
                                    <img src="{{asset("assets/fronts/img/icon/award-new.svg")}}" alt="">
                                    <p>Award Winning Course Management</p>
                                </div>
                            </li>
                            <li>
                                <div class="knowledge-list-group">
                                    <img src="{{asset("assets/fronts/img/icon/award-new.svg")}}" alt="">
                                    <p>Learn anything from anywhere anytime
                                    </p>
                                </div>
                            </li>
                            <li class="mb-0">
                                <div class="knowledge-list-group">
                                    <img src="{{asset("assets/fronts/img/icon/award-new.svg")}}" alt="">
                                    <p>Certification for solid development of your Carrer
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <a href="#" class="learn-more-five">Commencez à enseigner aujourd'hui</a>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
