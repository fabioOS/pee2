@extends('web.main')
@section('title', "PEE | Recherche")

@section('css')
@endsection

@section('js')
@endsection

@section('content')
<section class="course-content">
    <div class="container">

        <div class="title-sec">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <h2>{{$search}}</h2>
                    <p>Votre recherche</p>
                </div>
            </div>
        </div>
        <hr>
        <!-- Notifications List -->
        <div class="notify-sec mt-5">
            <div class="row">
                @php $nbsea = 0 @endphp
                {{-- @if($type==0) --}}
                <div class="col-md-12">
                    {{-- Check dans la formation --}}
                    @if(isset($datas['formas']) && count($datas['formas'])>0)
                    @php $nbsea = count($datas['formas']) @endphp
                    <h5>Formations</h5>
                        @foreach ($datas['formas'] as $item)
                            <div class="notify-item">
                                <div class="row align-items-center">
                                    <div class="col-md-9">
                                        <div class="notify-content">
                                            <a href="{{route('w.formation.show',$item->slug)}}">
                                                @if ($item->img != null)
                                                    <img class="avatar-img semirounded-circle" src="{{asset($item->img)}}" alt="{{$item->titre}}">
                                                @else
                                                    <img class="avatar-img semirounded-circle" src="{{asset('assets/images/big/img5.jpg')}}" alt="default">
                                                @endif
                                            </a>
                                            <div class="notify-detail">
                                                <h6><a href="{{route('w.formation.show',$item->slug)}}">{{$item->titre}}</a></h6>
                                                <p>{!! str_limit(strip_tags($item->description),200,'...') !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="text-md-end">
                                            <a href="{{route('w.formation.show',$item->slug)}}" class="btn">Voir plus</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                    {{-- Check dans la formation --}}
                    @if(isset($datas['offres']) &&  count($datas['offres'])>0)
                    @php $nbsea = count($datas['offres']) @endphp
                    <h5>Offres</h5>
                        @foreach ($datas['offres'] as $item)
                            <div class="notify-item">
                                <div class="row align-items-center">
                                    <div class="col-md-9">
                                        <div class="notify-content">
                                            <div class="notify-detail">
                                                <h6><a href="{{route('w.offreItem',$item->slug)}}">{{$item->poste}}</a></h6>
                                                <small class="text-info">{{$item->categorie->libelle}}</small>
                                                <p>{!! str_limit(strip_tags($item->description),200,'...') !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="text-md-end">
                                            <a href="{{route('w.offreItem',$item->slug)}}" class="btn">Voir plus</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                    {{-- Check dans la Actus --}}
                    @if(isset($datas['actus']) &&  count($datas['actus'])>0)
                    @php $nbsea = count($datas['actus']) @endphp
                    <h5>Actualités</h5>
                        @foreach ($datas['actus'] as $item)
                            <div class="notify-item">
                                <div class="row align-items-center">
                                    <div class="col-md-9">
                                        <div class="notify-content">
                                            <a href="{{route('w.newsItem',$item->slug)}}">
                                                @if ($item->img != null)
                                                    <img class="avatar-img semirounded-circle" src="{{asset('articles/'.$item->img)}}" alt="{{$item->title}}">
                                                @else
                                                    <img class="avatar-img semirounded-circle" src="{{asset('assets/images/big/img5.jpg')}}" alt="default">
                                                @endif
                                            </a>
                                            <div class="notify-detail">
                                                <h6><a href="{{route('w.newsItem',$item->slug)}}">{{$item->title}}</a></h6>
                                                <p>{!! str_limit(strip_tags($item->des),200,'...') !!}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="text-md-end">
                                            <a href="{{route('w.newsItem',$item->slug)}}" class="btn">Voir plus</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
                {{-- @endif --}}

                @if($nbsea==0)
                    <p>Aucun resultat trouvé pour votre recherche.</p>
                @endif
            </div>
        </div>
        <!-- /Notifications List -->

    </div>
</section>

@endsection
