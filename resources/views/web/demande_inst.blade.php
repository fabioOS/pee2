@extends('web.main')
@section('title', "PEE | Demande")

@section('css')
    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/switchery/dist/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />

@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}" type="text/javascript"></script>

    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        // Select2
        $(".select2").select2();
        var onloadCallback = function() {
            grecaptcha.render('reCAPTCHA-Container', {
                'sitekey' : "{{ env('CAPTCHA_KEY') }}",
                'theme' : 'light'
            });
        };
        function formatNumber(input) {
            input.value = input.value.replace(/[^\d]/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "");
        }
    </script>
@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">
                                Demande
                                @if ($type=='part')
                                    de partenariat
                                @elseif ($type=='inst')
                                    de collaboration
                                @endif
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<section class="course-content ">

    @include('web.include.successOrError')

    <div class="container">
        <div class="row">
            <div class="col-lg-7 text-center mx-auto">
                <div class="title-sec">
                    <h2>Envoyer-nous votre demande d'encadrement</h2>
                    <h4 class="">Vous souhaiter devenir un de nos encadreurs ?</h4>
                    <p>
                        Soumettons-nous votre demande, et vous serez informer par la suite.
                    </p>
                </div>
            </div>
        </div>

        <!-- Plan Type -->
        <div class="row">
            <!-- <div class="col-lg-1"></div> -->
            <div class="col-lg-2 col-md-3 plan-box- p-4">

            </div>
            <div class="col-lg-8">
                <div class="plan-box loginbox">
                    <form action="{{route('w.demande.send')}}" method="POST" class="col-md-12">
                        @csrf
                        <input type="hidden" name="type" value="{{$type}}">
                        <div class="col-lg-12 col-md-12">
                            <h4>Formulaire de demande d'encadrement</h4>
                            <div class="news-letter border border-top border-light mb-4"></div>
                                <p>Veuillez renseigner le formulaire puis l'envoyer. </p>
                                <p text-muted>Les champs marqués ( <span class="text-danger">*</span> ) sont obligatoires</p> <br>
                                <!-- Form contact -->
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="nom">Nom et Prénom (<span class="text-danger">*</span>)</label>
                                <input required type="text" min="6" max="250" name="nom" id="nom" class="form-control" placeholder="Nom et Prénom">
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="nom">Compétence (<span class="text-danger">*</span>)</label>
                                <input required type="text" min="6" max="200" name="competence" id="competence" class="form-control" placeholder="competence">
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="encadrement">Encadrement(s) envisagé(s) (<span class="text-danger">*</span>)</label>
                                <select required name="encadrement[]" id="encadrement" class="form-control select2 select2-multiple" multiple>
                                    <option value="" disabled>-- Encadrement envisagé --</option>
                                    @foreach ($catformations as $catformation)
                                        <option value="{{$catformation->id}}">{{$catformation->libelle}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">Email (<span class="text-danger">*</span>)</label>
                                <input required type="email" min="6" max="100" name="email" id="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="email">Contact (<span class="text-danger">*</span>)</label>
                                <input required type="text" min="6" max="30" name="contact" id="contact" class="form-control" placeholder="Contact" onkeyup="formatNumber(this)">
                            </div>
                            <div class="form-group">
                                <label for="message">Commentaire </label>
                                <textarea class="form-control" name="commentaire" id="commentaire" rows="5" placeholder="Ajouter un commentaire"></textarea>
                            </div>
                            <div class="col-md-12" id="reCAPTCHA-Container" style="margin-top: 10px;">Chargement de Recaptcha...</div>
                            <div id="reCAPTCHA-error" class="text-red">&nbsp;</div>

                            <div class="form-group col-md-12" style="text-align:left;">
                                <button type="submit" class="btn btn-primary">Envoyer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <!-- /Plan Type -->

    </div>
</section>

@endsection
