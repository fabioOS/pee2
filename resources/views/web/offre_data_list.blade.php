<div class="all-course">
    <div class="row">
        @if(count($offres)>0)
            @foreach ($offres as $item)
            <div class="col-xl-12 col-lg-12 col-md-6 col-12">
                <div class="course-box-three">
                    <div class="course-three-item">
                        <div class="course-three-content ">
                            <div class="course-three-text">
                                <a href="javascript:void();">
                                    <p>{{$item->categorie->libelle}}</p>
                                    <h3 class="title instructor-text">{{$item->poste}}</h3>
                                </a>
                            </div>

                            <div class="student-counts-info d-flex align-items-center">
                                <div class="students-three-counts d-flex align-items-center">
                                    <p style="margin-left:0 ;"><span class="fa fa-calendar"> </span> &nbsp; Publiée le : <b>{{date('d/m/Y',strtotime($item->datepublie))}}</b></p>
                                </div>
                            </div>

                            <div class="price-three-group d-flex align-items-center justify-content-between justify-content-between">
                                <div class="price-three-view d-flex align-items-center">
                                    <div class="course-price-three">
                                        <a href="{{route('w.offreItem',$item->slug)}}" class="btn btn-action">Voir les détails</a>
                                    </div>
                                </div>
                                <div class="price-three-time d-inline-flex align-items-center">
                                    <i class="fa-regular fa-clock me-2"></i>
                                    <span>Date limit : <b>{{date('d/m/Y',strtotime($item->datelimite))}}</b></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        @else
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="col-lg-12 col-md-12 d-flex label label-warning text-center"> <p style="text-align:center;" class="lead m-t-0">Aucune offre disponible pour l'instant !</p>  </div>
        @endif
    </div>
</div>

<!-- /pagination -->
@if($offres)
    {{$offres->links('vendor.pagination.news')}}
@endif
<!-- /pagination -->
