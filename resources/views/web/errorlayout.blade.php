<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>@yield('title')</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/fronts/img/favicon.png') }}">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/bootstrap.min.css') }}">
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/fontawesome/css/fontawesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/fontawesome/css/all.min.css') }}">
		<!-- Main CSS -->
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/style.css') }}">

	</head>
	<body>
        
        <!-- Main Wrapper -->
        <div class="main-wrapper">
            <div class="error-box">
                @yield('content')
			</div>
        </div>
		<!-- /Main Wrapper -->

        <!-- jQuery -->
		<script src="{{ asset('assets/fronts/js/jquery-3.6.0.min.js') }}"></script>
		<!-- Bootstrap Core JS -->
		<script src="{{ asset('assets/fronts/js/bootstrap.bundle.min.js') }}"></script>
		<!-- Custom JS -->
		<script src="{{ asset('assets/fronts/js/script.js') }}"></script>

	</body>
</html>
