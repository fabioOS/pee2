@extends('web.main')
@section('title', $item->title)

@section('css')
<style>
    .blog-image{
        height: 300px;
    }
</style>
@endsection

@section('js')
@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item"><a href="{{route('w.news')}}">Actualités</a></li>
                            <li class="breadcrumb-item" aria-current="page">{{$item->title}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<section class="course-content">

    @include('web.include.successOrError')

    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <!-- Blog Post -->
                <div class="blog">
                    @if($item->type_article=='text')

                        @if($item->img)
                            <div class="blog-image">
                                <a href="#"><img class="img-fluid" src="{{asset('articles/'.$item->img)}}" alt="{{$item->title}}"></a>
                            </div>
                        @endif
                    @else
                        <div class="blog-image">
                            <a href="#">
                                <img class="img-fluid" src="{{$item->img}}" alt="{{$item->title}}">
                            </a>
                        </div>
                    @endif


                    <div class="blog-info clearfix">
                        <div class="post-left">
                            <ul>
                                @if($item->auteur)
                                <li>
                                    <div class="post-author">
                                        <a href="#"><i class="fa fa-user m-r-5"></i> <span> {{$item->auteur}}</span></a>
                                    </div>
                                </li>@endif
                                <li><img class="img-fluid" src="{{asset('assets/fronts/img/icon/icon-22.svg')}}" alt="">{{date('d/m/Y',strtotime($item->date))}}</li>
                                <li><img class="img-fluid" src="{{asset('assets/fronts/img/icon/icon-23.svg')}}" alt="">{{$item->categories->implode('libelle',', ')}}</li>
                            </ul>
                        </div>
                    </div>
                    <h3 class="blog-title"><a href="#">{{$item->title}}</a></h3>
                    <div class="blog-content">
                        {!! $item->des !!}
                    </div>
                </div>
                <!-- /Blog Post -->

            </div>

            <!-- Blog Sidebar -->
            <div class="col-lg-3 col-md-12 sidebar-right theiaStickySidebar">

                <!-- Search -->
                <div class="card search-widget blog-search blog-widget">
                    <div class="card-body">
                        <form class="search-form">
                            <div class="input-group">
                                <input type="text" placeholder="Search..." class="form-control">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /Search -->

                <!-- Latest Posts -->
                <div class="card post-widget blog-widget">
                    <div class="card-header">
                        <h4 class="card-title">Actualites Récentes</h4>
                    </div>
                    <div class="card-body">
                        <ul class="latest-posts">
                            @foreach($recent3 as $recent)
                            <li>
                                @if($item->img)
                                <div class="post-thumb">
                                    <a href="{{route('w.newsItem',$recent->slug)}}">
                                        <img class="img-fluid" src="{{$recent->type_article=='text' ? asset('articles/'.$recent->img) : $recent->img}}" alt="">
                                    </a>
                                </div>
                                @endif
                                <div class="post-info">
                                    <h4>
                                        <a href="{{route('w.newsItem',$recent->slug)}}">{{$recent->title}}</a>
                                    </h4>
                                    <p><img class="img-fluid" src="assets/img/icon/icon-22.svg" alt="">{{date('d/m/Y',strtotime($recent->date))}}</p>
                                </div>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
                <!-- /Latest Posts -->

                <!-- Categories -->
                <div class="card category-widget blog-widget">
                    <div class="card-header">
                        <h4 class="card-title">Categories</h4>
                    </div>
                    <div class="card-body">
                        <ul class="categories">
                            @foreach($cats as $itemc)
                            <li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i> {{$itemc->libelle}} </a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <!-- /Categories -->

                <!-- Archives Categories -->
                {{-- <div class="card category-widget blog-widget">
                    <div class="card-header">
                        <h4 class="card-title">Archives</h4>
                    </div>
                    <div class="card-body">
                        <ul class="categories">
                            <li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i> January 2022 </a></li>
                            <li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i> February 2022 </a></li>
                            <li><a href="javascript:void(0);"><i class="fas fa-angle-right"></i> April 2022 </a></li>
                        </ul>
                    </div>
                </div> --}}
                <!-- /Archives Categories -->

            </div>
            <!-- /Blog Sidebar -->

        </div>
    </div>
</section>

@endsection
