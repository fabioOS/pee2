@extends('web.main')
@section('title', $offre->poste." - PEE | Offres ")

@section('css')
@endsection

@section('js')
@endsection

@section('content')

<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">Offres</li>
                            <li class="breadcrumb-item" aria-current="page">{{$offre->poste}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<div class="inner-banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="instructor-wrap border-bottom-0 m-0">
                    <span class="web-badge mb-3">{{$offre->categorie->libelle}}</span>
                </div>
                <h2>{{$offre->poste}}</h2>
            </div>
        </div>
    </div>
</div>

<section class="page-content course-sec">
    <div class="container">

        <div class="row">
            <div class="col-lg-8">

                <!-- Overview -->
                <div class="card overview-sec">
                    <div class="card-body">
                        <h5 class="subs-title">Description de l'offre</h5>

                        {!! $offre->description !!}
                    </div>
                </div>
                <!-- /Overview -->
            </div>

            <div class="col-lg-4">
                <div class="sidebar-sec">

                    <div class="card feature-sec">
                        <div class="card-body">
                            <div class="cat-title">
                                {{-- <h4>Includes</h4> --}}
                            </div>
                            <ul>
                                <li>Type d'offre:
                                    <span>
                                        @if($offre->type==1)
                                            Emplois
                                        @elseif ($offre->type==2)
                                            Stages
                                        @else
                                            Financement
                                        @endif
                                    </span>
                                </li>
                                <li>Niveau(x): <span>{{$offre->niveau}}</span></li>
                                <li>Expérience: <span>{{$offre->experience}}</span></li>
                                <li>Lieu:<span> {{$offre->lieu}}</span></li>
                                <li>Date de publication: <span class="text-danger">{{date('d/m/Y',strtotime($offre->datepublie))}}</span></li>
                                <li>Date limite: <span class="text-danger">{{date('d/m/Y',strtotime($offre->datepublie))}}</span></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
