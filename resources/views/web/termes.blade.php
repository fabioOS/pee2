@extends('web.main')
@section('title', "PEE | Termes d'utilisation")

@section('css')
@endsection

@section('js')
@endsection

@section('content')

<div class="breadcrumb-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="breadcrumb-list">
                    <nav aria-label="breadcrumb" class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('web')}}">Accueil</a></li>
                            <li class="breadcrumb-item" aria-current="page">Termes d'utilisation</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12">
                <h1 class="mb-0">Termes d'utilisation</h1>
            </div>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="terms-content">
                    <div class="terms-text">
                        <h3>Date d'entrée en vigueur : <span>20 mai 2023</span></h3>
                        <p>Merci de lire attentivement les termes d'utilisation suivants avant d'utiliser notre plateforme de formation des étudiants entrepreneurs. En utilisant cette plateforme, vous acceptez de vous conformer aux termes et conditions énoncés ci-dessous.</p>
                        <h4>Accès et utilisation</h4>
                        <p>Vous devez vous inscrire sur la plateforme en fournissant des informations précises, complètes et à jour. Vous êtes responsable de la confidentialité de votre compte et de vos informations d'identification. Vous acceptez de ne pas partager vos informations d'identification avec des tiers et de nous informer immédiatement en cas d'utilisation non autorisée de votre compte.</p>
                        <h4>Utilisation appropriée</h4>
                        <p>Vous vous engagez à utiliser la plateforme de manière appropriée et conforme à nos politiques. Vous ne devez pas utiliser la plateforme pour diffuser, partager ou accéder à un contenu illégal, diffamatoire, frauduleux, offensant, obscène, pornographique ou qui viole les droits de propriété intellectuelle d'autrui.</p>
                        <h4>Propriété intellectuelle</h4>
                        <p>Tout le contenu disponible sur la plateforme, y compris les cours, les ressources, les vidéos, les documents et les éléments visuels, est protégé par des droits d'auteur et d'autres lois sur la propriété intellectuelle. Vous acceptez de respecter ces droits et de ne pas utiliser, copier, reproduire ou distribuer ce contenu sans autorisation.</p>
                        <h4>Responsabilité</h4>
                        <p>Nous nous efforçons de fournir des informations précises et à jour sur la plateforme, mais nous ne garantissons pas l'exactitude, l'exhaustivité ou la fiabilité de ce contenu. Vous reconnaissez que l'utilisation des informations fournies sur la plateforme est à vos propres risques. Nous déclinons toute responsabilité pour les pertes ou dommages résultant de votre utilisation de la plateforme ou de votre confiance en son contenu.</p>
                        <h4>Confidentialité</h4>
                        <p>Nous respectons votre vie privée et nous engageons à protéger vos informations personnelles conformément à notre politique de confidentialité. Veuillez consulter notre politique de confidentialité pour en savoir plus sur la collecte, l'utilisation et la divulgation de vos informations.</p>
                        <h4>Modification des termes</h4>
                        <p>Nous nous réservons le droit de modifier ces termes d'utilisation à tout moment. Les modifications prendront effet dès leur publication sur la plateforme. Il est de votre responsabilité de consulter régulièrement les termes d'utilisation pour être informé des modifications. Votre utilisation continue de la plateforme après la publication des modifications constitue votre acceptation de ces modifications.</p>
                        <h4>Résiliation</h4>
                        <p>Nous nous réservons le droit de résilier ou de suspendre votre accès à la plateforme à tout moment, pour quelque raison que ce soit, sans préavis ni responsabilité.</p>
                        <h4>Contact</h4>
                        <p>Si vous avez des questions, des préoccupations ou des commentaires concernant ces termes d'utilisation, veuillez nous contacter à l'adresse e-mail suivante : <a href="mailto:infos@pee-ci.com">infos@pee-ci.com</a>. Nous nous efforcerons de répondre à vos demandes dans les meilleurs délais.</p>
                        <h4>Modifications de la plateforme</h4>
                        <p>Nous nous réservons le droit de modifier, de mettre à jour ou de supprimer tout aspect de la plateforme de formation des étudiants entrepreneurs, y compris les fonctionnalités, le contenu ou les heures d'accessibilité, sans préavis ni responsabilité. Nous nous efforcerons de vous informer de tout changement majeur affectant votre utilisation de la plateforme.</p>
                        <h4>Indemnisation</h4>
                        <p>Vous acceptez d'indemniser et de dégager de toute responsabilité notre entreprise, ses dirigeants, ses employés et ses partenaires contre toute réclamation, tout dommage, toute responsabilité, tout coût ou toute dépense découlant de votre utilisation de la plateforme ou de toute violation de ces termes d'utilisation.</p>
                        <h4>Intégralité de l'accord</h4>
                        <p>Ces termes d'utilisation constituent l'accord complet entre vous et notre entreprise concernant votre utilisation de la plateforme de formation des étudiants entrepreneurs, et ils remplacent tout accord antérieur ou concurrent. Aucune modification de ces termes d'utilisation ne sera contraignante à moins d'être faite par écrit et signée par les deux parties.</p>
                        <h4>&nbsp;</h4>
                        <p> <i>En acceptant ces termes d'utilisation, vous reconnaissez avoir lu, compris et accepté d'être lié par ces conditions. Si vous n'acceptez pas ces termes, veuillez ne pas utiliser notre plateforme de formation des étudiants entrepreneurs.</i></p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
