@extends('layouts.app')

@section('title')
    Actualités
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .activ{
            color: red;
        }
    </style>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });
        });

        $('.actus').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cet article",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", true],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()!='action_groupp'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes !","error" )
                }
                else {
                    if($('#actionGroup').val()=='del_select'){
                        //$("#loading").show();
                        swal({
                            title: "Êtes-vous sûr?",
                            text: "Voulez vous vraiment supprimer ces lignes",
                            icon: "warning",
                            buttons: true,
                            buttons: ["Annuler", "Oui"],
                            dangerMode: true,
                        }).then((willDelete) => {
                            if(willDelete) {
                                var valueIds = allVals.join(",");

                                $.ajax({
                                    url:"{{ route('news.deletes') }}",
                                    method:"GET",
                                    data:{value:valueIds},
                                    success:function(res){
                                        console.log(res);
                                        if(res == '1'){
                                            //$("#loading").hide();
                                            location.reload();
                                        }else{
                                            //$("#loading").hide();
                                            location.reload();
                                        }
                                    }
                                })
                            }
                        });
                    }else if($('#actionGroup').val()=='broui_select'){
                        swal({
                            title: "Êtes-vous sûr?",
                            text: "Voulez vous vraiment mettre en brouillons ces lignes ",
                            icon: "warning",
                            buttons: true,
                            buttons: ["Annuler", "Oui"],
                            dangerMode: true,
                        }).then((willDelete) => {
                            if(willDelete) {
                                var valueIds = allVals.join(",");

                                $.ajax({
                                    url:"{{ route('news.brouillons') }}",
                                    method:"GET",
                                    data:{value:valueIds},
                                    success:function(res){
                                        console.log(res);
                                        if(res == '1'){
                                            //$("#loading").hide();
                                            location.reload();
                                        }else{
                                            //$("#loading").hide();
                                            location.reload();
                                        }
                                    }
                                })
                            }
                        });
                    }else if($('#actionGroup').val()=='retablie_select'){
                        swal({
                            title: "Êtes-vous sûr?",
                            text: "Voulez vous vraiment retablir ces lignes ",
                            icon: "warning",
                            buttons: true,
                            buttons: ["Annuler", "Oui"],
                            dangerMode: true,
                        }).then((willDelete) => {
                            if(willDelete) {
                                var valueIds = allVals.join(",");

                                $.ajax({
                                    url:"{{ route('news.retablirs') }}",
                                    method:"GET",
                                    data:{value:valueIds},
                                    success:function(res){
                                        console.log(res);
                                        if(res == '1'){
                                            //$("#loading").hide();
                                            location.reload();
                                        }else{
                                            //$("#loading").hide();
                                            location.reload();
                                        }
                                    }
                                })
                            }
                        });
                    }else{
                        //$('#actionGroup').val()=='publie_select'
                        swal({
                            title: "Êtes-vous sûr?",
                            text: "Voulez vous vraiment publier ces lignes",
                            icon: "warning",
                            buttons: true,
                            buttons: ["Annuler", "Oui"],
                            dangerMode: true,
                        }).then((willDelete) => {
                            if(willDelete) {
                                var valueIds = allVals.join(",");

                                $.ajax({
                                    url:"{{ route('news.publies') }}",
                                    method:"GET",
                                    data:{value:valueIds},
                                    success:function(res){
                                        console.log(res);
                                        if(res == '1'){
                                            //$("#loading").hide();
                                            location.reload();
                                        }else{
                                            //$("#loading").hide();
                                            location.reload();
                                        }
                                    }
                                })
                            }
                        });
                    }
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });


    </script>


@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">
                            Articles
                            <a href="{{route('news.create')}}" class="btn btn-default btn-xs">Ajouter</a>
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <ul class="subsubsub">
                                <li><a href="{{route('news')}}">Tous ({{$countAllActus}}) |</a></li>
                                <li><a href="{{route('news.show_type','publies')}}" class="{{$id=='publies' ? 'activ' : ''}}">Publiés ({{$countPublie}}) |</a></li>
                                <li><a href="{{route('news.show_type','corbeille')}}" class="{{$id=='corbeille' ? 'activ' : ''}}">Corbeille ({{$countDel}}) |</a></li>
                                <li><a href="{{route('news.show_type','brouillons')}}" class="{{$id=='brouillons' ? 'activ' : ''}}">Brouillons ({{$countBrouille}}) </a></li>
                            </ul>

                            <div class="form-inline">
                                <div class="form-group">
                                    <select name="actionGroup" class="form-control" id="actionGroup">
                                        <option value="action_groupp">Actions groupée</option>
                                        @if($id=='publies')
                                            <option value="broui_select">Mettre en brouillons</option>
                                            <option value="del_select">Déplacer dans la corbeille</option>
                                        @elseif($id=='corbeille')
                                            <option value="retablie_select">Rétablir</option>
                                        @elseif($id=='brouillons')
                                            <option value="publie_select">Publier</option>
                                        @endif
                                    </select>
                                </div>
                                <button type="submit" id="sendActionGroup" class="btn btn-default waves-effect waves-light m-l-10 btn-md">Appliquer</button>
                            </div>
                            <br>



                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    {{--<th>Ref.No.</th>--}}
                                    <th> <input type="checkbox" id="check_all"></th>
                                    <th>Titre</th>
                                    <th>Date publication</th>
                                    <th>Auteur</th>
                                    <th>Catégories</th>
                                    <th>Date creation</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($actus as $k=>$actu)
                                    <tr>
                                        {{--<td>#0{{$k+1}} </td>--}}
                                        <td>
                                            @if(Auth::user()->role_id=='3')
                                                @if(Auth::user()->id==$actu->auteur)
                                                    <input type="checkbox" class="role_item" data-id="{{$actu->id}}">
                                                @endif
                                            @else
                                                <input type="checkbox" class="role_item" data-id="{{$actu->id}}">
                                            @endif
                                        </td>
                                        <td style="font-weight: bold">
                                            {{$actu->title}}

                                            @if(Auth::user()->role_id=='3')
                                                @if(Auth::user()->id==$actu->auteur)
                                                    @if($id=='corbeille')
                                                        <div class="row-actions-edit">
                                                            <span class="retablir"> <a href="{{route('news.retablir',$actu->id)}}">Rétablir</a></span>
                                                        </div>
                                                    @else
                                                        <div class="row-actions-edit">
                                                            <span class="edit"> <a href="{{route('news.edit',$actu->id)}}">Modifier</a> | </span>
                                                            <span class="trash"><a href="{{route('news.delete',$actu->id)}}" class="delete" style="color: red">Corbeille</a> | </span>
                                                            <span class="view"><a href="{{route('news.show',$actu->slug)}}" rel="afficher">Afficher</a></span>
                                                        </div>
                                                    @endif
                                                @else
                                                    <div class="row-actions-edit">
                                                        <span class="view"><a href="{{route('news.show',$actu->slug)}}" rel="afficher">Afficher</a></span>
                                                    </div>
                                                @endif
                                            @else
                                                @if($id=='corbeille')
                                                    <div class="row-actions-edit">
                                                        <span class="retablir"> <a href="{{route('news.retablir',$actu->id)}}">Rétablir</a></span>
                                                    </div>
                                                @else
                                                    <div class="row-actions-edit">
                                                        <span class="edit"> <a href="{{route('news.edit',$actu->id)}}">Modifier</a> | </span>
                                                        <span class="trash"><a href="{{route('news.delete',$actu->id)}}" class="delete" style="color: red">Corbeille</a> | </span>
                                                        <span class="view"><a href="{{route('news.show',$actu->slug)}}" rel="afficher">Afficher</a></span>
                                                    </div>
                                                @endif
                                            @endif

                                        </td>

                                        <td>{{date('d/m/Y',strtotime($actu->date))}}</td>
                                        <td>{{$actu->auteur}}</td>
                                        {{-- @php $cats = (new \App\Http\Controllers\NewsController())->getCatlibell($actu->id) @endphp --}}
                                        <td>
                                            {{$actu->categories->implode('libelle',',')}}
                                        </td>
                                        <td>{{date("d/m/Y",strtotime($actu->created_at))}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
