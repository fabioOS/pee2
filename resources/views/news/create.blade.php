@extends('layouts.app')

@section('title')
    Ajouter un nouvel article
    @parent
@stop

@section('header_styles')
{{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}


@endsection

@section('footer_scripts')
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}
    <!--form validation init-->
    <script src="{{ url('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        var loadFile = function(event) {
            $("#output").show();
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

        function loadVideoImg(){
            var lien = $("#linkvideo").val();
            var out = document.getElementById('outputvideo');
                out.src = null;
            var search = lien.split("?v=");
            var result = search[1];
            if(result){
                //console.log(search);
                var thumbnail="http://img.youtube.com/vi/"+result+"/maxresdefault.jpg";
                $("#outputvideo").show();
                out.src = thumbnail;
            }

        }

        $("#type_article").change(function () {

            if ($("#type_article").val()=='text'){
                $("#vdAvant").hide();
                $("#imgAvant").show();
            }else{
                $("#imgAvant").hide();
                $("#vdAvant").show();
            }
        });

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Ajouter un nouvel article</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{route('news.store')}}" role="form">
                        @csrf
                        <div class="col-md-8">
                            <div class="card-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="type_article">Type d'actualité <span class="text-danger">*</span></label>
                                                <select required class="form-control col-md-12" id="type_article" name="type_article">
                                                    <option value="text">Text</option>
                                                    <option value="video">Video</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="type_article">Saisissez votre titre ici <span class="text-danger">*</span></label>
                                                <input type="text" required value="{{old('title')}}" name="title" class="form-control col-md-12" placeholder="Saisissez votre titre ici">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea class="form-control ckeditor" required name="description" rows="10" cols="50">{{old('description')}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="date">Date de publication <span class="text-danger">*</span></label>
                                                <input type="date" required name="date" id="date" value="{{old('date')}}" class="form-control col-md-12">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="auteur">Auteur</label>
                                                <input type="text" name="auteur" id="auteur" value="{{old('auteur')}}" class="form-control col-md-12">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 m-b-20 header-title"><b>Catégories</b> <span class="text-danger">*</span></h4>
                                    <hr>
                                    <div class="row" style="max-height: 103px;overflow: auto;">
                                        @foreach($cats as $cat)
                                        <div class="checkbox checkbox-primary">
                                            <input name="catid[]" id="catid{{$cat->id}}" value="{{$cat->id}}" type="checkbox">
                                            <label for="catid{{$cat->id}}"> {{$cat->libelle}} </label>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" id="imgAvant">
                                <div class="card-box">
                                    <h4 class="m-t-0 m-b-20 header-title"><b>Image mise en avant</b></h4>
                                    <hr>
                                    <div class="form-group">
                                        {{--<input type="file" class="filestyle" data-buttonname="btn-white">--}}
                                        <label for="file" class="input input-file">
                                            <div class="button">
                                                {{--Selectionnez une image--}}
                                                <input type="file" class="filestyle" data-buttonname="btn-white" accept='image/*' id="file" name="fileUser" onchange="loadFile(event)">
                                            </div>
                                        </label>
                                        <img id="output" src="" alt="" class="img-responsive" style="display: none;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" id="vdAvant" style="display: none;">
                                <div class="card-box">
                                    <h4 class="m-t-0 m-b-20 header-title"><b>Video mise en avant</b></h4>
                                    <hr>
                                    <div class="form-group">
                                        <label for="file" class="input input-file">
                                            <div class="button">
                                                <label class="col-md-12" for="type_article">Saisissez votre lien</label>
                                                <input type="url" value="{{old('linkvideo')}}" class="form-control" id="linkvideo" name="linkvideo" onkeyup="loadVideoImg()">
                                            </div>
                                        </label>
                                        <img id="outputvideo" src="" alt="" class="img-responsive" style="display: none;">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 m-b-20 header-title"><b>Publier</b></h4>
                                    <hr>
                                    <div class="row">
                                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light" name="btn" value="brouillon"><i class="fa fa-check-square"></i> Enregistrer le brouillon </button>
                                        <br><br><button type="submit" class="btn btn-primary btn-md waves-effect waves-light" name="btn" value="apercu"><i class="fa fa-eye"></i> Prévisualiser </button>
                                        <br><br><button type="submit" class="btn btn-primary btn-md waves-effect waves-light" name="btn" value="publier"><i class="fa fa-save"></i> Publier</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
