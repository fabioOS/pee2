@extends('layouts.app')

@section('title')
    {{$actu->title}}
    @parent
@stop

@section('header_styles')
    {{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}


@endsection

@section('footer_scripts')
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}

@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-md-8 col-md-offset-1">
                        @if($actu->type_article=='text')
                        <img src="{{asset('articles/'.$actu->img)}}" alt="{{$actu->title}}" class="img-responsive img-me">
                        @else
                        <img src="{{$actu->img)}}" alt="{{$actu->title}}" class="img-responsive img-me">
                        @endif
                        <div class="card-box m-t-20">
                            <h1>{{$actu->title}}</h1>
                            <div class="w-blogpost-meta">
                                <span class="w-blogpost"><i class="fa fa-clock-o"></i> {{$actu->created_at->format('d/m/Y')}}</span>
                                <span class="w-blogpost"><i class="fa fa-user"></i> {{$actu->auteur}}</span>
                                {{-- @php $cats = (new \App\Http\Controllers\NewsController())->getCatlibell($actu->cat_id) @endphp --}}

                                <span class="w-blogpost"><i class="fa fa-folder"></i>
                                    {{$actu->categories->implode('libelle',',')}}
                                </span>
                            </div>

                            <div class="row m-t-40">
                                {!! $actu->des !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
