


@extends('web.errorlayout')

@section('page_title')
    Erreur 419 | {{ env('APP_NAME') }}
@stop

@section('content')
        <div class="error-logo-">
            <a href="@if(isset($_SERVER['HTTP_REFERER'])) {{$_SERVER['HTTP_REFERER']}} @else {{route('home')}} @endif">
                <img src="{{asset('assets/fronts/img/logo.png')}}" class="img-fluid" alt="Logo">
            </a>
        </div>
        <div class="error-box-img">
            <p>&nbsp;</p>
            {{-- <img src="{{asset('assets/fronts/img/error.png')}}" alt="" class="img-fluid" > --}}
        </div>
        <h3 class="h2 mb-3"> Oops! Erreur 419</h3>
        <p class="h4 font-weight-normal">La page que vous tentez d'afficher a expiré !</p>
        <a class="btn btn-primary waves-effect waves-light" href="@if(isset($_SERVER['HTTP_REFERER'])) {{$_SERVER['HTTP_REFERER']}} @else {{route('web')}} @endif"> <i class="fas fa-refresh"></i> &nbsp; Actualiser </a>

@endsection



