@extends('layouts.app')

@section('title')
    Profil
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    {{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link rel="stylesheet" href="{{ asset('assets/plugins/dropify/dropify.css') }}">

@endsection

@section('footer_scripts')

    {{--<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>--}}
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/dropify/dropify.js') }}"></script>
    <script>
        $('.dropify').dropify();
    </script>

@endsection


@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="wraper container">

                <div class="row">
                    <div class="col-md-4 col-lg-3">
                        <div class="profile-detail card-box">
                            <div>
                                <img src="{{Auth::user()->img ? asset('assets/userAvatar/'.Auth::user()->img.'')  : asset('assets/images/users/user.jpeg')}}" class="img-circle" alt="profile-image">
                                <hr>
                                <h4 class="text-uppercase font-600">{{Auth::user()->name}}</h4>
                                <p class="text-muted font-13 m-b-30">
                                    {{Auth::user()->email}}
                                </p>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-9 col-md-8">
                        <div class="card-box">
                            <ul class="nav nav-tabs tabs">
                                <li class="active tab">
                                    <a href="#home-2" data-toggle="tab" aria-expanded="false">
                                        <span class="hidden-xs">Modifier mon profil</span>
                                    </a>
                                </li>
                                <li class="tab">
                                    <a href="#profile-2" data-toggle="tab" aria-expanded="false">
                                        <span class="hidden-xs">Changer de mot de passe</span>
                                    </a>
                                </li>
                                <li class="tab">
                                    <a href="#messages-2" data-toggle="tab" aria-expanded="true">
                                        <span class="hidden-xs">Mon avatar</span>
                                    </a>
                                </li>

                            </ul>
                            <div class="tab-content">
                                    <div class="tab-pane active" id="home-2">
                                        <form class="form-horizontal" role="form" action="{{route('profiles.save')}}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Nom</label>
                                                <div class="col-md-10">
                                                    <input type="text" name="nom" class="form-control" value="{{Auth::user()->name}}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Sexe</label>
                                                <div class="col-md-10">
                                                    <select class="form-control" name="sexe">
                                                        <option value="Homme" {{Auth::user()->sexe=='Homme' ? 'selected' : '' }}>Homme</option>
                                                        <option value="Femme" {{Auth::user()->sexe=='Femme' ? 'selected' : '' }}>Femme</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group m-b-0">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-default waves-effect waves-light">Enregistrer</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="profile-2">
                                        <form class="form-horizontal" role="form" action="{{route('profiles.savepass')}}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Mot de passe actuel</label>
                                                <div class="col-md-8">
                                                    <input type="password" name="motpass" required="required" class="form-control" placeholder="Mot de passe actuel">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Nouveau mot de passe</label>
                                                <div class="col-md-8">
                                                    <input type="password" name="newpass" required="required" class="form-control" placeholder="Nouveau mot de passe">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Confirmer mot de passe</label>
                                                <div class="col-md-8">
                                                    <input type="password" name="confirmpass" required="required" class="form-control" placeholder="Confirmer mot de passe">
                                                </div>
                                            </div>

                                            <div class="form-group m-b-0">
                                                <div class="col-sm-offset-4 col-sm-8">
                                                    <button type="submit" class="btn btn-default waves-effect waves-light">Enregistrer</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="messages-2">
                                        <form class="form-horizontal" enctype="multipart/form-data" role="form" action="{{route('profiles.saveavatar')}}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <input type="file" id="fileUser" name="fileUser" class="dropify" data-default-file="{{ Auth::user()->img  ? asset('assets/userAvatar/'.Auth::user()->img.'') : asset('assets/images/users/user.jpeg') }}"/>
                                                </div>
                                            </div>
                                            <div class="form-group m-b-0">
                                                <div class="pull-right">
                                                    <button type="submit" class="btn btn-default waves-effect waves-light">Enregistrer</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                        </div>
                    </div>

                </div>



            </div> <!-- container -->

        </div> <!-- content -->

        

    </div>
@endsection
