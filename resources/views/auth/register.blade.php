<!DOCTYPE html>
<html lang="fr">
	<head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<title>PEE | Créer un compte</title>

		<link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/fronts/img/favicon.png') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/fontawesome/css/fontawesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/fontawesome/css/all.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/owl.carousel.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/owl.theme.default.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/fronts/plugins/feather/feather.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/fronts/css/style.css') }}">

	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper log-wrap">

			<div class="row">

				<!-- Login Banner -->
				<div class="col-md-6 login-bg">
					<div class="owl-carousel login-slide owl-theme">
						<div class="welcome-login">
							<div class="login-banner">
								<img src="{{asset('assets/fronts/img/login-img-2.png')}}" class="img-fluid" alt="Logo">
							</div>
							<div class="mentor-course text-center">
								<h2>Bienvenu sur PEE<br>Pôle de l'Étudiant Entrepreneur.</h2>
								<h5>Créateur et Bâtisseur d'Avenirs</h5>
								<p>Formations - Coaching - Incubation - Éducation Financière</p>
							</div>
						</div>
						<div class="welcome-login">
							<div class="login-banner">
								<img src="{{asset('assets/fronts/img/login-img.png')}}" class="img-fluid" alt="Logo">
							</div>
							<div class="mentor-course text-center">
								<h2>Bienvenu sur PEE<br>Pôle de l'Étudiant Entrepreneur.</h2>
								<h5>Créateur et Bâtisseur d'Avenirs</h5>
								<p>Formations - Coaching - Incubation - Éducation Financière</p>
							</div>
						</div>
						<div class="welcome-login">
							<div class="login-banner">
								<img src="{{asset('assets/fronts/img/login-img-3.png')}}" class="img-fluid" alt="Logo">
							</div>
							<div class="mentor-course text-center">
								<h2>Bienvenu sur PEE<br>Pôle de l'Étudiant Entrepreneur.</h2>
								<h5>Créateur et Bâtisseur d'Avenirs</h5>
								<p>Formations - Coaching - Incubation - Éducation Financière</p>
							</div>
						</div>

					</div>
				</div>
				<!-- /Login Banner -->

				<div class="col-md-6 login-wrap-bg">

					<!-- Login -->
					<div class="login-wrapper">
						<div class="loginbox">
							<div class="w-100">
								<div class="img-logo">
									<img src="{{asset('assets/fronts/img/logo.png')}}" class="img-fluid" style="width:360px;" alt="Logo">
									<div class="back-home">
										<a href="{{route('web')}}">Retour au site</a>
									</div>
								</div>
								<h1>Créer un compte étudiant</h1>
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        @include('ens.include.messages')
                                        @include('ens.include.errors')
                                    </div>
                                </div>
                                <form action="{{route('w.store_user_etud')}}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label class="form-control-label">Nom complet <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" required name="nm1" placeholder="Entrez votre nom complet">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Email <span class="text-danger">*</span></label>
                                        <input type="email" class="form-control" required name="em2" placeholder="Entrez votre email">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Mot de passe <span class="text-danger">*</span></label>
                                        <div class="pass-group" id="passwordInput">
                                            <input type="password" class="form-control pass-input" required name="pass3" placeholder="Entrez votre mot de passe">
                                            <span class="toggle-password feather-eye"></span>
                                            <span class="pass-checked"><i class="feather-check"></i></span>
                                        </div>
                                        <div  class="password-strength" id="passwordStrength">
                                            <span id="poor"></span>
                                            <span id="weak"></span>
                                            <span id="strong"></span>
                                            <span id="heavy"></span>
                                        </div>
                                        <div id="passwordInfo"></div>
                                    </div>

                                    <div class="form-check remember-me">
                                        <label class="form-check-label mb-0">
                                        <input class="form-check-input" type="checkbox" name="remember" required> J'accepte les  <a href="{{route('w.terme')}}" target="_blank">termes et conditions d'utilisation</a> et <a href="{{route('w.politique')}}" target="_blank">la politique de confidentialité.</a>
                                        </label>
                                    </div>
                                    <div class="col-md-12" id="reCAPTCHA-Container" style="margin-top: 10px;">Chargement de Recaptcha...</div>
                                    <div id="reCAPTCHA-error" class="text-red">&nbsp;</div>
                                    <div class="d-grid">
                                        <button class="btn btn-primary btn-start" type="submit">Créer votre compte</button>
                                    </div>
                                </form>
							</div>
						</div>
						<div class="google-bg text-center">
							{{-- <span><a href="#">Ou connectez-vous avec</a></span>
							<div class="sign-google">
								<ul>
									<li><a href="#"><img src="{{asset('assets/fronts/img/net-icon-01.png')}}" class="img-fluid" alt="Logo"> Sign In using Google</a></li>
									<li><a href="#"><img src="{{asset('assets/fronts/img/net-icon-02.png')}}" class="img-fluid" alt="Logo">Sign In using Facebook</a></li>
								</ul>
							</div> --}}
							<p class="mb-0">Vous avez déjà un compte? <a href="{{route('login')}}">Connectez-vous</a></p>
						</div>
					</div>
					<!-- /Login -->

				</div>

			</div>

	   </div>
	   <!-- /Main Wrapper -->

       <script src="{{asset('assets/fronts/js/jquery-3.6.0.min.js')}}"></script>
		<script src="{{asset('assets/fronts/js/bootstrap.bundle.min.js')}}"></script>
		<script src="{{asset('assets/fronts/js/owl.carousel.min.js')}}"></script>
		<script src="{{asset('assets/fronts/js/script.js')}}"></script>
		<script src="{{asset('assets/fronts/js/validation.js')}}"></script>
		<script src="{{asset('assets/fronts/js/script.js')}}"></script>

	</body>
</html>
