<!-- ========== Left Sidebar Start ========== -->

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>

                <li> <a href="{{route('home')}}"><i class="fa fa-dashboard"></i> <span> Tableau de bord </span></a></li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-text"></i><span> Articles </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('news')}}">Tous les articles</a></li>
                        <li><a href="{{route('news.create')}}">Ajouter</a></li>
                        @if(Auth::user()->role_id==1)
                        <li><a href="{{route('categories')}}">Catégories</a></li>
                        @endif
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="icon-user-following"></i><span> Étudiants </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('etudiants')}}">Tous les étudiants</a></li>
                        <li><a href="{{route('etudiants.create')}}">Ajouter</a></li>
                        <li><a href="{{route('etudiants.import')}}">Importer</a></li>
                        <li><a href="#{{route('etudiants.indicateurs')}}">Indicateurs</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ion-android-social"></i><span> Formateurs </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('enseignants')}}">Tous les formateurs</a></li>
                        <li><a href="{{route('enseignants.create')}}">Ajouter</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-book"></i><span> Formations </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('formations')}}">Toutes les formations</a></li>
                        <li><a href="{{route('formations.create')}}">Ajouter</a></li>
                        <li><a href="{{route('formations.demandes')}}">Voir les demandes</a></li>
                        <li><a href="{{route('formations.cats')}}">Catégories</a></li>
                    </ul>
                </li>

                @if(Auth::user()->role_id==1)

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-users"></i><span> Utilisateurs </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('users')}}">Tous les utilisateurs</a></li>
                        <li><a href="{{route('users.create')}}">Ajouter</a></li>
                        <li><a href="{{route('roles')}}">Rôles</a></li>
                    </ul>
                </li>

                {{-- <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-image"></i><span> Médiathèques </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('albums')}}">Photos</a></li>
                        <li><a href="{{route('videos')}}">Vidéos</a></li>
                    </ul>
                </li> --}}


                <li> <a href="{{route('partnaire')}}"><i class="fa icon-people"></i> <span> Partenaires </span></a></li>
                <li> <a href="{{route('testimonials')}}"><i class="fa fa-comment"></i> <span> Témoignages </span></a></li>
                {{-- <li> <a href="{{route('offres')}}"><i class="fa fa-file-alt"></i> <span> Offres </span></a></li> --}}
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file"></i><span> Offres </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('offres')}}">Toutes les offres</a></li>
                        <li><a href="{{route('offres.create')}}">Ajouter</a></li>
                        <li><a href="{{route('offres.cats')}}">Catégories</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-bullhorn"></i><span> Réalisations </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('promo')}}">Toutes les réalisations</a></li>
                        <li><a href="{{route('promo.create')}}">Ajouter</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-envelope"></i><span> Messages </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('message')}}">Toutes les messages</a></li>
                        <li><a href="{{route('message.create')}}">Envoyer un message</a></li>
                    </ul>
                </li>
           @endif

            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
