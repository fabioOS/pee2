@extends('layouts.app')

@section('title')
    Ajouter une offre
    @parent
@stop

@section('header_styles')
{{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}


@endsection

@section('footer_scripts')
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}
    <!--form validation init-->
    {{-- <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script> --}}
    <script src="{{ url('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Ajouter une offre</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{route('offres.store')}}">
                        @csrf
                        <div class="col-md-8">
                            <div class="card-box">
                                <div class="row">
                                    <div class="col-md-12">


                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="type_article">Poste <span class="text-danger">*</span></label>
                                                <input type="text" required value="{{old('poste')}}" name="poste" class="form-control col-md-12" placeholder="Saisissez le poste">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="niveau">Niveau <span class="text-danger">*</span></label>
                                                <input type="text" required value="{{old('niveau')}}" name="niveau" class="form-control col-md-12" placeholder="Saisissez le niveau">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="experience">Experience <span class="text-danger">*</span></label>
                                                <input type="text" required value="{{old('experience')}}" name="experience" class="form-control col-md-12" placeholder="Saisissez le niveau">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="col-md-12" for="lieu">Lieu <span class="text-danger">*</span></label>
                                                <input type="text" required value="{{old('lieu')}}" name="lieu" class="form-control col-md-12" placeholder="Saisissez le niveau">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="datepublie">Date de publication <span class="text-danger">*</span></label>
                                                <input type="date" required max="{{date('Y-m-d')}}" name="datepublie" id="datepublie" value="{{old('datepublie')}}" class="form-control col-md-12">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label for="datelimite">Date limite <span class="text-danger">*</span></label>
                                                <input type="date" required min="{{date('Y-m-d')}}" name="datelimite" id="datelimite" value="{{old('datelimite')}}" class="form-control col-md-12">
                                            </div>
                                        </div>

                                        <br>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                {{-- <textarea id="elm1" class="form-control" required name="description" rows="10" cols="50">{{old('description')}}</textarea> --}}
                                                <textarea name="description" required class="ckeditor" id="description" cols="30" rows="10">{{old('description')}}</textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 m-b-20 header-title"><b>Types d'offre</b> <span class="text-danger">*</span></h4>
                                    <hr>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select required class="form-control col-md-12" id="types" name="types">
                                                <option selected disabled>Selection un type</option>
                                                <option value="1">Emplois</option>
                                                <option value="2">Stages</option>
                                                <option value="3">Financement</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 m-b-20 header-title"><b>Catégories</b> <span class="text-danger">*</span></h4>
                                    <hr>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <select required class="form-control col-md-12" id="catid" name="catid">
                                                <option selected disabled>Selection une catégorie</option>
                                                @foreach($cats as $cat)
                                                <option value="{{$cat->id}}">{{$cat->libelle}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 m-b-20 header-title"><b>Publier</b></h4>
                                    <hr>
                                    <div class="row">
                                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light"><i class="fa fa-check-square"></i> Enregistrer </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
