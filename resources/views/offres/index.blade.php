@extends('layouts.app')

@section('title')
    Offres
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });
        });

        $('.catgries').on('click', '.delete', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            swal({
                title: "Êtes-vous sûr?",
                text: "Voulez vous vraiment supprimer cette offre ?",
                icon: "warning",
                buttons: true,
                buttons: ["Annuler", "Oui"],
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    window.location = href;
                }
            });
        });

        $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        });

        $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()=='del_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à supprimer!","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment supprimer ces lignes",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if(willDelete) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('categories.deletes') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        });


    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">
                            Offres
                            <a href="{{route('offres.create')}}" class="btn btn-default btn-xs">Ajouter</a>
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">

                            <table id="myTable" class="table table-striped table-bordered catgries">
                                <thead>
                                <tr>
                                    <th>Ref.No.</th>
                                    <th>Code</th>
                                    <th>Poste</th>
                                    <th>Date</th>
                                    <th>Détails</th>
                                    <th>Catégorie</th>
                                    <th>Type</th>
                                    <th>Statut</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($offres as $k=>$item)
                                    <tr>
                                        <td>#0{{$k+1}} </td>
                                        <td>{{$item->code}}</td>
                                        <td style="font-weight: bold">
                                            {{$item->poste}}
                                            <div class="row-actions-edit">
                                                <span class="edit"> <a href="{{route('offres.edit',$item->id)}}">Modifier</a> | </span>
                                                <span class="trash"><a href="{{route('offres.delete',$item->id)}}" class="delete" style="color: red">Supprimer</a></span>
                                            </div>
                                        </td>
                                        <td>
                                            <span>Date publication : <strong>{{date('d/m/Y',strtotime($item->datepublie))}}</strong></span> <br>
                                            <span>Date limite : <strong>{{date('d/m/Y',strtotime($item->datelimite))}}</strong></span> <br>
                                        </td>
                                        <td>
                                            <span>Niveau : <strong>{{$item->niveau}}</strong></span> <br>
                                            <span>Experience : <strong>{{$item->experience}}</strong></span> <br>
                                            <span>Lieu : <strong>{{$item->lieu}}</strong></span>
                                        </td>
                                        <td>{{$item->categorie->libelle}}</td>
                                        <td>
                                            @if($item->type==1)
                                                <span class="badge badge-info">Emplois</span>
                                            @elseif ($item->type==2)
                                                <span class="badge badge-primary">Stages</span>
                                            @else
                                                <span class="badge badge-default">Financement</span>
                                            @endif

                                        </td>
                                        <td>
                                            @if($item->statut==1)
                                                <span class="badge badge-success">Actif</span>
                                            @else
                                                <span class="badge badge-danger">inactif</span>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
