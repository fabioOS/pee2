@extends('layouts.app')

@section('title')
    Liste des témoignages
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('assets/plugins/dropify/dropify.css') }}">

@endsection

@section('footer_scripts')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

<!-- Sweet-Alert  -->
<script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropify/dropify.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });


            $('.actus').on('click', '.delete', function (event) {
                event.preventDefault();
                var href = $(this).attr('href');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez vous vraiment supprimer cet témoignage",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });
        });


    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Témoignages
                            <a href="{{route('testimonials.create')}}" class="btn btn-default btn-xs">Ajouter</a>
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">
                            <table id="myTable" class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th>Ref.No.</th>
                                    <th>Photo</th>
                                    <th>Nom et Prénom(s)</th>
                                    <th>Fonction</th>
                                    <th>Contenu</th>
                                    <th>Date d'ajout</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($testimonials as $k => $testimonial)
                                    <tr>
                                        <td>#0{{$k+1}} </td>
                                        <td><img class="img-responsive" src="{{ asset('assets/temoignages/'.$testimonial->photo) }}" alt="{{ $testimonial->nom }}" style="width:100px;height: 100px"></td>
                                        <td style="font-weight: bold">
                                            {{$testimonial->nom}}
                                            <div class="row-actions-edit">
                                                <span class="edit"> <a href="{{route('testimonials.edit',$testimonial->id)}}">Modifier</a> | </span>
                                                <span class="trash"><a href="{{route('testimonials.delete',$testimonial->id)}}" class="delete" style="color: red">Supprimer</a> </span>
                                            </div>
                                        </td>
                                        <td>{{ $testimonial->fonction }}</td>
                                        <td>{!! Str::limit($testimonial->contenu) !!}</td>
                                        <td>{{ $testimonial->created_at->format('d/m/Y') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
