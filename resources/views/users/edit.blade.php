@extends('layouts.app')

@section('title')
    Modifier un utilisateur
    @parent
@endsection

@section('header_styles')
    {{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}


@endsection

@section('footer_scripts')
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}
    <!--form validation init-->
    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            if($("#elm1").length > 0){
                tinymce.init({
                    selector: "textarea#elm1",
                    theme: "modern",
                    menubar:false,
                    statusbar: false,
                    height:300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link  | print preview media fullpage | forecolor backcolor",
                    style_formats: [
                        {title: 'Bold text', inline: 'b'},
                        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                        {title: 'Example 1', inline: 'span', classes: 'example1'},
                        {title: 'Example 2', inline: 'span', classes: 'example2'},
                        {title: 'Table styles'},
                        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                    ]
                });
            }

            if($("#type_article").val()=='text'){
                $("#vdAvant").hide();
                $("#imgAvant").show();
            }else{
                $("#imgAvant").hide();
                $("#vdAvant").show();
                loadVideoImg();
            }
        });

        var loadFile = function(event) {
            $("#output").show();
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

        function loadVideoImg(){
            var lien = $("#linkvideo").val();
            var out = document.getElementById('outputvideo');
            out.src = null;
            var search = lien.split("?v=");
            var result = search[1];
            if(result){
                //console.log(search);
                var thumbnail="http://img.youtube.com/vi/"+result+"/maxresdefault.jpg";
                $("#outputvideo").show();
                out.src = thumbnail;
            }

        }

        $("#type_article").change(function () {

            if ($("#type_article").val()=='text'){
                $("#vdAvant").hide();
                $("#imgAvant").show();
            }else{
                $("#imgAvant").hide();
                $("#vdAvant").show();
            }
        });

    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Modifier un utilisateur</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <form class="form-horizontal" method="post" action="{{route('users.update')}}" role="form">
                        @csrf
                        <div class="col-md-8">
                            <div class="card-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="nom">Nom</label>
                                                <input type="text" name="nom" value="{{$user->name}}" class="form-control col-md-12" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="email">Email</label>
                                                <input type="email" name="email" value="{{$user->email}}" class="form-control col-md-12" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="password">Mot de passe </label>
                                                <input type="password" name="password" class="form-control col-md-12">
                                            </div>
                                        </div>
                                        {{--<div class="form-group">--}}
                                        {{--<div class="col-sm-12">--}}
                                        {{--<label class="col-md-12" for="comfirm_password">Confirmer Mot de passe </label>--}}
                                        {{--<input type="password" name="comfirm_password" class="form-control col-md-12" required>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="col-md-12" for="role_id">Rôle </label>
                                                <select class="form-control m-input" name="role_id" required>
                                                    <option value="">Selectionnez un rôle</option>
                                                    @foreach($roles as $role)
                                                        <option value="{{$role->id}}" {{$user->role_id==$role->id ? 'selected' : ''}}>{{$role->libelle}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="col-md-12">
                                <div class="card-box">
                                    {{--<h4 class="m-t-0 m-b-20 header-title"><b>Publier</b></h4>
                                    <hr>--}}
                                    <div class="row">
                                        <input type="hidden" name="iduser" value="{{$user->id}}">
                                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light" name="btn" value="add"><i class="fa fa-check-square"></i> Mettre à jour</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
