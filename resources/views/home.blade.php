@extends('layouts.app')

@section('title')
    Admin
    @parent
@stop

@section('header_styles')
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ url('assets/plugins/morris/morris.css') }}">
@endsection

@section('footer_scripts')
    <script src="{{ url('assets/plugins/peity/jquery.peity.min.js') }}"></script>
    <!-- jQuery  -->
    <script src="{{ url('assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ url('assets/plugins/counterup/jquery.counterup.min.js') }}"></script>

    <script src="{{ url('assets/plugins/morris/morris.min.js') }}"></script>
    <script src="{{ url('assets/plugins/raphael/raphael-min.js') }}"></script>

    <script src="{{ url('assets/plugins/jquery-knob/jquery.knob.js') }}"></script>

    <script src="{{ url('assets/pages/jquery.dashboard.js') }}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.counter').counterUp({
                delay: 100,
                time: 1200
            });

            $(".knob").knob();

        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->
                {{-- <div class="row">
                    <div class="col-sm-12">
                        <h1>&nbsp;</h1>
                        <h1>&nbsp;</h1>
                        <h1>&nbsp;</h1>
                        <h1>Bienvenue sur votre panel d'administration !</h1>
                    </div>
                </div> --}}
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Tableau de bord </h4>
                        <p class="text-muted page-title-alt">Bienvenue sur votre panel d'administration !</p>
                    </div>
                </div>

                <div class="row">
                    <a class="col-md-6 col-lg-3" href="{{route('etudiants')}}">
                        <div class="widget-bg-color-icon card-box fadeInDown animated">
                            <div class="bg-icon bg-icon-info pull-left">
                                <i class="icon-user-following text-info"></i>
                            </div>
                            <div class="text-right">
                                <h3 class="text-dark"><b class="counter">{{$dt['etu']}}</b></h3>
                                <p class="text-muted">Étudiant{{$dt['etu']>1?'s':''}}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </a>

                    <a class="col-md-6 col-lg-3" href="{{route('enseignants')}}">
                        <div class="widget-bg-color-icon card-box">
                            <div class="bg-icon bg-icon-pink pull-left">
                                <i class="ion-android-social text-pink"></i>
                            </div>
                            <div class="text-right">
                                <h3 class="text-dark"><b class="counter">{{$dt['ens']}}</b></h3>
                                <p class="text-muted">Enseignant{{$dt['ens']>1?'s':''}}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </a>

                    <a class="col-md-6 col-lg-3" href="{{route('formations')}}">
                        <div class="widget-bg-color-icon card-box">
                            <div class="bg-icon bg-icon-purple pull-left">
                                <i class="fa fa-book text-purple"></i>
                            </div>
                            <div class="text-right">
                                <h3 class="text-dark"><b class="counter">{{$dt['form']}}</b></h3>
                                <p class="text-muted">Formation{{$dt['form']>1?'s':''}}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </a>

                    <a class="col-md-6 col-lg-3" href="{{route('partnaire')}}">
                        <div class="widget-bg-color-icon card-box">
                            <div class="bg-icon bg-icon-success pull-left">
                                <i class="md md-announcement text-success"></i>
                            </div>
                            <div class="text-right">
                                <h3 class="text-dark"><b class="counter">{{$dt['demand']}}</b></h3>
                                <p class="text-muted">Demande{{$dt['demand']>1?'s':''}}</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>

                <div class="row">
                    <a class="col-lg-3 col-sm-6">
                        <div class="widget-panel widget-style-2 bg-white">
                            <i class="fa icon-people text-custom"></i>
                            <h2 class="m-0 text-dark counter font-600">{{$dt['part']}}</h2>
                            <div class="text-muted m-t-5">Partenaire{{$dt['part']>1?'s':''}}</div>
                        </div>
                    </a>
                    <a class="col-lg-3 col-sm-6" href="{{route('offres')}}">
                        <div class="widget-panel widget-style-2 bg-white">
                            <i class="md md-attach-money text-primary"></i>
                            <h2 class="m-0 text-dark counter font-600">{{$dt['stag']}}</h2>
                            <div class="text-muted m-t-5">Offre{{$dt['stag']>1?'s':''}} de stage</div>
                        </div>
                    </a>
                    <a class="col-lg-3 col-sm-6" href="{{route('offres')}}">
                        <div class="widget-panel widget-style-2 bg-white">
                            <i class="md md-add-shopping-cart text-pink"></i>
                            <h2 class="m-0 text-dark counter font-600">{{$dt['empl']}}</h2>
                            <div class="text-muted m-t-5">Offre{{$dt['empl']>1?'s':''}} d'emploi</div>
                        </div>
                    </a>
                    <a class="col-lg-3 col-sm-6" href="{{route('offres')}}">
                        <div class="widget-panel widget-style-2 bg-white">
                            <i class="md md-store-mall-directory text-info"></i>
                            <h2 class="m-0 text-dark counter font-600">{{$dt['finan']}}</h2>
                            <div class="text-muted m-t-5">Offre{{$dt['finan']>1?'s':''}} de financement</div>
                        </div>
                    </a>
                </div>
                <!-- end row -->


            </div> <!-- container -->

        </div> <!-- content -->

    </div>
@endsection
