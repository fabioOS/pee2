<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Admin blog">
    <meta name="author" content="Admin">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@section('title') | {{env('APP_NAME')}} @show</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/fronts/img/favicon.png') }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    {{--<link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('assets/css/mystyle.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>

    @yield('header_styles')

</head>
<body>
    <div id="app">

        {{--@if(!Request::is('login'))--}}
        @auth
            <!-- Begin page -->
            <div id="wrapper">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <!-- LOGO -->
                    <div class="topbar-left">
                        <div class="text-center">

                            <!-- Image Logo here -->
                            <a href="{{route('home')}}" class="logo">
                                <i class="icon-c-logo"> <img src="{{asset('assets/images/logo_sm.png')}}" height="42"/> </i>
                                <span><img src="{{asset('assets/fronts/img/logo.png')}}" height="40"/></span>
                            </a>
                        </div>
                    </div>

                    <!-- Button mobile view to collapse sidebar menu -->
                    <div class="navbar navbar-default" role="navigation">
                        <div class="container">
                            <div class="">
                                <div class="pull-left">
                                    <button class="button-menu-mobile open-left waves-effect waves-light">
                                        <i class="md md-menu"></i>
                                    </button>
                                    <span class="clearfix"></span>
                                </div>

                                <ul class="nav navbar-nav navbar-right pull-right">

                                    @php
                                        $total_notif=0;
                                        $demandF = App\Notif::where('user_id',Auth::user()->id)->where('lu',0)->get();
                                        $total_notif += count($demandF);
                                    @endphp
                                    <li class="dropdown hidden-xs">
                                        <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                            <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">{{$total_notif}}</span>
                                        </a>
                                        @if($total_notif>0)
                                        <ul class="dropdown-menu dropdown-menu-lg">
                                            <li class="notifi-title"><span class="label label-default pull-right">nouvelles {{$total_notif}}</span>Notifications</li>
                                            <li class="list-group nicescroll notification-list">
                                               <!-- list item-->
                                                <a href="{{route('formations.demandes')}}" class="list-group-item">
                                                    <div class="media">
                                                        <div class="pull-left p-r-10">
                                                            <em class="icon-user-follow fa-2x text-danger"></em>
                                                        </div>
                                                        <div class="media-body">
                                                            <h5 class="media-heading">
                                                                Vous avez {{$total_notif}} demandes de formation.
                                                            </h5>
                                                            <p class="m-0">
                                                                <small></small>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                        @endif
                                    </li>

                                    <li class="hidden-xs">
                                        <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                                    </li>

                                    <li class="dropdown top-menu-item-xs">
                                        <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="{{Auth::user()->img ? asset('assets/userAvatar/'.Auth::user()->img.'')  : asset('assets/images/users/user.jpeg')}}" alt="user-img" class="img-circle"> </a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{route('profiles')}}"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                    <i class="ti-power-off m-r-10 text-danger"></i> Déconnexion
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!--/.nav-collapse -->
                        </div>
                    </div>
                </div>
                <!-- Top Bar End -->


                @include('includes/nav')
        @endauth
        {{--@endif--}}

        <main class="py- 4">
            @include('includes/successOrError')

            @yield('content')
        </main>


        {{--@if(!Request::is('login'))--}}
        @auth
                <!-- Right Sidebar -->
                <div class="side-bar right-bar nicescroll">
                        <h4 class="text-center">Chat</h4>
                        <div class="contact-list nicescroll">
                            <ul class="list-group contacts-list">
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-1.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Chadengle</span>
                                        <i class="fa fa-circle online"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-2.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Tomaslau</span>
                                        <i class="fa fa-circle online"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-3.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Stillnotdavid</span>
                                        <i class="fa fa-circle online"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-4.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Kurafire</span>
                                        <i class="fa fa-circle online"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-5.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Shahedk</span>
                                        <i class="fa fa-circle away"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-6.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Adhamdannaway</span>
                                        <i class="fa fa-circle away"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-7.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Ok</span>
                                        <i class="fa fa-circle away"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-8.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Arashasghari</span>
                                        <i class="fa fa-circle offline"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-9.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Joshaustin</span>
                                        <i class="fa fa-circle offline"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                                <li class="list-group-item">
                                    <a href="#">
                                        <div class="avatar">
                                            <img src="{{asset('assets/images/users/avatar-10.jpg')}}" alt="">
                                        </div>
                                        <span class="name">Sortino</span>
                                        <i class="fa fa-circle offline"></i>
                                    </a>
                                    <span class="clearfix"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                <!-- /Right-bar -->
            </div>
            <!-- END wrapper -->
        {{--@endif--}}
        @endauth
    </div>

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="{{ url('assets/js/jquery.min.js') }}"></script>
    <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('assets/js/detect.js') }}"></script>
    <script src="{{ url('assets/js/fastclick.js') }}"></script>
    <script src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ url('assets/js/jquery.blockUI.js') }}"></script>
    <script src="{{ url('assets/js/waves.js') }}"></script>
    <script src="{{ url('assets/js/wow.min.js') }}"></script>
    <script src="{{ url('assets/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ url('assets/js/jquery.scrollTo.min.js') }}"></script>


    <script src="{{ url('assets/js/jquery.core.js') }}"></script>
    <script src="{{ url('assets/js/jquery.app.js') }}"></script>

    <!-- Scripts -->
    {{--<script src="{{ asset('assets/js/app.js') }}" defer></script>--}}

    @yield('footer_scripts')
</body>
</html>
