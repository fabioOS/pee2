@extends('layouts.app')

@section('title')
    Envoyer un message
    @parent
@stop

@section('header_styles')
<link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
{{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}
@endsection

@section('footer_scripts')
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}
    <!--form validation init-->
    {{-- <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script> --}}
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ url('assets/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#multiselect').select2();
        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Envoyer un message</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{route('message.send')}}">
                        @csrf
                        <div class="col-md-8">
                            <div class="card-box">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label>Description <span class="text-danger">*</span></label>
                                                <textarea name="description" required class="ckeditor" id="description" cols="30" rows="10">{{old('description')}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 m-b-20 header-title"><b>Objet</b> <span class="text-danger">*</span></h4>
                                    <hr>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="text" required value="{{old('objet')}}" name="objet" class="form-control col-md-12" placeholder="Objet">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 m-b-20 header-title"><b>Destinataire</b> <span class="text-danger">*</span></h4>
                                    <hr>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <select name="destination[]" id="multiselect" class="" multiple="multiple">
                                                <option value="0">Tous</option>
                                                @if(count($allstudents)>0)
                                                <optgroup label="Etudiants">
                                                        <option value="00">Tous les etudiants</option>
                                                        @foreach($allstudents as $item)
                                                            <option value="{{$item->id}}">{{$item->name}} </option>
                                                        @endforeach
                                                    </optgroup>
                                                @endif

                                                @if(count($allprofs)>0)
                                                    <optgroup label="Enseignant">
                                                        <option value="000">Tous les enseigants</option>
                                                        @foreach($allprofs as $item)
                                                            <option value="{{$item->id}}">{{$item->name}} </option>
                                                        @endforeach
                                                    </optgroup>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 m-b-20 header-title"><b>Envoyer</b></h4>
                                    <hr>
                                    <div class="row">
                                        <button type="submit" class="btn btn-primary btn-md waves-effect waves-light"><i class="fa fa-check-square"></i> Envoyer </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
