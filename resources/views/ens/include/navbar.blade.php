<div class="settings-widget dash-profile">
    <div class="settings-menu p-0">
        <div class="profile-bg">
            {{-- <h5>Beginner</h5> --}}
            <img src="{{('assets/fronts/img/instructor-profile-bg.jpg')}}" alt="">
            <div class="profile-img">
                <a href="{{route('ens.profiles')}}">
                    @if (Auth::user()->img != '')
                        <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Avatar">
                    @else
                        <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Avatar">
                    @endif
                </a>
            </div>
        </div>
        <div class="profile-group">
            <div class="profile-name text-center">
                <h4 class="text-uppercase- font-600"><a>{{Auth::user()->name}}</a></h4>
                <p class="text-muted font-13 m-b-30">
                    {{Auth::user()->email}}
                </p>
            </div>
            <div class="go-dashboard text-center">
                <a href="add-course.html" class="btn btn-primary">Créer une nouvelle formation</a>
            </div>
        </div>
    </div>
</div>
<div class="settings-widget account-settings">
    <div class="settings-menu">
        <h3>DASHBOARD</h3>
        <ul>
            <li class="nav-item active--">
                <a href="{{route('ens.home')}}" class="nav-link">
                    <i class="feather-home"></i> Tableau de bord
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('ens.formations')}}" class="nav-link">
                    <i class="feather-book"></i> Formations
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('ens.etudiants')}}" class="nav-link">
                    <i class="feather-users"></i> Étudiants
                </a>
            </li>

        </ul>
        <div class="instructor-title">
            <h3>MON COMPTE</h3>
        </div>
        <ul>
            <li class="nav-item">
                <a href="{{route('ens.profiles')}}" class="nav-link ">
                    <i class="feather-settings"></i> Editer mon Profil
                </a>
            </li>
            {{-- <li class="nav-item">
                <a href="instructor-security.html" class="nav-link">
                    <i class="feather-lock"></i> Securité
                </a>
            </li> --}}
            <li class="nav-item">
                <a href="instructor-social-profiles.html" class="nav-link">
                    <i class="feather-user"></i> Réseaux sociaux
                </a>
            </li>
            <li class="nav-item">
                <a href="instructor-tickets.html" class="nav-link">
                    <i class="feather-server"></i> Messages
                </a>
            </li>
            <li class="nav-item">
                <a href="instructor-notification.html" class="nav-link">
                    <i class="feather-bell"></i> Notifications
                </a>
            </li>
            {{-- <li class="nav-item">
                <a href="#" class="nav-link text-danger">
                    <i class="feather-trash-2"></i> Supprimer mon compte
                </a>
            </li> --}}
            <li class="nav-item">
                <a href="{{route('quicklogout')}}" class="nav-link">
                    <i class="feather-power"></i> Déconnexion
                </a>
            </li>
        </ul>
    </div>
</div>

