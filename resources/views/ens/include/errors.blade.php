@if($errors->any())
    <div class="text-center col-sm-12">
        <div class="alert alert-danger alert-dismissible fade show mt-4 px-4 mb-0 text-center" role="alert">
            <i class="uil uil-exclamation-octagon d-block display-4 mt-2 mb-3 text-danger"></i>
            <h5 class="text-danger">Erreur</h5>
            @foreach($errors->all() as $error)
                <p class="text-danger"><i class="fas fa-info-circle"></i> &nbsp; {!! $error !!} </p>
            @endforeach
        </div>
    </div>
@endif
