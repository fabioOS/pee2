@extends('ens.app')
@section('title', "PEE | Pôle de l'Etudiant Entrepreneur")

@section('css')
@endsection

@section('js')
    <!-- Chart JS -->
    <script src="{{asset('assets/fronts/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{asset('assets/fronts/plugins/apexchart/chart-data.js')}}"></script>
@endsection

@section('content')

<div class="col-xl-12 col-lg-12 col-md-12 m-0 p-0">
    <div class="row">
        <div class="col-md-4 d-flex">
            <div class="card instructor-card w-100">
                <div class="card-body">
                    <div class="instructor-inner">
                        <h6>REVENUE</h6>
                        <h4 class="instructor-text-success">$467.34</h4>
                        <p>Earning this month</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 d-flex">
            <div class="card instructor-card w-100">
                <div class="card-body">
                    <div class="instructor-inner">
                        <h6>STUDENTS ENROLLMENTS</h6>
                        <h4 class="instructor-text-info">12,000</h4>
                        <p>New this month</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 d-flex">
            <div class="card instructor-card w-100">
                <div class="card-body">
                    <div class="instructor-inner">
                        <h6>COURSES RATING</h6>
                        <h4 class="instructor-text-warning">4.80</h4>
                        <p>Rating this month</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card instructor-card">
                <div class="card-header">
                    <h4>Earnings</h4>
                </div>
                <div class="card-body">
                    <div id="instructor_chart"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card instructor-card">
                <div class="card-header">
                    <h4>Order</h4>
                </div>
                <div class="card-body">
                    <div id="order_chart"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
