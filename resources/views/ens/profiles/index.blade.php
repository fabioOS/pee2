@extends('ens.app')
@section('title', "PEE | Pôle de l'Etudiant Entrepreneur")

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/dropify/dropify.css') }}">
@endsection

@section('js')
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/dropify/dropify.js') }}"></script>
    <script>
        $('.dropify').dropify();
        $('.select2').select2();
    </script>
@endsection

@section('content')
<div class="col-xl-12 col-lg-12 col-md-12 m-0 p-0">
    <div class="settings-widget profile-details">
        <div class="settings-menu p-0">
            <div class="profile-heading">
                <h3>Editer mon Profile</h3>
            </div>
			<!-- Category Tab -->
            <div class="category-tab">
                <div class="checkout-form personal-address add-course-info">
                    <ul class="nav nav-justified">
                        <li class="nav-item text-center col-md-4"><a href="#Dinfos" class="nav-link active" data-bs-toggle="tab" >Informations de compte</a></li>
                        <li class="nav-item text-center col-md-3"><a href="#Dmotdepasse" class="nav-link" data-bs-toggle="tab" >Mot de passe</a></li>
                        <li class="nav-item text-center col-md-3"><a href="#Davatar" class="nav-link" data-bs-toggle="tab" >Photo de profil</a></li>
                    </ul>
                </div>
            </div>
            <!-- /Category Tab -->

            <!-- Category List -->
            <div class="tab-content">
                <div class="tab-pane fade show active" id="Dinfos">
                    <div class="plan-box loginbox" style="border: none;">
                        <form class="col-md-12" action="{{route('ens.profiles.save')}}" method="post">
                            <div class="row">
                                @csrf
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Nom</label>
                                        <input type="text" name="nom" class="form-control" required value="{{Auth::user()->name}}">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Sexe</label>
                                        <select class="form-select form-control" name="sexe" required>
                                            <option value="Homme" {{Auth::user()->sexe=='Homme' ? 'selected' : '' }}>Homme</option>
                                            <option value="Femme" {{Auth::user()->sexe=='Femme' ? 'selected' : '' }}>Femme</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="profile-share">
                                            <button type="submit" class="btn btn-danger">Modifier</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="tab-pane fade show" id="Dmotdepasse">
                    <div class="plan-box loginbox" style="border: none;">
                        <form class="col-md-12" action="{{route('ens.profiles.savepass')}}" method="post">
                            <div class="row">
                                @csrf
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Mot de passe actuel</label>
                                        <input type="password" name="motpass" required="required" class="form-control" placeholder="Mot de passe actuel">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Nouveau mot de passe</label>
                                        <input type="password" name="newpass" required="required" class="form-control" placeholder="Nouveau mot de passe">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-control-label">Confirmer mot de passe</label>
                                        <input type="password" name="confirmpass" required="required" class="form-control" placeholder="Confirmer mot de passe">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="profile-share">
                                            <button type="submit" class="btn btn-danger">Modifier</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="tab-pane fade show" id="Davatar">
                    <div class="plan-box loginbox" style="border: none;">
                        <form class="col-md-12" enctype="multipart/form-data" action="{{route('ens.profiles.saveavatar')}}" method="post">
                            @csrf
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="file" id="fileUser" name="fileUser" class="dropify" data-default-file="{{ Auth::user()->img  ? asset('assets/userAvatar/'.Auth::user()->img.'') : asset('assets/images/users/user.jpeg') }}"/>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="profile-share">
                                        <button type="submit" class="btn btn-danger">Modifier</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
            <!-- /Category List -->


        </div>
    </div>
</div>
@endsection
