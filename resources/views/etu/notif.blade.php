@extends('etu.app')
@section('title', "PEE | Pôle de l'Etudiant Entrepreneur")

@section('css')
<!-- Feathericon CSS -->
<link rel="stylesheet" href="{{ asset('assets/fronts/plugins/feather/feather.css')}}">
@endsection

@section('js')
    <script>
        $('.select2').select2();
    </script>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="settings-widget">
            <div class="settings-inner-blk p-0">
                <div class="sell-course-head comman-space">
                    <h3>Notifications</h3>
                </div>
                <div class="comman-space pb-0">
                    <div class="settings-tickets-blk course-instruct-blk table-responsive">

                        <!-- Referred Users-->
                        @if ($notifies->count()>0)
                            <table class="table table-nowrap mb-2">
                                <thead>
                                <tr>
                                    <th>Notifications</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($notifies as $nt)
                                        <tr>
                                            <td>
                                                <div class="sell-table-group d-flex align-items-center">
                                                    <div class="sell-tabel-info">
                                                        <p><a href="{{$nt->lien}}">{{$nt->message}}</a></p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{$nt->created_at->format('d/m/Y H:i:s')}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <h4 class="text-warning">Vous n'avez aucune notification pour l'instant !</h4><br>
                        @endif
                        <!-- /Referred Users-->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
