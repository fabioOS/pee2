@extends('etu.app')
@section('title', "PEE | Pôle de l'Etudiant Entrepreneur")

@section('css')
@endsection

@section('js')

@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="settings-widget">
            <div class="settings-inner-blk p-0">
                <div class="sell-course-head comman-space">
                    <h3>Favoris</h3>
                    <p>Ma liste de favoris.</p>
                </div>
                <div class="comman-space pb-0">

                    <div class="row">
                        @if (count($formations)==0)
                            <div class="col-lg-12 col-md-12 d-flex label text-warning text-center">
                                <p style="text-align:center;" class="lead m-t-0">
                                    Vous n'avez ajouté aucun favoris pour l'instant !
                                </p>
                            </div>
                        @else
                            @foreach ($formations as $item)
                                <div class="col-lg-4 col-md-6 d-flex">
                                    <div class="course-box course-design d-flex " >
                                        <div class="product">
                                            <div class="product-img">
                                                <a href="{{route('etu.formations.favoris.show',$item->slug)}}">
                                                    @if ($item->img != null)
                                                        <img class="img-fluid" src="{{asset($item->img)}}" alt="cover">
                                                    @else
                                                        <img class="img-fluid" src="{{asset('assets/images/big/img5.jpg')}}" alt="default">
                                                    @endif
                                                </a>
                                                <div class="price">
                                                    @if ($item->prix != null)
                                                        @if ($item->prixpromo != null)
                                                            <h3>{{number_format($item->prixpromo,0,'','.')}}<sup>F CFA</sup>  <span>{{number_format($item->prix,0,'','.')}}<sup>F CFA</sup></span></h3>
                                                        @else
                                                            <h3>{{number_format($item->prix,0,'','.')}}<sup>F CFA</sup> </h3>
                                                        @endif
                                                    @else
                                                        <h3>Gratuit</h3>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="course-group d-flex">
                                                    <div class="course-group-img d-flex">
                                                        <a href="javascript:void();">
                                                            @if ($item->professeur->img != '')
                                                                <img src="{{asset('assets/userAvatar/'.$item->professeur->img)}}" alt="avatar" class="img-fluid">
                                                            @else
                                                                <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="avatar" class="img-fluid">
                                                            @endif
                                                        </a>
                                                        <div class="course-name">
                                                            <h4><a href="javascript:void();" class="text-secondary">{{ucwords(substr($item->professeur->name,0,15))}}</a></h4>
                                                            <p>Formateur</p>
                                                        </div>
                                                    </div>
                                                    <div class="course-share d-flex align-items-center justify-content-center">
                                                        @if (Auth::check())
                                                            @if (App\Formation_favori::where('formation_id',$item->id)->where('etudiant_id',Auth::user()->id)->count() == 0)
                                                                <a title="Ajouter à mes favoris" href="javascript:void(0);" ref="{{route('etu.formations.savefavoris', encrypt($item->id))}}" class="btnAction"><i class="fa-regular fa-heart"></i></a>
                                                                <div class="feedback mb-2"></div>
                                                            @else
                                                                <i class="fa fa-check-circle fa-3x text-success"></i>
                                                            @endif
                                                        @else
                                                            <a title="Ajouter à mes favoris" href="{{route('login')}}?backto=formations"><i class="fa-regular fa-heart"></i></a>
                                                        @endif
                                                    </div>
                                                </div>
                                                <h3 class="title"><a href="{{route('etu.formations.favoris.show',$item->slug)}}">{{substr($item->titre,0,100)}} ...</a></h3>
                                                <div class="course-info d-flex align-items-center">
                                                    <div class="rating-img d-flex align-items-center">
                                                        <p title="Date de début"><i class="fa fa-calendar" style="color:#e96525;"></i> <span class="text-muted">{{Illuminate\Support\Carbon::parse($item->debut)->format('d/m/Y')}}</span></p>
                                                    </div>
                                                    <div class="course-view d-flex align-items-center">
                                                        <p title="Durée"><i class="fa fa-clock" style="color:#e96525;"></i> <span class="text-muted">{{$item->duree.' '.ucfirst($item->typedelai)}}</span></p>
                                                    </div>
                                                </div>

                                                <div class="all-btn all-category d-flex align-items-center">
                                                    <a href="{{route('etu.formations.favoris.show',$item->slug)}}" class="btn btn-primary btn-block">Détails</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
