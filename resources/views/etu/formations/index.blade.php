@extends('etu.app')
@section('title', "PEE | Pôle de l'Etudiant Entrepreneur")

@section('css')
@endsection

@section('js')
    {{-- <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script> --}}
    <script>
        //document ready
        $(document).ready(function() {
            $( ".filterObj" ).prop( "checked", false );
            $('#f_search .filterObj').each(function(index, element){
                $(this).click(function() {
                    var data = $('#f_search').serialize();
                    //console.log(serial);
                    var url = $('#f_search').attr('url');
                    var container = $('#f_container');
                    return Query_GET(url,data,container);
                });
            });

            //
            function Query_GET(url,data,container) {
                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    beforeSend: function() {
                        //container.html('<div class="text-center"><div class="spinner-border text-primary" role="status"><span class="sr-only">Chargement...</span></div></div>');
                        container.html('<div class="text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
                    },
                    success: function(data) {
                        container.html(data);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
            ////
            $('.btnAction').each(function(index, element){
                $(this).click(function() {
                    $(this).hide();
                    var container = $('.feedback:eq('+index+')');
                    container.delay( 800 ).html('<div class="text-center mb-4"><i class="fa fa-spinner fa-spin fa-3x fa-fw text-secondary"></i></div>');
                    var data = '';
                    var url  = $(this).attr('ref');
                    console.log(url);
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: data,
                        dataType: "json",
                        cache: false,
                        beforeSend: function() {
                            container.html('<div class="text-center m-6"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
                        },
                        success: function(data) {
                            if(data.status == 'error')
                                container.html('<div class="text-center m-6"><i class="fa fa-times-circle fa-3x text-danger"></i> <span class="mb-4">'+data.message+'</span> </div>');
                            else
                                container.html('<div class="text-center m-6"><i class="fa fa-check-circle fa-3x text-success"></i> </div>');
                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });
                });
            });
        });

    </script>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="settings-widget">
            <div class="settings-inner-blk p-0">
                <div class="sell-course-head comman-space">
                    <h3>Formations</h3>
                    <p>Liste de mes formations.</p>
                </div>
                <div class="comman-space pb-0">

                    @include('etu.formations.data')

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
