@extends('etu.app')
@section('title','PEE | '.$item->slug)

@section('css')
@endsection

@section('js')
<script>
    //document ready
    $(document).ready(function() {
        $('.btnAction').each(function(index, element){
            $(this).click(function() {
                $(this).hide();
                var container = $('.feedback:eq('+index+')');
                container.delay( 800 ).html('<div class="text-center mb-4"><i class="fa fa-spinner fa-spin fa-3x fa-fw text-secondary"></i></div>');
                var data = '';
                var url  = $(this).attr('ref');
                console.log(url);
                return Query_GET(url,data,container);
            });
        });

        //
        function Query_GET(url,data,container) {
            $.ajax({
                type: "GET",
                url: url,
                data: data,
                dataType: "json",
                cache: false,
                beforeSend: function() {
                    container.html('<div class="text-center m-6"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>');
                },
                success: function(data) {
                    console.log(data);
                    if(data.status == 'error')
                        container.html('<div class="text-center m-6"><i class="fa fa-times-circle fa-3x text-danger"></i> <span class="mb-4">'+data.message+'</span> </div>');
                    else
                        container.html('<div class="text-center m-6"><i class="fa fa-check-circle fa-3x text-success"></i> <span class="mb-4">'+data.message+'</span>  </div>');
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    });
</script>
@endsection

@section('content')
<div class="inner-banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="instructor-wrap border-bottom-0 m-0">
                    <div class="about-instructor align-items-center">
                        <div class="abt-instructor-img">
                            <a>
                                @if ($item->professeur->img != '')
                                    <img src="{{asset('assets/userAvatar/'.$item->professeur->img)}}" alt="avatar" class="img-fluid">
                                @else
                                    <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="avatar" class="img-fluid">
                                @endif
                            </a>
                        </div>
                        <div class="instructor-detail me-3">
                            <h5><a>{{ucwords($item->professeur->name)}}</a></h5>
                            <p>{{ucwords($item->professeur->grade)}}</p>
                        </div>
                    </div>

                    @foreach ($item->categories as $cat)
                    <a href="{{route('w.formation')}}?catego={{$cat->catformation_id}}" class="web-badge mb-3">{{$cat->catformation->libelle}}</a>
                    @endforeach

                </div>
                <h2>{{$item->titre}}</h2>
                <div class="course-info d-flex align-items-center border-bottom-0 m-0 p-0">
                    <div class="cou-info">
                        <i class="fa fa-calendar" style="color:#e96525;"></i>
                        <p title="Date de début">{{Illuminate\Support\Carbon::parse($item->debut)->format('d/m/Y')}}</p>
                    </div>
                    <div class="cou-info">
                        <i class="fa fa-clock" style="color:#e96525;"></i>
                        <p title="Durée">{{$item->duree.' '.ucfirst($item->typedelai)}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Course Content -->
<section class="page-content course-sec">
    <div class="container">

        <div class="row">
            <div class="col-lg-8">

                <!-- Overview -->
                <div class="card overview-sec">
                    <div class="card-body">
                        <h5 class="subs-title">Description</h5>
                        <div class="row">
                            <div class="col-md-12">
                                {!!$item->description!!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Overview -->
                @if ($item->cible != null || $item->objectif != null || $item->competence != null)
                    <div class="card overview-sec">
                        <div class="card-body">
                            @if ($item->cible != null)
                                <h5 class="subs-title">Cible</h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        {!!$item->cible!!}
                                    </div>
                                </div>
                            @endif
                            @if ($item->objectif != null)
                                <h5 class="subs-title">Objectif</h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        {!!$item->objectif!!}
                                    </div>
                                </div>
                            @endif
                            @if ($item->competence != null)
                                <h5 class="subs-title">Compétences à acquérir</h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        {!!$item->competence!!}
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif

                <!-- Features -->
                <div class="card feature-sec">
                    <div class="card-body">
                        <div class="cat-title">
                            <h4>{{-- Includes --}}</h4>
                        </div>
                        <ul>
                            <li><img src="{{asset('assets/fronts/img/icon/icon-22.svg')}}" class="me-2" alt=""> Date de début: <span>{{$item->debut->format('d/m/Y')}}</span></li>
                            <li><img src="{{asset('assets/fronts/img/icon/timer.svg')}}" class="me-2" alt=""> Durée: <span>{{$item->duree.' '.ucfirst($item->typedelai)}}</span></li>
                            <li><img src="{{asset('assets/fronts/img/icon/icon-24.svg')}}" class="me-2" alt=""> Type : <span>{{($item->salle!='') ? 'Présentielle (Salle '.$item->salle.')' : ' En ligne '}}</span></li>
                            <li><img src="{{asset('assets/fronts/img/icon/users.svg')}}" class="me-2" alt=""> Personnes inscrites: <span>{{DB::table('formation_demandes')->where('statut',1)->count()}} </span></li>
                        </ul>
                    </div>
                </div>
                <!-- /Features -->
                <!-- Instructor -->
                <div class="card instructor-sec">
                    <div class="card-body">
                        <h5 class="subs-title">A propos de l'formateur</h5>
                        <div class="instructor-wrap">
                            <div class="about-instructor">
                                <div class="abt-instructor-img">
                                    <a href="instructor-profile.html">
                                        @if ($item->professeur->img != '')
                                            <img src="{{asset('assets/userAvatar/'.$item->professeur->img)}}" alt="avatar" class="img-fluid">
                                        @else
                                            <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="avatar" class="img-fluid">
                                        @endif
                                    </a>
                                </div>
                                <div class="instructor-detail">
                                    <h5><a>{{ucwords($item->professeur->name)}}</a></h5>
                                    <p>{{ucwords($item->professeur->grade)}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="course-info d-flex align-items-center">
                            <div class="cou-info">
                                <img src="{{asset('assets/fronts/img/icon/play.svg')}}" alt="">
                                <p>{{$ins['format']}} Formation{{(int)$ins['format']>0 ? 's':''}} terminée{{(int)$ins['format']>0 ? 's':''}}</p>
                            </div>
                            <div class="cou-info">
                                <img src="{{asset('assets/fronts/img/icon/people.svg')}}" alt="">
                                <p>{{$ins['etu']}} Étudiant{{(int)$ins['etu']>0 ? 's':''}} encadré{{(int)$ins['etu']>0 ? 's':''}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Instructor -->


            </div>

            <div class="col-lg-4">
                <div class="sidebar-sec">

                    <!-- Video -->
                    <div class="video-sec vid-bg">
                        <div class="card">
                            <div class="card-body">
                                <a class="video-thumbnail">
                                    {{-- <div class="play-icon">
                                        <i class="fa-solid fa-play"></i>
                                    </div> --}}
                                    @if ($item->img != null)
                                        <img src="{{asset($item->img)}}" alt="cover">
                                    @else
                                        <img src="{{asset('assets/images/big/img5.jpg')}}" alt="cover">
                                    @endif
                                </a>
                                <div class="video-details">
                                    <div class="course-fee">
                                    @if ($item->prix != null)
                                        @if ($item->prixpromo != null)
                                            <h2>{{number_format($item->prixpromo,0,'','.')}}<sup>F CFA</sup>  </h2>
                                            <p><span>{{number_format($item->prix,0,'','.')}}<sup>F CFA</sup></span></p>
                                        @else
                                            <h2>{{number_format($item->prix,0,'','.')}}<sup>F CFA</sup> </h2>
                                        @endif
                                    @else
                                        <h2>Gratuit</h2>
                                    @endif

                                    </div>

                                    <div class="row gx-2">
                                        {{-- <div class="col-md-12">
                                            <a class="btn btn-dark w-100 disabled" @disabled(true) disabled="disabled" style="cursor:no-drop;"><i class="feather-heart"></i> Ajouter à mes favoris</a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Video -->

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
