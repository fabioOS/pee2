<div class="settings-widget dash-profile">
    <div class="settings-menu p-0">
        <div class="profile-bg">
            {{-- <h5>Beginner</h5> --}}
            <img src="{{('assets/fronts/img/instructor-profile-bg.jpg')}}" alt="">
            <div class="profile-img">
                <a href="{{route('etu.profiles')}}">
                    @if (Auth::user()->img != '')
                        <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Avatar">
                    @else
                        <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Avatar">
                    @endif
                </a>
            </div>
        </div>
        <div class="profile-group">
            <div class="profile-name text-center">
                <h4 class="text-uppercase- font-600"><a>{{Auth::user()->name}}</a></h4>
                <p class="text-muted font-13 m-b-30">
                    {{Auth::user()->email}}
                </p>
            </div>
            <div class="go-dashboard text-center">
                <a href="{{route('web')}}" class="btn btn-primary"> <i class="fa fa-chevron-left"></i> Retour au site web</a>
            </div>
        </div>
    </div>
</div>
<div class="settings-widget account-settings">
    <div class="settings-menu">
        <h3>DASHBOARD</h3>
        <!-- mark active link -->
        <ul>
            <li class="nav-item active--">
                <a href="{{route('etu.home')}}" class="nav-link {{ url()->current() == route('etu.home') ? 'active' : '' }}">
                    <i class="feather-home"></i> Tableau de bord
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('etu.formations')}}" class="nav-link {{ url()->current() == route('etu.formations') ? 'active' : '' }}">
                    <i class="feather-book"></i> Formations
                </a>
            </li>

        </ul>
        <div class="instructor-title">
            <h3>MON COMPTE</h3>
        </div>
        <ul>
            <li class="nav-item {{ url()->current() == route('etu.profiles') ? 'active' : '' }}">
                <a href="{{route('etu.profiles')}}" class="nav-link">
                    <i class="feather-settings"></i> Editer mon Profil
                </a>
            </li>
            <li class="nav-item {{ url()->current() == route('etu.formations.favoris') ? 'active' : '' }}">
                <a href="{{route('etu.formations.favoris')}}" class="nav-link">
                    <i class="feather-heart"></i> Favoris
                </a>
            </li>
            <li class="nav-item {{ url()->current() == route('etu.notifications') ? 'active' : '' }}">
                @php
                    $demandF = App\Notif::where('user_id',Auth::user()->id)->where('lu',0)->get();
                    $total_notif = count($demandF);
                @endphp
                <a href="{{route('etu.notifications')}}" class="nav-link">
                    <i class="feather-bell"></i> Notifications &nbsp; @if($total_notif>0)<span class="badge badge-xs badge-danger">{{$total_notif}}</span>@endif
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('quicklogout')}}" class="nav-link bg-warning" style="border-radius: 4px;">
                    <i class="feather-power"></i> Déconnexion
                </a>
            </li>
        </ul>
    </div>
</div>

