<!-- Header -->
<header class="header header-page">
    <div class="header-fixed">
        <nav class="navbar navbar-expand-lg header-nav scroll-sticky">
            <div class="container header-border">
                <div class="navbar-header">
                    <a id="mobile_btn" href="javascript:void(0);">
                        <span class="bar-icon">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </a>
                    <a href="{{route('web')}}" class="navbar-brand logo">
                        <img src="{{asset('assets/fronts/img/logo.png')}}" class="img-fluid" alt="Logo">
                    </a>
                </div>
                <div class="main-menu-wrapper">
                    <div class="menu-header">
                        <a href="{{route('web')}}" class="menu-logo">
                            <img src="{{asset('assets/fronts/img/logo.png')}}" class="img-fluid" alt="Logo">
                        </a>
                        <a id="menu_close" class="menu-close" href="javascript:void(0);">
                            <i class="fas fa-times"></i>
                        </a>
                    </div>
                    <ul class="main-nav">


                        <li class="login-link">
                            <a href="javascript:void();" class="dropdown-toggle">
                                <span class="user-img">
                                    <img src="{{('assets/fronts/img/profile-avatar.png')}}" alt="">
                                    <span class="status online">Bienvenue {{substr(ucfirst(Auth::user()->name),0,7)}}...</span>
                                    <h6></h6>
                                </span>
                            </a>
                            <ul class="submenu first-submenu">
                                <a class="p-1 mt-2" href="{{route('etu.home')}}"><i class="feather-home me-1"></i> Tableau de bord</a>
                                <a class="p-1 m-1" href="{{route('etu.profiles')}}"><i class="fa fa-user-cog me-1"></i> Profil </a>
                                <a class="p-1 mb-2" href="{{route('quicklogout')}}"><i class="feather-log-out me-1"></i> Déconnexion</a>
                            </ul>
                        </li>
                    </ul>
                </div>
                <ul class="nav header-navbar-rht">

                    @php
                        $total_notif=0;
                        $demandF = App\Notif::where('user_id',Auth::user()->id)->where('lu',0)->get();
                        $total_notif += count($demandF);
                    @endphp
                    <li class="nav-item noti-nav">
                        <a href="javascript:void();" class="dropdown-toggle" data-bs-toggle="dropdown">
                            <img src="{{asset('assets/fronts/img/icon/notification.svg')}}" alt="notif">
                            @if($total_notif>0)<span class="badge badge-xs badge-danger">{{$total_notif}}</span>@endif
                        </a>
                        <div class="notifications dropdown-menu dropdown-menu-right">
                            <div class="noti-content">
                                <ul class="notification-list">
                                    @if($demandF->count()>0)
                                        @foreach ($demandF as $df)
                                            <li class="notification-message">
                                                <div class="media d-flex">
                                                    <div>
                                                        <a href="{{$df->lien}}" class="avatar">
                                                            <em class="icon-user-follow fa-2x text-danger"></em>
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h6><a href="{{$df->lien}}"><span>{{$df->message}}</span> </a></h6>
                                                        <a href="{{$df->lien}}" class="btn btn-accept">Voir les détails</a>
                                                        <!-- date calcul -->
                                                        @php
                                                            $date = $df->created_at;
                                                            $now = date('Y-m-d H:i:s');
                                                            $diff = abs(strtotime($now) - strtotime($date));
                                                            $years = floor($diff / (365*60*60*24));
                                                            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                                            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                                                            $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
                                                            $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
                                                            $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));
                                                        @endphp
                                                        {{-- <p>{{$df->created_at->diffForHumans(date('Y-m-d'))}}</p> --}}
                                                        <p>Il y a {{$days}} jours {{$hours}} heures {{$minutes}} minutes</p>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    @else
                                        <li class="notification-message">
                                            <div class="media d-flex">
                                                <div class="media-body">
                                                    Vous n'avez aucune notification
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item user-nav">
                        <a href="javascript:void();" class="dropdown-toggle" data-bs-toggle="dropdown">
                            <span class="user-img">
                                @if (Auth::user()->img != '')
                                    <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Image Utilisateur">
                                @else
                                    <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Image Utilisateur">
                                @endif
                                <span class="status online"></span>
                            </span>
                        </a>
                        <div class="users dropdown-menu dropdown-menu-right" data-popper-placement="bottom-end" >
                            <div class="user-header">
                                <div class="avatar avatar-sm">
                                    @if (Auth::user()->img != '')
                                        <img src="{{asset('assets/userAvatar/'.Auth::user()->img)}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                    @else
                                        <img src="{{asset('assets/fronts/img/profile-avatar.png')}}" alt="Image Utilisateur" class="avatar-img rounded-circle">
                                    @endif
                                </div>
                                <div class="user-text">
                                    <h6>{{ucwords(Auth::user()->name)}}</h6>
                                    <p class="text-muted mb-0">{{Auth::user()->roles->libelle}}</p>
                                </div>
                            </div>
                            <a class="dropdown-item" href="{{route('etu.home')}}"><i class="feather-home me-1"></i> Tableau de bord</a>
                            <a class="dropdown-item" href="{{route('etu.profiles')}}"><i class="feather-user me-1"></i> Profil </a>
                            <a class="dropdown-item" href="{{route('quicklogout')}}"><i class="feather-log-out me-1"></i> Déconnexion</a>
                        </div>
                    </li>

                </ul>

            </div>
        </nav>
    </div>
</header>
<!-- /Header -->
