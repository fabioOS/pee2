@extends('etu.app')
@section('title', "PEE | Pôle de l'Etudiant Entrepreneur")

@section('css')
@endsection

@section('js'){{--
    <!-- Chart JS -->
    <script src="{{asset('assets/fronts/plugins/apexchart/apexcharts.min.js')}}"></script>
    <script src="{{asset('assets/fronts/plugins/apexchart/chart-data.js')}}"></script> --}}
@endsection

@section('content')
<div class="settings-widget">
    <div class="settings-inner-blk p-0">
        <div class="sell-course-head comman-space">
            <h3>Tableau de bord étudiant</h3>
        </div>

    </div>
</div>
<div class="col-xl-12 col-lg-12 col-md-12 m-0 p-0">
    <div class="row">

        <div class="col-md-4 d-flex">
            <div class="card instructor-card w-100">
                <div class="card-body">
                    <div class="instructor-inner">
                        <h6>MES FORMATIONS</h6>
                        <h4 class="instructor-text-success">{{($dt['form']>0 && $dt['form']<10) ? '0':''}}{{$dt['form']}}</h4>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 d-flex">
            <div class="card instructor-card w-100">
                <div class="card-body">
                    <div class="instructor-inner">
                        <h6>DEMANDES EN ATTENTES</h6>
                        <h4 class="instructor-text-info">{{($dt['deman']>0 && $dt['deman']<10) ? '0':''}}{{$dt['deman']}}</h4>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 d-flex">
            <div class="card instructor-card w-100">
                <div class="card-body">
                    <div class="instructor-inner">
                        <h6>MES FAVORIS</h6>
                        <h4 class="instructor-text-warning">{{($dt['favo']>0 && $dt['favo']<10) ? '0':''}}{{$dt['favo']}}</h4>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
