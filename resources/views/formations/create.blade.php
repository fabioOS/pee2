@extends('layouts.app')

@section('title')
    Ajouter une formation
    @parent
@endsection

@section('header_styles')
    {{--<link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/jquery.steps/demo/css/jquery.steps.css') }}" />

    <!-- Plugins css-->
    <link href="{{ asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/switchery/dist/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/multiselect/css/multi-select.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <!-- Dropzone css -->
    <link href="{{ asset('assets/plugins/dropzone/dist/dropzone.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('footer_scripts')
    {{--<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>--}}
    <!--form validation init-->
    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>

    <!--Form Validation-->
    <script src="{{ asset('assets/plugins/bootstrapvalidator/dist/js/bootstrapValidator.js') }}" type="text/javascript"></script>
    <!--Form Wizard-->
    <script src="{{ asset('assets/plugins/jquery.steps/build/jquery.steps.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <!--wizard initialization-->
    <script src="{{ asset('assets/pages/jquery.wizard-init.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/jquery-quicksearch/jquery.quicksearch.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-filestyle/src/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>

    <!-- Page Specific JS Libraries -->
    <script src="{{ asset('assets/plugins/dropzone/dist/dropzone.js') }}"></script>



    <script type="text/javascript">
        $(document).ready(function () {
            if($("#elm1").length > 0){
                tinymce.init({
                    selector: "textarea#elm1",
                    theme: "modern",
                    height:300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                    style_formats: [
                        {title: 'Bold text', inline: 'b'},
                        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                        {title: 'Example 1', inline: 'span', classes: 'example1'},
                        {title: 'Example 2', inline: 'span', classes: 'example2'},
                        {title: 'Table styles'},
                        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                    ]
                });
            }
            if($("#elm2").length > 0){
                tinymce.init({
                    selector: "textarea#elm2",
                    theme: "modern",
                    menubar:false,
                    statusbar: false,
                    height:300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link  | print preview media fullpage | forecolor backcolor",
                    style_formats: [
                        {title: 'Bold text', inline: 'b'},
                        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                        {title: 'Example 1', inline: 'span', classes: 'example1'},
                        {title: 'Example 2', inline: 'span', classes: 'example2'},
                        {title: 'Table styles'},
                        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                    ]
                });
            }

            if($("#type_article").val()=='text'){
                $("#vdAvant").hide();
                $("#imgAvant").show();
            }else{
                $("#imgAvant").hide();
                $("#vdAvant").show();
                loadVideoImg();
            }
        });

        var loadFile = function(event) {
            $("#output").show();
            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        };

        function loadVideoImg(){
            var lien = $("#linkvideo").val();
            var out = document.getElementById('outputvideo');
            out.src = null;
            var search = lien.split("?v=");
            var result = search[1];
            if(result){
                //console.log(search);
                var thumbnail="http://img.youtube.com/vi/"+result+"/maxresdefault.jpg";
                $("#outputvideo").show();
                out.src = thumbnail;
            }

        }

        $("#type_article").change(function () {

            if ($("#type_article").val()=='text'){
                $("#vdAvant").hide();
                $("#imgAvant").show();
            }else{
                $("#imgAvant").hide();
                $("#vdAvant").show();
            }
        });

    </script>
    <script>
        // Select2
        $(".select2").select2();
        $(document).ready(function() {
            $('form').parsley();
        });
        function formatNumber_prix(input) {
            input.value = input.value.replace(/[^\d]/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        }
        $('#wizard-validation-form input').blur(function() {
            var nm = $(this).attr('id');
            var vl = $(this).val();
            $('[ref="'+nm+'"]').html(vl);
        });
        $('#wizard-validation-form textarea').blur(function() {
            var nm = $(this).attr('name');
            var vl = $(this).val();
            $('[ref="'+nm+'"]').html(vl);
        });
        $('#wizard-validation-form select').blur(function() {
            var nm = $(this).attr('name');
            var vl = $(this).find('option:selected').text();
            $('[ref="'+nm+'"]').html(vl);
        });

        function setDateCloture(delai,type) {
            if(delai != '' && type != '' && delai != null && type != null) {
                var today = new Date();
                var dl = parseInt(delai);
                // To add Days
                if(type=='jour')    dl = dl;
                if(type=='semaine') dl = dl*7;
                if(type=='mois')    dl = dl*30;
                today.setDate(today.getDate() + dl);
                $('#datecloture').val(today.toLocaleDateString());
            }
            else
                $('#datecloture').val('');

        }
        $('#delai, #typedelai, #datecloture').val('');
        $('#delai, #typedelai').each(function() {
            $(this).change(function() {
                return setDateCloture($('#delai').val(),$('#typedelai').val());
            });
            $(this).keyup(function() {
                return setDateCloture($('#delai').val(),$('#typedelai').val());
            });
        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Ajouter une formation</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title"><b></b></h4>
                            <p class="text-muted m-b-30 font-13">
                                Utiliser le bouton <code>Suivant</code> pour passer à l'étape suivante
                            </p>

                            <form id="wizard-validation-form" class="bg-dark" method="post" action="{{route('formations.store')}}" enctype="multipart/form-data">
                                @csrf
                                <div>
                                    <h3>Étape 1</h3>
                                    <section class="table-responsive">
                                        <div class="form-group clearfix">
                                            <label class="col-lg-12 control-label ">(*) Champs obligatoires </label>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="titre">Titre *</label>
                                            <div class="col-lg-10">
                                                <input class="required form-control input-lg" id="titre" name="titre" type="text" required>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="categorie"> Catégories</label>
                                            <div class="col-lg-10">
                                                @php
                                                    $categories = \App\Catformation::all();
                                                @endphp
                                                <select class="select2 select2-multiple" name="categories[]" multiple="multiple" multiple data-placeholder="Choisir ...">
                                                    @foreach ($categories as $categorie)
                                                        <option value="{{$categorie->id}}">{{$categorie->libelle}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="prerequis">Préréquis</label>
                                            <div class="tags-default col-lg-10">
												<input id="prerequis" name="prerequis" type="text" class="form-control" data-role="tagsinput" placeholder="Préréquis"/>
											</div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="elm1">Description</label>
                                            <div class="col-lg-10">
										        <textarea id="elm1" name="description" style="height: 200px;"></textarea>
											</div>
                                        </div>
                                        <p>&nbsp;</p>

                                    </section>
                                    <h3>Étape 2</h3>
                                    <section class="table-responsive">

                                        <div class="form-group clearfix">
                                            <label class="col-lg-12 control-label ">(*) Champs obligatoires </label>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="cible">La cible *</label>
                                            <div class="tags-default col-lg-10">
												<input id="cible" name="cible" type="text" class="form-control" data-role="tagsinput" placeholder="La cible" required/>
											</div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="objectifs">Les objectifs *</label>
                                            <div class="tags-default col-lg-10">
												<input id="objectifs" name="objectif" type="text" class="form-control" data-role="tagsinput" placeholder="Les objectifs" required/>
											</div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="elm2">Les compétences à acquérir</label>
                                            <div class="col-lg-10">
										        <textarea id="elm2" name="competence"></textarea>
											</div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label" for="lien"> Lien (si en ligne)</label>
                                            <div class="col-lg-10">
                                                <input id="lien" name="lien" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="salle"> Salle (si en présentiel)</label>
                                            <div class="col-lg-10">
                                                <input id="salle" name="salle" type="text" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="prix">Prix (si payant) FCFA</label>
                                            <div class="col-lg-10">
                                                <input id="prix" name="prix" type="text" class="form-control" onkeyup="formatNumber_prix(this)">
                                            </div>
                                        </div>

                                        <div class="form-group clearfix portlets">
                                            <label class="col-lg-2 control-label " for="cover">Image de couveture (Taille max = 10Mo)</label>
                                            <div class="col-lg-10">
                                                <div class="fallback">
                                                    <input name="file" type="file" name="cover" class="form-control" />
                                                  </div>
                                            </div>
                                        </div>

                                    </section>
                                    <h3>Étape 3</h3>
                                    <section>
                                        <div class="form-group clearfix">
                                            <label class="col-lg-12 control-label ">(*) Champs obligatoires </label>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="motscles">Mots clés</label>
                                            <div class="tags-default col-lg-10">
												<input id="motscles" name="motscles" type="text" class="form-control" data-role="tagsinput" placeholder="Mots clès"/>
											</div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="debut">Date début </label>
                                            <div class="tags-default col-lg-10">
												<input id="debut" name="debut" type="date" min="{{date('Y-m-d')}}" class="form-control required"/>
											</div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label">Durée <span class="text-danger">*</span></label>
                                            <div class="col-lg-3 col-xl-3">
                                                <input class="form-control required" name="duree" id="delai" type="number" min="1" max="1000" value="" required />
                                            </div>
                                            <div class="col-xl-7 col-lg-7">
                                                <select class="form-control required" name="typedelai" id="typedelai" required>
                                                    <option value="jour">Jour(s)</option>
                                                    <option value="semaine">Semaine(s)</option>
                                                    <option value="mois">Mois</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label" for="datecloture">Date de fin</label>
                                            <div class="col-lg-10">
                                                <input class="form-control" name="datecloture" id="datecloture" type="text" placeholder="Choisir le délai..." readonly required/>
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="professeur">Professeur</label>
                                            <div class="col-lg-10">
                                                @php
                                                    $professeurs = \App\User::where('role_id', 3)->get();
                                                @endphp
                                                <select class="select2- required form-control" name="professeur" data-placeholder="Choisir ...">
                                                    <option value=""></option>
                                                    @foreach ($professeurs as $prof)
                                                        <option value="{{$prof->id}}">{{$prof->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label class="col-lg-2 control-label " for="statut">Statut</label>
                                            <div class="col-lg-10">
                                                <select class="form-control" name="statut" data-placeholder="Choisir ...">
                                                    <option value="0">Brouillon</option>
                                                    <option value="1">En ligne</option>
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="etp_id" value="1">
                                        {{-- <div class="form-group clearfix">
                                            <button class="btn btn-primary btn-lg" type="submit"> Valider </button>
                                        </div> --}}
                                    </section>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
