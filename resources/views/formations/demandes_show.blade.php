<!-- détails de la formation -->
@php
    $formation = $item->formation;
    $professeur = $item->formation->professeur;
    $usr = $item->etudiant;
    $etudiant = $usr->etudiant;
@endphp
@if ($item->statut == 0)
<div class="row">
    <!-- btn valider & btn réfuser -->
    <div class="col-lg-12">
        <div class="col-lg-5">
            <form style="display: none;" method="POST" id="formValid" action="{{route('formations.demandes.confirm', encrypt($item->id))}}">
                @csrf
                <input type="hidden" name="statut" value="1">
                <!-- commnentaire -->
                <div class="form-group">
                    <label for="commentaire">Commentaire</label>
                    <textarea name="commentaire" id="commentaire" cols="30" rows="3" placeholder="Ajouter un commentaire" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <input type="hidden" name="id" value="{{encrypt($item->id)}}">
                    <button type="submit" class="btn btn-success btn-lg- btn-block" id="btnValid">Valider</button>
                </div>
            </form>
            <a type="javascript:void(0)" class="btn btn-success btn-lg btn-block" id="btnV"> <i class="md-thumb-up"></i> Accepter</a>
        </div>
        <div class="col-lg-2"></div>
        <div class="col-lg-5">
            <form style="display: none;" method="POST" id="formReject" action="{{route('formations.demandes.confirm', encrypt($item->id))}}">
                @csrf
                <input type="hidden" name="statut" value="2">
                <!-- commnentaire -->
                <div class="form-group">
                    <label for="commentaire2">Commentaire<span class="text-danger">*</span></label>
                    <textarea name="commentaire" id="commentaire2" cols="30" rows="3" placeholder="Ajouter un commentaire" class="form-control" required></textarea>
                </div>
                <div class="form-group">
                    <input type="hidden" name="id" value="{{encrypt($item->id)}}">
                    <button type="submit" class="btn btn-danger btn-lg btn-block" id="btnReject">Rejéter</button>
                </div>
            </form>
            <a type="javascript:void(0)" class="btn btn-danger btn-lg btn-block" id="btnR"> <i class="md-thumb-down"></i> Rejéter</a>
        </div>
    </div>
</div>
<div class="row"><hr></div>
@endif
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <ul class="nav nav-pills m-b-30">
                    <li class="active">
                        <a href="#dt_for" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="fa fa-file-o"></i></span>
                            <span class="hidden-xs"><b>Détails de la formation</b> </span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#dt_etu" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs"><i class="fa fa-user"></i></span>
                            <span class="hidden-xs"><b>Profil de l'étudiant</b> </span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content br-n pn">
                    <div class="tab-pane active" id="dt_for">
                        <table class="table table-striped table-bordered-">
                            <tr>
                                <th>Titre</th>
                                <td>{{$formation->titre}}</td>
                            </tr>
                            <tr>
                                <th>Professeur</th>
                                <td>{{$professeur->name}}</td>
                            </tr>
                            @if ($formation->niveau!=null)
                                <tr>
                                    <th>Niveau</th>
                                    <td>{{$formation->niveau}}</td>
                                </tr>
                            @endif
                            <tr>
                                <th>Prérequis</th>
                                <td>
                                    @php
                                        $prerequis = explode(',',$formation->prerequis);
                                        foreach ($prerequis as $acquis) {
                                            echo '<span class="badge badge-primary">'.$acquis.'</span> ';
                                        }
                                    @endphp
                                </td>
                            </tr>
                            <tr>
                                <th>Mots clés</th>
                                <td>
                                    @php
                                        $mots = explode(',',$formation->motscles);
                                        foreach ($mots as $mot) {
                                            echo '<span class="badge badge-primary">'.$mot.'</span> ';
                                        }
                                    @endphp
                                </td>
                            </tr>
                            <tr>
                                <th>Date début</th>
                                <td>{{$formation->debut->format('d/m/Y')}}</td>
                            </tr>
                            <tr>
                                <th>Durée</th>
                                <td>{{$formation->duree}} {{$formation->typedelai}}</td>
                            </tr>
                            <tr>
                                <th>Prix</th>
                                <td>{{($formation->prix>0) ? number_format($formation->prix,0,'',' ').' FCFA' : 'Gratuit'}}</td>
                            </tr>
                            <tr>
                                <th>Type de formation</th>
                                <td>
                                    {{($formation->lien!=null) ? 'En ligne' : 'En présentiel'.(($formation->salle!=null) ? ' - Salle : '.$formation->salle : '')}}
                                </td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{!!$formation->description!!}</td>
                            </tr>
                            <tr>
                                <th>Cible</th>
                                <td>{!!$formation->cible!!}</td>
                            </tr>
                            <tr>
                                <th>Objectif</th>
                                <td>{!!$formation->objectif!!}</td>
                            </tr>
                            <tr>
                                <th>Compétences à acquérir</th>
                                <td>{!!$formation->competence!!}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="tab-pane " id="dt_etu">
                        <table class="table table-striped table-bordered-">
                            <tr>
                                <th width="220">Nom</th>
                                <td>{{$etudiant->civilite}} {{$usr->name}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$usr->email}}</td>
                            </tr>
                            <tr>
                                <th>Niveau</th>
                                <td>{{($etudiant->niveau!='') ? $etudiant->niveau : 'Non définit'}}</td>
                            </tr>
                            <tr>
                                <th>Formation</th>
                                <td>{{($etudiant->formation!='') ? $etudiant->formation : 'Non définit'}}</td>
                            </tr>
                            <!-- datenaissance -->
                            <tr>
                                <th>Date de naissance</th>
                                <td>
                                    @php
                                        $datenaiss = Illuminate\Support\Carbon::createFromFormat('Y-m-d',$etudiant->datenaiss);
                                        $age = $datenaiss->diffInYears(date('Y-m-d'));
                                    @endphp
                                    {{$datenaiss->format('d/m/Y')}} ({{$age}} an{{($age>1) ? 's' : ''}})
                                </td>
                            </tr>
                            <tr>
                                <th>Lieu de naissance</th>
                                <td>{{$etudiant->lieunaiss}}</td>
                            </tr>
                            <tr>
                                <th>Nationalité</th>
                                <td>{{$etudiant->nationalite}}</td>
                            </tr>
                            <tr>
                                <th>Contact 1</th>
                                <td>{{$etudiant->contact1}}</td>
                            </tr>
                            @if ($etudiant->contact2!=null)
                                <tr>
                                    <th>Contact 2</th>
                                    <td>{{$etudiant->contact2}}</td>
                                </tr>
                            @endif
                            <tr>
                                <th>Adresse</th>
                                <td>{{$etudiant->adresse}}</td>
                            </tr>
                            <tr>
                                <th>Ville</th>
                                <td>{{$etudiant->ville}}</td>
                            </tr>
                            <tr>
                                <th>Etablissement fréquenté</th>
                                <td>{{$etudiant->etablissement}}</td>
                            </tr>
                            @if ($usr->img != null)
                                <tr>
                                    <th>Photo</th>
                                    <td>
                                        @if ($usr->img != null)
                                            <img src="{{asset($etudiant->img)}}" alt="photo" width="100">
                                        @else
                                            <img src="{{asset('assets/images/big/img5.jpg')}}" alt="photo" width="100">
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                    <!-- end All results tab -->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#btnV').click(function(){
            $('#btnV, #formReject').hide();
            $('#formValid').show();
        });
        $('#btnR').click(function(){
            $('#btnR, #formValid').hide();
            $('#formReject').show();
        });
    } );
</script>
