@extends('layouts.app')

@section('title')
    Formations
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')

    <script type="text/javascript" src="{{ url('assets/plugins/isotope/js/isotope.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}
    <script type="text/javascript">
        $(window).load(function(){
            var $container = $('.portfolioContainer');
            $container.isotope({
                filter: '*',
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });

            $('.portfolioFilter a').click(function(){
                $('.portfolioFilter .current').removeClass('current');
                $(this).addClass('current');

                var selector = $(this).attr('data-filter');
                $container.isotope({
                    filter: selector,
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                    }
                });
                return false;
            });
        });
        $(document).ready(function() {
            $('.image-popup').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                mainClass: 'mfp-fade',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                }
            });
        });

        $('.action-obj').each(function(index,element){
            $(this).click(function (event) {
                event.preventDefault();
                //class danger for tr parent
                var href = $(this).attr('ref');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez-vous vraiment supprimer cet étudiant ?",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui, supprimer"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                });
            });

        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Formations
                            <a href="{{route('formations.create')}}" class="btn btn-default btn-md waves-effect waves-light" style="float: right;"><i class="md md-add"></i> Ajouter</a>
                        </h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">{{env('APP_NAME')}}</a>
                            </li>
                            <li class="active">
                                Formations
                            </li>
                        </ol>
                    </div>
                    <div class="col-md-12">

                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 ">
                        <div class="portfolioFilter">
                            {{-- <a href="#" data-filter="*">All</a> --}}
                            @php
                                $count = 0;
                            @endphp
                            @foreach ($data as $key=>$dt)
                                @php
                                    $count += count($dt);
                                @endphp
                                <a href="#" data-filter=".{{Str::slug($key)}}">{{$key}}</a>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-8">
                        <form role="form" action="{{route('formations.search')}}" method="GET">
                            <div class="form-group contact-search m-b-30">
                                <input type="text" id="search" name="search" class="form-control" value="{{$search}}" placeholder="Recherche ...">
                                <button type="submit" id="searchFor" class="btn btn-white"><i class="fa fa-search"></i>
                            </div> <!-- form-group -->
                        </form>
                    </div>
                </div>

                <div class="row port">
                    <div class="portfolioContainer m-b-15" id="SrContainer">
                        @if ($count > 0)
                            @php
                                $e=0;
                            @endphp
                            @foreach ($data as $i => $dt)
                                @php
                                    $e++;
                                @endphp
                                @foreach ($dt as $item)
                                    <div class="col-sm-6 col-lg-4 {{Str::slug($i)}}">
                                        @php
                                            $color = '';
                                            if($e == 1) $color = '#666666';
                                            if($e == 2) $color = '#04d01c';
                                            if($e == 3) $color = '#be9d30';
                                        @endphp
                                        <div class="card-box" style="border-bottom: 2px solid {{$color}}">
                                            <div class="contact-card">
                                                <a class="pull-left" href="#">
                                                    @if ($item->img != null)
                                                        <img class="img-circle" src="{{asset($item->img)}}" alt="cover">
                                                    @else
                                                        <img class="img-circle" src="{{asset('assets/images/big/img5.jpg')}}" alt="cover">
                                                    @endif
                                                </a>
                                                <div class="member-info">
                                                    <h4 class="m-t-0 m-b-5 header-title"><b>{{$item->titre}}</b></h4>
                                                    <p class="text-muted">
                                                        @foreach ($item->categories as $cat)
                                                            {{$cat->catformation->libelle}}
                                                        @endforeach
                                                    </p>
                                                    <p class="text-dark"><i class="fa fa-user-secret m-r-10"></i><small>{{ucfirst($item->professeur->name)}}</small></p>
                                                    <div class="contact-action">
                                                        @if ($item->statut == '0')
                                                            <a ref="{{route('formations.online', encrypt($item->id))}}" class="btn btn-success btn-sm action-obj" title="Publier"><i class="md md-input"></i></a>
                                                        @endif
                                                        {{-- <a href="#" class="btn btn-info btn-sm"><i class="md md-query-builder"></i></a> --}}
                                                        <a href="{{route('formations.edit', encrypt($item->id))}}" class="btn btn-warning btn-sm"><i class="md md-mode-edit"></i></a>
                                                        @if ($item->statut == '0')
                                                            <a ref="{{route('formations.delete', encrypt($item->id))}}" class="btn btn-danger btn-sm action-obj"><i class="md md-close"></i></a>
                                                        @endif
                                                    </div>
                                                </div>

                                                <ul class="social-links list-inline m-0">
                                                    <li>
                                                        <span title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Date de début"><i class="fa fa-calendar-check-o"></i> {{Illuminate\Support\Carbon::parse($item->debut)->format('d/m/Y')}}</span>
                                                    </li>
                                                    <li style="float:right;">
                                                        <span title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Durée"><i class="fa fa-clock-o"></i> {{$item->duree.' '.ucfirst($item->typedelai)}}</span>
                                                    </li>
                                                {{--  <li>
                                                        <a title="" data-placement="top" data-toggle="tooltip" class="tooltips" href="" data-original-title="Mots clés"><i class="fa fa-tags"></i> {{ucwords($item->motscles)}}</a>
                                                    </li> --}}
                                                </ul>
                                            </div>
                                        </div>

                                    </div> <!-- end col -->
                                @endforeach
                            @endforeach
                        @else
                            <h3 class="text-warning">&nbsp; Aucune formation trouvées !</h3>
                        @endif
                    </div>
                </div>



            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
