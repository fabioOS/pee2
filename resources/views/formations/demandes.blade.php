@extends('layouts.app')

@section('title')
    Demandes de formation
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')

    <script type="text/javascript" src="{{ url('assets/plugins/isotope/js/isotope.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js') }}"></script>

    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable();
            $('.btnAction').each(function(index, element){
                $(this).click(function() {
                    var container = $('#dmContainer');
                    container.html('<div class="text-center mb-4"><i class="fa fa-spinner fa-spin fa-3x fa-fw text-secondary"></i> Chargement ...</div>');
                    var data = '';
                    var url  = $(this).attr('ref');
                    console.log(url);
                    //ajax header
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    });
                    //multiorigine (Cross-Origin Request)
                    $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
                        options.crossDomain ={
                            crossDomain: true
                        };
                        options.xhrFields = {
                            withCredentials: true
                        };
                    });
                    $.ajax({
                        type: "GET",
                        url: url,
                        data: data,
                        //dataType: "json",
                        cache: false,
                        beforeSend: function() {
                            container.html('<div class="text-center m-6"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i> Chargement ...</div>');
                        },
                        success: function(data) {
                            container.html(data);
                        },
                        error: function(data) {
                            console.log(data);
                            if(data.status == 401){
                                document.location.href = "{{route('login')}}?backto=formations/demandes";
                                ///container.html('<div class="text-center m-6"><i class="fa fa-times-circle fa-3x text-danger"></i> <span class="mb-4">Vous devez vous connecter pour accéder à cette page ! <a class="./login"> Connexion </a> </span> </div>');
                            }
                        }
                    });
                });
            });
        } );
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Demandes de formation
                        </h4>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">{{env('APP_NAME')}}</a>
                            </li>
                            <li class="active">
                                Demandes de formation
                            </li>
                        </ol>
                    </div>
                    <div class="col-md-12">

                    </div>
                </div>


                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <form action="{{route('formations.demandes')}}" method="GET">
                            <div class="input-group m-t-10">
                                <input type="text" id="" name="search" value="{{$search}}" class="form-control input-lg" placeholder="Recherche...">
                                <span class="input-group-btn">
                                <button type="submit" class="btn waves-effect waves-light btn-default btn-lg"><i class="fa fa-search m-r-5"></i> Recherche</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center m-t-30">
                        @if ($search != '')
                            <h4 class="m-b-30"> <strong>{{($total>0 && $total<10)?'0':''}}{{$total}}</strong> Résultat{{$total>1?'s':''}} </h4>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="search-result-box m-t-40">
                            <ul class="nav nav-tabs">
                                @foreach ($data as $label=>$item)
                                    @php
                                        $count = count($item);
                                    @endphp
                                    <li class="{{($loop->index==0)?'active':''}}">
                                        <a href="#dm{{$loop->index}}" data-toggle="tab" aria-expanded="{{($loop->index==0)?'true':'false'}}">
                                            <span class="visible-xs"><i class="fa fa-user"></i></span>
                                            <span class="hidden-xs"><b>{{$label}}</b> <span class="badge badge-pink m-l-10">{{$count}}</span> </span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="tab-content">
                                @foreach ($data as $label=>$demands)
                                    <div class="tab-pane {{($loop->index==0)?'active':''}}" id="dm{{$loop->index}}">
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Date de demande</th>
                                                    <th>Etudiant</th>
                                                    <th>Formation</th>
                                                    <th>Début de la formation</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            @foreach ($demands as $item)
                                                <tr>
                                                    <td>
                                                        {{$item->created_at->format('d/m/Y')}}
                                                        <br> <span class="label label-table label-warning">{{$item->created_at->diffForHumans()}}</span>
                                                    </td>
                                                    <td>{{$item->etudiant->name}}</td>
                                                    <td>{{$item->formation->titre}}</td>
                                                    <td>
                                                        {{$item->formation->debut->format('d/m/Y')}}
                                                        <br>
                                                        @php
                                                            //datediff between datecloture and created_at
                                                            $datediff = strtotime($item->formation->debut) - strtotime(now());
                                                            $nbjours = round($datediff / (60 * 60 * 24));
                                                            if ($nbjours>0) {
                                                                echo '<span class="label label-lg font-weight-bold label-danger label-inline">Dans '.$nbjours.' jour(s)</span>';
                                                            }
                                                            else{
                                                                echo '<span class="label label-table label-danger">Déjà terminée</span>';}
                                                        @endphp
                                                        {{-- <span class="label label-table label-danger">Dans {{$item->formation->debut->diffInDays(now()->format('Y-m-d'))}} jour(s)</span> --}}
                                                    </td>
                                                    <td>
                                                        <div class="button-list">
                                                            {{-- @if($nbjours<0)
                                                                <a href="javascript:void(0)" title="Aperçu" class="btn btn-primary btn-icon waves-effect waves-light disabled"> <i class="ion ion-document-text fa-2x"></i> </a>
                                                            @else
                                                                <a href="javascript:void(0)" ref="{{route('formations.demandes.show', encrypt($item->id))}}" title="Aperçu" class="btn btn-primary btn-icon waves-effect waves-light btnAction" data-toggle="modal" data-target="#custom-width-modal"> <i class="ion ion-document-text fa-2x"></i> </a>
                                                            @endif --}}
                                                            <a href="javascript:void(0)" ref="{{route('formations.demandes.show', encrypt($item->id))}}" title="Aperçu" class="btn btn-primary btn-icon waves-effect waves-light btnAction" data-toggle="modal" data-target="#custom-width-modal"> <i class="ion ion-document-text fa-2x"></i> </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                @endforeach
                                <!-- end All results tab -->
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- container -->
        </div> <!-- content -->
    </div>

    <div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="width:55%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="custom-width-modalLabel">Aperçu de la demande</h4>
                </div>
                <div class="modal-body bg-light" id="dmContainer">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Fermer</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
