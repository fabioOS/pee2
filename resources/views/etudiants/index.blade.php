@extends('layouts.app')

@section('title')
    Étudiants
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')

    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
            $('#myTable').dataTable({
                language: {
                    lengthMenu: "_MENU_",
                    search: "_INPUT_",
                    searchPlaceholder: "Recherche",
                    sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    sInfoPostFix:    "",
                    sLoadingRecords: "Chargement en cours...",
                    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    oPaginate: {
                        "sFirst":    "Premier",
                        "sLast":    "Dernier",
                        "sNext":    "Suivant",
                        "sPrevious": "Précédent"
                    },
                }
            });
        });
        $('.remove-obj').each(function(index,element){
            $(this).click(function (event) {
                event.preventDefault();
                //class danger for tr parent
                $(this).parent().parent().parent().addClass('bg-warning');
                var href = $(this).attr('ref');
                swal({
                    title: "Êtes-vous sûr?",
                    text: "Voulez-vous vraiment supprimer cet étudiant ?",
                    icon: "warning",
                    buttons: true,
                    buttons: ["Annuler", "Oui, supprimer"],
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        window.location = href;
                    }
                    else {
                        $(this).parent().parent().parent().removeClass('bg-warning');
                    }
                });
            });

        });

        /* $('#check_all').on('click', function(e){
            if($(this).is(':checked',true)){
                $(".role_item").prop('checked', true);
            }else{
                $(".role_item").prop('checked', false);
            }
        }); */

       /*  $('#sendActionGroup').on('click', function(e) {

            if($('#actionGroup').val()=='del_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à supprimer!","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment supprimer ces lignes",
                        icon: "danger",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDelete) => {
                        if(willDelete) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('etudiants.deletes') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }
            else if($('#actionGroup').val()=='dis_select'){
                var allVals = [];
                $(".role_item:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });
                //console.log(allVals.length);
                // return false;
                if(allVals.length <=0)
                {
                    swal("Oops","Veuillez selectionnez des lignes à désactiver !","error" )
                }
                else {
                    //$("#loading").show();
                    swal({
                        title: "Êtes-vous sûr?",
                        text: "Voulez vous vraiment désactiver ces lignes",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Annuler", "Oui"],
                        dangerMode: true,
                    }).then((willDisable) => {
                        if(willDisable) {
                            var valueIds = allVals.join(",");

                            $.ajax({
                                url:"{{ route('etudiants.disable') }}",
                                method:"GET",
                                data:{value:valueIds},
                                success:function(res){
                                    console.log(res);
                                    if(res == '1'){
                                        //$("#loading").hide();
                                        location.reload();
                                    }else{
                                        //$("#loading").hide();
                                        location.reload();
                                    }
                                }
                            })
                        }
                    });
                }
            }
            else{
                return false;
                //swal("Oops","Something went wrong!","error" )
            }

        }); */
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Étudiants
                            <a href="{{route('etudiants.create')}}" class="btn btn-default btn-xs" style="margin-right: 12px;">Ajouter</a>
                            <a href="{{route('etudiants.import')}}" class="btn btn-warning btn-xs">Importer</a>
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive">

                            {{-- <div class="form-inline">
                                <div class="form-group">
                                    <select name="actionGroup" class="form-control" id="actionGroup">
                                        <option value="">Action groupée</option>
                                        <option value="dis_select">Désactiver</option>
                                        <option value="del_select">Supprimer</option>
                                    </select>
                                </div>
                                <button type="submit" id="sendActionGroup" class="btn btn-default waves-effect waves-light m-l-10 btn-md">Appliquer</button>
                            </div>
                            <br> --}}

                            <table id="myTable" class="table datatable- table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th width="10">N°</th>
                                    {{-- <th width="10"> <input type="checkbox" id="check_all"></th> --}}
                                    <th>Nom</th>
                                    <th>Email </th>
                                    <th>Civilite</th>
                                    <th>Niveau d'étude</th>
                                    <th>Formation</th>
                                    <th>Spécialité</th>
                                    <th>Date de naissance</th>
                                    <th>Lieu de naissance</th>
                                    <th>Nationalité</th>
                                    <th>Contact 1</th>
                                    <th>Contact 2</th>
                                    <th>Catégorie</th>
                                    <th>Date de création</th>
                                    <th>Dernière connexion</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $k=>$user)
                                    <tr>
                                        <td width="10">{{$k+1}}</td>
                                        {{-- <td width="10">
                                            <input type="checkbox" class="role_item" data-id="{{$user->id}}">
                                        </td> --}}
                                        <td style="font-weight: bold">
                                            <a class="text-dark" href="{{route('etudiants.show',encrypt($user->email))}}">{{$user->name}}</a>
                                        </td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->sexe?:''}}</td>
                                        <td>{{$user->niveau?:''}}</td>
                                        <td>{{$user->formation?:''}}</td>
                                        <td>{{$user->specialite?:''}}</td>
                                        <td>{{$user->datenaiss?:''}}</td>
                                        <td>{{$user->lieunaiss?:''}}</td>
                                        <td>{{$user->nationalite?:''}}</td>
                                        <td>{{$user->contact1?:''}}</td>
                                        <td>{{$user->contact2?:''}}</td>
                                        <td>{{$user->type?:''}}</td>
                                        <td>{{$user->created_at->format('d/m/Y')}}</td>
                                        <td>{{\Illuminate\Support\Carbon::parse($user->last_login_at)->format('d/m/Y H:i:s')}}</td>
                                        <td>
                                            <div class="button-list" style="width: 150px;">
                                                <a class="btn btn-icon waves-effect waves-light btn-info show-obj" href="{{route('etudiants.show',encrypt($user->email))}}" title="Aperçu"> <i class="fa fa-file-text"></i> </a>
                                                <a class="btn btn-icon waves-effect waves-light btn-primary edit-obj" href="{{route('etudiants.edit',encrypt($user->email))}}" id="Éditer"> <i class="fa fa-pencil"></i> </a>
                                                <button class="btn btn-icon waves-effect waves-light btn-danger remove-obj" ref="{{route('etudiants.delete', encrypt($user->email))}}" title="Supprimer"> <i class="fa fa-trash-o"></i> </button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
