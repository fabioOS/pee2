@extends('layouts.app')

@section('title')
    Étudiants
    @parent
@stop

@section('header_styles')
    <!-- DataTables -->
    <link href="{{ asset('assets/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('footer_scripts')

    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    {{--<script src="{{ asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>--}}

    <script type="text/javascript">
        $('#myTable').dataTable();
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-title">
                            Étudiants
                            <a href="{{route('etudiants')}}" class="btn btn-default btn-xs" style="margin-right: 12px;"> <i class="fa fa-chevron-left"></i> Retour</a>
                        </h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-8">
                        <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30">Informations du compte</h4>
                            <table class="table table-striped table-bordered actus">
                                <thead>
                                <tr>
                                    <th style="width: 300px;">Nom</th>
                                    <td>{{$user->name}}</td>
                                </tr>
                                <tr>
                                    <th>Email </th>
                                    <td>{{$user->email}}</td>
                                </tr>
                                <tr>
                                    <th>Civilite</th>
                                    <td>{{$user->sexe?:''}}</td>
                                </tr>
                                <tr>
                                    <th>Niveau d'étude</th>
                                    <td>{{$user->niveau?:''}}</td>
                                </tr>
                                <tr>
                                    <th>Formation</th>
                                    <td>{{$user->formation?:''}}</td>
                                </tr>
                                <tr>
                                    <th>Spécialité</th>
                                    <td>{{$user->specialite?:''}}</td>
                                </tr>
                                <tr>
                                    <th>Date de naissance</th>
                                    <td>{{$user->datenaiss?:''}}</td>
                                </tr>
                                <tr>
                                    <th>Lieu de naissance</th>
                                    <td>{{$user->lieunaiss?:''}}</td>
                                </tr>
                                <tr>
                                    <th>Nationalité</th>
                                    <td>{{$user->nationalite?:''}}</td>
                                </tr>
                                <tr><th>Contact 1</th>
                                    <td>{{$user->contact1?:''}}</td>
                                </tr>
                                <tr>
                                    <th>Contact 2</th>
                                    <td>{{$user->contact2?:''}}</td>
                                </tr>
                                <tr>
                                    <th>Catégorie d'entrepreneur</th>
                                    <td>{{$user->type?:''}}</td>
                                </tr>
                                <tr>
                                    <th>Date de création</th>
                                    <td>{{$user->created_at->format('d/m/Y')}}</td>
                                </tr>
                                <tr>
                                    <th>Dernière connexion</th>
                                    <td>{{\Illuminate\Support\Carbon::parse($user->last_login_at)->format('d/m/Y H:i:s')}}</td>
                                </tr>
                                </thead>
                            </table>
                            <a class="btn btn-icon- waves-effect waves-light btn-primary" href="{{route('etudiants.edit',encrypt($user->email))}}" id="Éditer"> <i class="fa fa-pencil"></i> Éditer le compte</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4 class="header-title m-t-0 m-b-30">Formations</h4>
                        <div class="card-box">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12">
                                    <div class="widget-panel widget-style-2 ">
                                        <i class="fa fa-book text-primary"></i>
                                        <h2 class="m-0 text-dark counter font-600">{{$formations}}</h2>
                                        <div class="text-muted m-t-5">Formation(s) effectuée(s)</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-box">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12">
                                    <div class="widget-panel widget-style-2 ">
                                        <i class="md md-announcement text-primary"></i>
                                        <h2 class="m-0 text-dark counter font-600">{{$demandes}}</h2>
                                        <div class="text-muted m-t-5">Demande(s) de formation en attente</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
