@extends('layouts.app')

@section('title')
    Modifier un étudiant
    @parent
@endsection


@section('header_styles')
<link rel="stylesheet" href="{{ asset('assets/plugins/dropify/dropify.css') }}">
<!-- Dropzone -->
{{-- <link rel="stylesheet" href="{{ asset('assets/fronts/plugins/dropzone/dropzone.min.css') }}"> --}}
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/pages/jquery.sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/dropify/dropify.js') }}"></script>
    <!-- Dropzone JS -->
    {{-- <script src="{{ asset('assets/fronts/plugins/dropzone/dropzone.min.js') }}"></script> --}}
    <script>
        $('.dropify').dropify();
        $('.select2').select2();
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Modifier un étudiant</h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            @include('etu.include.messages')
                            @include('etu.include.errors')
                        </div>
                    </div>
                </div>

                <div class="row bg-white">
                    <div class="col-lg-12">
                        <ul class="nav nav-tabs tabs">
                            <li class="active tab">
                                <a href="#home-2" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                                    <span class="hidden-xs">Informations de compte</span>
                                </a>
                            </li>
                            <li class="tab">
                                <a href="#profile-2" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                                    <span class="hidden-xs">Mot de passe</span>
                                </a>
                            </li>
                            <li class="tab">
                                <a href="#messages-2" data-toggle="tab" aria-expanded="true">
                                    <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                                    <span class="hidden-xs">Photo de profil</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="home-2">

                                <form class="col-md-12" action="{{route('etudiants.updateprofil')}}" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        @csrf
                                        <input type="hidden" name="etu" value="{{encrypt(($user->id))}}">
                                        <div class="row text-warning- mb-4">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="form-control-label">Dans quelle catégorie d'entrepreneur vous vous situez <span class="text-danger">*</span></label>
                                                </div>
                                                <div class="wallet-method">
                                                    <label class="radio-inline custom_radio me-4" style="font-weight: 700; font-size:16px; letter-spacing:1px;">
                                                        <input type="radio" name="typeinvest" value="Initiateur" required {{($etu->type=='Initiateur')?' checked="true"':''}}>
                                                        <span class="checkmark border border-danger"></span> Initiateur
                                                    </label>
                                                    <label class="radio-inline custom_radio me-4" style="font-weight: 700; font-size:16px; letter-spacing:1px;">
                                                        <input type="radio" name="typeinvest" value="Inventeur" required {{($etu->type=='Inventeur')?' checked="true"':''}}>
                                                        <span class="checkmark border border-danger"></span> Inventeur
                                                    </label>
                                                    <label class="radio-inline custom_radio me-4" style="font-weight: 700; font-size:16px; letter-spacing:1px;">
                                                        <input type="radio" name="typeinvest" value="Promoteur" required {{($etu->type=='Promoteur')?' checked="true"':''}}>
                                                        <span class="checkmark border border-danger"></span> Promoteur
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <h3>&nbsp;</h3>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-control-label">Nom & prénom<span class="text-danger">*</span></label>
                                                    <input type="text" name="nom" class="form-control" required value="{{$user->name}}">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-control-label">Civilité<span class="text-danger">*</span></label>
                                                    <select class="form-select form-control" name="civilite" required>
                                                        <option value=""></option>
                                                        <option value="Monsieur" {{$user->sexe=='Monsieur' ? 'selected' : '' }}>Monsieur</option>
                                                        <option value="Madame" {{$user->sexe=='Madame' ? 'selected' : '' }}>Madame</option>
                                                        <option value="Mademoiselle" {{$user->sexe=='Mademoiselle' ? 'selected' : '' }}>Mademoiselle</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        @if ($etu != null)
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Niveau d'étude<span class="text-danger">*</span></label>
                                                        <select class="form-select form-control" name="niveau" required>
                                                            <option value="Aucun" {{$etu->niveau=='Aucun' ? 'selected' : '' }}>Aucun</option>
                                                            <option value="CEPE" {{$etu->niveau=='CEPE' ? 'selected' : '' }}>CEPE</option>
                                                            <option value="BEPC" {{$etu->niveau=='BEPC' ? 'selected' : '' }}>BEPC</option>
                                                            <option value="BAC" {{$etu->niveau=='BAC' ? 'selected' : '' }}>BAC</option>
                                                            <option value="BTS" {{$etu->niveau=='BTS' ? 'selected' : '' }}>BTS</option>
                                                            <option value="LICENCE 1" {{$etu->niveau=='LICENCE 1' ? 'selected' : '' }}>LICENCE 1</option>
                                                            <option value="LICENCE 2" {{$etu->niveau=='LICENCE 2' ? 'selected' : '' }}>LICENCE 2</option>
                                                            <option value="LICENCE 3" {{$etu->niveau=='LICENCE 3' ? 'selected' : '' }}>LICENCE 3</option>
                                                            <option value="MASTER 1" {{$etu->niveau=='MASTER 1' ? 'selected' : '' }}>MASTER 1</option>
                                                            <option value="MASTER 2" {{$etu->niveau=='MASTER 2' ? 'selected' : '' }}>MASTER 2</option>
                                                            <option value="INGENIEUR DES TECHNIQUES" {{$etu->niveau=='INGENIEUR DES TECHNIQUES' ? 'selected' : '' }}>INGENIEUR DES TECHNIQUES</option>
                                                            <option value="INGENIEUR DE CONCEPTION" {{$etu->niveau=='INGENIEUR DE CONCEPTION' ? 'selected' : '' }}>INGENIEUR DE CONCEPTION</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Formation</label>
                                                        <input type="text" name="formation" class="form-control" value="{{$etu->formation}}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Etablissement fréquenté</label>
                                                        <input type="text" name="etablissement" class="form-control" value="{{$etu->etablissement}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Nationalité<span class="text-danger">*</span></label>
                                                        <input type="text" name="nationalite" class="form-control" value="{{$etu->nationalite}}" required >
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Contact 1<span class="text-danger">*</span></label>
                                                        <input type="text" name="contact1" class="form-control" required maxlength="12" value="{{$etu->contact1}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Contact 2</label>
                                                        <input type="text" name="contact2" class="form-control" maxlength="12" value="{{$etu->contact2}}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Date de naissance<span class="text-danger">*</span></label>
                                                        <input type="date" name="datenaiss" class="form-control" required value="{{$etu->datenaiss}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Lieu de naissance</label>
                                                        <input type="text" name="lieunaiss" class="form-control" value="{{$etu->lieunaiss}}">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Ville d'habitation<span class="text-danger">*</span></label>
                                                        <input type="text" name="ville" class="form-control" value="{{$etu->ville}}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="add-course-label" for="cv">{{($etu->cv != null) ? 'Remplacer le CV' : 'Ajouter le CV'}}</label>
                                                        @if ($etu->cv != null)
                                                            &nbsp; <a target="_blank" href="{{asset('assets/userCv/'.$etu->cv)}}" class="btn btn-warning mb-2"> Télécharger le CV actuel</a>
                                                        @endif
                                                        {{-- <input type="file" id="fileCv" name="fileCv" class="dropify" data-default-file="{{ $etu->cv ? asset('assets/userCv/'.$etu->cv) : '' }}"/> --}}
                                                        <input class="form-control form-control-solid form-control-lg" name="cvfile" id="cv" type="file" />
                                                        <small class="text-muted" for="cv">Formats autorisés : docx, doc, pdf, jpg, jpeg, png</small>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Niveau d'étude<span class="text-danger">*</span></label>
                                                        <select class="form-select form-control" name="niveau" required>
                                                            <option value="Aucun" {{$user->niveau=='Aucun' ? 'selected' : '' }}>Aucun</option>
                                                            <option value="CEPE" {{$user->niveau=='CEPE' ? 'selected' : '' }}>CEPE</option>
                                                            <option value="BEPC" {{$user->niveau=='BEPC' ? 'selected' : '' }}>BEPC</option>
                                                            <option value="BAC" {{$user->niveau=='BAC' ? 'selected' : '' }}>BAC</option>
                                                            <option value="BTS" {{$user->niveau=='BTS' ? 'selected' : '' }}>BTS</option>
                                                            <option value="LICENCE 1" {{$user->niveau=='LICENCE 1' ? 'selected' : '' }}>LICENCE 1</option>
                                                            <option value="LICENCE 2" {{$user->niveau=='LICENCE 2' ? 'selected' : '' }}>LICENCE 2</option>
                                                            <option value="LICENCE 3" {{$user->niveau=='LICENCE 3' ? 'selected' : '' }}>LICENCE 3</option>
                                                            <option value="MASTER 1" {{$user->niveau=='MASTER 1' ? 'selected' : '' }}>MASTER 1</option>
                                                            <option value="MASTER 2" {{$user->niveau=='MASTER 2' ? 'selected' : '' }}>MASTER 2</option>
                                                            <option value="INGENIEUR DES TECHNIQUES" {{$user->niveau=='INGENIEUR DES TECHNIQUES' ? 'selected' : '' }}>INGENIEUR DES TECHNIQUES</option>
                                                            <option value="INGENIEUR DE CONCEPTION" {{$user->niveau=='INGENIEUR DE CONCEPTION' ? 'selected' : '' }}>INGENIEUR DE CONCEPTION</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Formation</label>
                                                        <input type="text" name="formation" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Etablissement fréquenté</label>
                                                        <input type="text" name="contact1" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Nationalité<span class="text-danger">*</span></label>
                                                        <input type="text" name="nationalite" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Contact 1<span class="text-danger">*</span></label>
                                                        <input type="text" name="contact1" class="form-control" required maxlength="12">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Contact 2</label>
                                                        <input type="text" name="contact2" class="form-control" maxlength="12">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Date de naissance<span class="text-danger">*</span></label>
                                                        <input type="date" name="datenaiss" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Lieu de naissance</label>
                                                        <input type="text" name="lieunaiss" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Ville d'habitation<span class="text-danger">*</span></label>
                                                        <input type="text" name="ville" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-lg-12" style="margin-top: 20px;">
                                            <div class="form-group">
                                                <div class="profile-share">
                                                    <button type="submit" class="btn btn-danger">Modifier</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div class="tab-pane" id="profile-2">
                                <form class="col-md-12" action="{{route('etudiants.updatepass')}}" method="post">
                                    <div class="row">
                                        @csrf
                                        <input type="hidden" name="etu" value="{{encrypt(($user->id))}}">
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label class="form-control-label">Nouveau mot de passe</label>
                                                <input type="password" name="newpass" required="required" class="form-control" placeholder="Nouveau mot de passe">
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label class="form-control-label">Confirmer le mot de passe</label>
                                                <input type="password" name="confirmpass" required="required" class="form-control" placeholder="Confirmer mot de passe">
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label class="form-control-label">&nbsp;</label>
                                                <button type="submit" class="form-control btn btn-danger">Modifier</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="messages-2">
                                <form class="col-md-12" enctype="multipart/form-data" action="{{route('etudiants.updateavatar')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="etu" value="{{encrypt(($user->id))}}">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input type="file" id="fileUser" name="fileUser" class="dropify" data-default-file="{{ $user->img  ? asset('assets/userAvatar/'.$user->img.'') : asset('assets/images/users/user.jpeg') }}"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="profile-share">
                                                <button type="submit" class="btn btn-danger btn-lg">Modifier</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>

                        </div>
                    </div>

                </div>
                 <!-- end row -->
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
