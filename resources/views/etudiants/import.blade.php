@extends('layouts.app')

@section('title')
    Importer une liste d'étudiants
    @parent
@endsection

@section('header_styles')


@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            //prevent form submit
            $('#form-import').submit(function(e) {
                $('#loading').html('<div class="text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i> <span> Chargement...</span> </div>').delay(1000);
            });
        });
    </script>
@endsection


@section('content')
    <div class="content-page">
        <div class="content">
            <div class="container">

                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="page-title">Importer une liste d'étudiants<a href="{{route('etudiants.modeleimport')}}" class="ml-4 btn btn-light btn-danger btn-sm waves-effect waves-light btn-rounded-" style="margin-left: 14px;">
                            <i class="fa fa-download"></i> Télécharger le modèle
                        </a></h4>
                        <ol class="breadcrumb"> </ol>
                    </div>
                </div>

                <div class="row">
                    <form class="form-horizontal" method="post" id="form-import" action="{{route('etudiants.storeimport')}}" role="form" enctype="multipart/form-data">
                        @csrf
                        <div class="col-md-12">
                            <div class="card-box">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="col-md-12" for="nom">Sélectionner le fichier Excel (.xlsx, .xls, .csv)</label>
                                            <input type="file" name="importfile" id="importfile" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="padding-left:34px;">
                                        <div class="form-group">
                                            <label class="col-md-12" for="nom">&nbsp;</label>
                                            <button type="submit" id="btn-send" class="btn btn-primary btn-md waves-effect waves-light"><i class="fa fa-check-square"></i> Valider </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center" id="loading"> </div>
                </div>
            </div> <!-- container -->
        </div> <!-- content -->
    </div>
@endsection
