<?php

use App\Login_activity;
use App\User;
use Illuminate\Support\Carbon;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


//quicklogout route
Route::get('/quicklogout', function () {
    if(Auth::user() == null) return redirect(route('login'));
    $user = User::findOrFail(Auth::user()->id);
    $user->update([
        'last_logout_at' => Carbon::now()->toDateTimeString(),
    ]);
    // update user login activity
    $activity_id = Login_activity::where('user_id', '=', Auth::user()->id)->max('id');
    $activity = Login_activity::find($activity_id);
    if($activity != null) {
        $activity->update([
            'logout_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
    Auth::logout();
    return redirect(route('web'));
})->name('quicklogout');
Route::get('/forbiden', function(){ return view('forbiden')->with('error', "Interdiction d'accès, vous ne disposez pas des droits d'accès à cette page !"); })->name('forbiden');
Route::get('formations-list', 'WebController@formation_list')->name('w.formation-list');
Route::get('formations-grid', 'WebController@formation_grid')->name('w.formation-grid');


Route::prefix('ens')->namespace('Ens')->middleware(['auth', 'isEnseignant'])->group(function(){
    Route::get('/', 'IndexController@index')->name('ens.home');
    //PROFIL
    Route::get('profile', 'ProfilController@index')->name('ens.profiles');
    Route::post('profile/save', 'ProfilController@profilupdate')->name('ens.profiles.save');
    Route::post('profile/pass', 'ProfilController@profileditpass')->name('ens.profiles.savepass');
    Route::post('profile/avatar', 'ProfilController@profileditavatar')->name('ens.profiles.saveavatar');
    /* //FORMATIONS
    Route::get('formations', 'FormationController@index')->name('ens.formations');
    Route::get('formations/create', 'FormationController@create')->name('ens.formations.create');
    Route::post('formations/store', 'FormationController@store')->name('ens.formations.store');
    Route::get('formations/{id}/edit', 'FormationController@edit')->name('ens.formations.edit')->where('id', '[0-9]+');
    Route::post('formations/{id}/update', 'FormationController@update')->name('ens.formations.update')->where('id', '[0-9]+');
    Route::get('formations/{id}/delete', 'FormationController@delete')->name('ens.formations.delete')->where('id', '[0-9]+');
    Route::get('formations/{id}/retablir', 'FormationController@retablir')->name('ens.formations.retablir')->where('id', '[0-9]+');
    //ETUDIANTS
    Route::get('etudiants', 'EtudiantController@index')->name('ens.etudiants');
    Route::get('etudiants/{id}/show', 'EtudiantController@show')->name('ens.etudiants.show')->where('id', '[0-9]+');
    Route::get('etudiants/{id}/delete', 'EtudiantController@delete')->name('ens.etudiants.delete')->where('id', '[0-9]+');
    Route::get('etudiants/{id}/retablir', 'EtudiantController@retablir')->name('ens.etudiants.retablir')->where('id', '[0-9]+'); */
    //COURSES
});


Route::prefix('etu')->namespace('Etu')->middleware(['auth', 'isEtudiant'])->group(function(){
    Route::get('/', 'IndexController@index')->name('etu.home');
    Route::get('profile/notifications', 'IndexController@notif')->name('etu.notifications');
    //PROFIL
    Route::get('profile', 'ProfilController@index')->name('etu.profiles');
    Route::post('profile/save', 'ProfilController@profilupdate')->name('etu.profiles.save');
    Route::post('profile/pass', 'ProfilController@profileditpass')->name('etu.profiles.savepass');
    Route::post('profile/avatar', 'ProfilController@profileditavatar')->name('etu.profiles.saveavatar');

    //FORMATIONS
    Route::get('formations', 'FormationController@index')->name('etu.formations');
    Route::get('formations/show/{id}', 'FormationController@show')->name('etu.formations.show');
    Route::get('formations/savedemande/{id}', 'FormationController@savedemande')->name('etu.formations.savedemande');
    Route::get('formations/favoris/', 'FormationController@favoris')->name('etu.formations.favoris');
    Route::get('formations/favoris/show/{id}', 'FormationController@favoris_show')->name('etu.formations.favoris.show');
    Route::get('formations/savefavoris/{id}', 'FormationController@savefavoris')->name('etu.formations.savefavoris');
});
