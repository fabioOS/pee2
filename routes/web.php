<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('web/index');
//});

use Illuminate\Support\Facades\Storage;

Route::get('/', 'WebController@index')->name('web');
Route::get('recherche', 'WebController@search')->name('w.search');
Route::get('apropos', 'WebController@about')->name('w.about');
Route::get('formations', 'WebController@formation')->name('w.formation');
Route::get('formations/{slug}', 'WebController@formation_show')->name('w.formation.show');
Route::get('formations_search', 'WebController@formation_search')->name('w.formation.search');
Route::get('offre', 'WebController@offre')->name('w.offre');
Route::get('offre_search', 'WebController@offre_search')->name('w.offre.search');
Route::get('offre/{type}', 'WebController@offreType')->name('w.offre.type');
Route::get('offre_item/{slug}', 'WebController@offreItem')->name('w.offreItem');
Route::get('realisation', 'WebController@promo')->name('w.promo');
Route::get('realisation/{slug}', 'WebController@promoItem')->name('w.promo.item');
Route::get('news', 'WebController@news')->name('w.news');
Route::get('news/{slug}', 'WebController@newsItem')->name('w.newsItem');
Route::get('news/categorie/{slug}', 'WebController@newsCat')->name('w.newsCat');
Route::get('contact', 'WebController@contact')->name('w.contact');
Route::post('contact', 'WebController@contact_send')->name('w.contact.send');
Route::get('demande/{type}', 'WebController@demande')->name('w.demande');
Route::post('demande/send', 'WebController@demande_send')->name('w.demande.send');
Route::get('terme-et-condition', 'WebController@terme')->name('w.terme');
Route::get('politique-de-confidentialite', 'WebController@politique')->name('w.politique');
Route::post('creer-un-compte-eudiant', 'WebController@store_user_etud')->name('w.store_user_etud');


Auth::routes();

Route::prefix('guest')->middleware(['auth', 'isAdmin'])->group(function(){ //->namespace('Guest')
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');

    //MESSAGE
    Route::get('/messages', 'CommController@index')->name('message');
    Route::get('/messages/create', 'CommController@create')->name('message.create');
    Route::get('/messages/{slug}/read', 'CommController@messageRead')->name('message.read');
    Route::post('/messages', 'CommController@store')->name('message.send');

    //NEWS
    Route::get('actualites', 'NewsController@index')->name('news');
    Route::get('actualites/create', 'NewsController@create')->name('news.create');
    Route::get('actualites/{id}/edit', 'NewsController@edit')->name('news.edit')->where('id', '[0-9]+');
    Route::get('actualites/{id}/retablir', 'NewsController@retablir')->name('news.retablir')->where('id', '[0-9]+');
    Route::get('actualites/type/{slug}', 'NewsController@showtype')->name('news.show_type')->where('slug', '[A-Za-z]+');
    Route::get('actualites/delete/{id}', 'NewsController@delet')->name('news.delete')->where('id', '[0-9]+');
    Route::get('actualites/deletes', 'NewsController@allDelet')->name('news.deletes');
    Route::get('actualites/brouillons', 'NewsController@allBrouillons')->name('news.brouillons');
    Route::get('actualites/publies', 'NewsController@allPublies')->name('news.publies');
    Route::get('actualites/retablirs', 'NewsController@allRetablirs')->name('news.retablirs');
    Route::get('actualites/{id}/apercu', 'NewsController@apercu')->name('news.apercu')->where('id', '[0-9]+');
    Route::get('actualites/{slug}', 'NewsController@show')->name('news.show');

    Route::post('actualites/store', 'NewsController@store')->name('news.store');
    Route::post('actualites/update', 'NewsController@update')->name('news.update');
    Route::post('actualites/preview/store', 'NewsController@previewStore')->name('news.preview_store');

    //PROMOS
    Route::get('promotions', 'PromosController@index')->name('promo');
    Route::get('promotions/create', 'PromosController@create')->name('promo.create');
    Route::get('promotions/{id}/edit', 'PromosController@edit')->name('promo.edit')->where('id', '[0-9]+');
    Route::get('promotions/delete/{id}', 'PromosController@delet')->name('promo.delete')->where('id', '[0-9]+');

    Route::post('promotions/store', 'PromosController@store')->name('promo.store');
    Route::post('promotions/update', 'PromosController@update')->name('promo.update');

    //CATEGORIE
    Route::get('categories', 'CategoriesController@index')->name('categories');
    Route::get('categories/{id}/edit', 'CategoriesController@edit')->name('categories.edit')->where('id', '[0-9]+');
    Route::get('categories/delete/{id}', 'CategoriesController@delet')->name('categories.delete')->where('id', '[0-9]+');
    Route::get('categories/deletes', 'CategoriesController@allDelet')->name('categories.deletes');

    Route::post('categories/store', 'CategoriesController@store')->name('categories.store');
    Route::post('categories/update', 'CategoriesController@update')->name('categories.update');

    //USERS
    Route::get('utilisateurs', 'UsersController@index')->name('users');
    Route::get('utilisateurs/create', 'UsersController@create')->name('users.create');
    Route::get('utilisateurs/{id}/edit', 'UsersController@edit')->name('users.edit')->where('id', '[0-9]+');
    Route::get('utilisateurs/delete/{id}', 'UsersController@delet')->name('users.delete')->where('id', '[0-9]+');
    Route::get('utilisateurs/deletes', 'UsersController@allDelet')->name('users.deletes');

    Route::post('utilisateurs/store', 'UsersController@store')->name('users.store');
    Route::post('utilisateurs/update', 'UsersController@update')->name('users.update');

    //ROLE
    Route::get('roles', 'RolesController@index')->name('roles');
    Route::get('roles/{id}/edit', 'RolesController@edit')->name('roles.edit')->where('id', '[0-9]+');
    Route::get('roles/delete/{id}', 'RolesController@delet')->name('roles.delete')->where('id', '[0-9]+');
    Route::get('roles/deletes', 'RolesController@allDelet')->name('roles.deletes');

    Route::post('roles/store', 'RolesController@store')->name('roles.store');
    Route::post('roles/update', 'RolesController@update')->name('roles.update');

    //PROFIL
    Route::get('profile', 'ProfilesController@index')->name('profiles');
    Route::post('profile/save', 'ProfilesController@profilupdate')->name('profiles.save');
    Route::post('profile/pass', 'ProfilesController@profileditpass')->name('profiles.savepass');
    Route::post('profile/avatar', 'ProfilesController@profileditavatar')->name('profiles.saveavatar');

    //NOS PARTENAIRES
    Route::get('partenaires', 'PartenairesController@index')->name('partnaire');
    Route::get('partenaires/edit/{sluf}', 'PartenairesController@edit')->name('partnaire.edit');
    Route::get('partenaires/del/{sluf}', 'PartenairesController@delet')->name('partnaire.delet');
    Route::post('partenaires/store', 'PartenairesController@store')->name('partnaire.store');
    Route::post('partenaires/update', 'PartenairesController@update')->name('partnaire.update');

    //TEMOIGNAGE
    Route::get('testimonials', 'TestimonialsController@listTestimonial')->name('testimonials');
    Route::get('testimonials/create', 'TestimonialsController@createTestimonial')->name('testimonials.create');
    Route::get('testimonials/{id}/edit', 'TestimonialsController@editTestimonial')->name('testimonials.edit');
    Route::get('testimonials/{id}/delete', 'TestimonialsController@deleteTestimonial')->name('testimonials.delete');
    Route::post('testimonials/store', 'TestimonialsController@storeTestimonial')->name('testimonials.store');
    Route::post('testimonials/update', 'TestimonialsController@updateTestimonial')->name('testimonials.update');

    //OFFRES
    Route::get('offres', 'OffresController@index')->name('offres');
    Route::get('offres/create', 'OffresController@create')->name('offres.create');
    Route::get('offres/{id}/edit', 'OffresController@edit')->name('offres.edit');
    Route::get('offres/{id}/delete', 'OffresController@delet')->name('offres.delete');
    Route::post('offres/store', 'OffresController@store')->name('offres.store');
    Route::post('offres/update', 'OffresController@update')->name('offres.update');

        Route::get('offres/categories', 'OffresController@indexCats')->name('offres.cats');
        Route::get('offres/categories/{id}/edit', 'OffresController@editCats')->name('offres.cats.edit')->where('id', '[0-9]+');
        Route::get('offres/categories/delete/{id}', 'OffresController@deletCats')->name('offres.cats.delete')->where('id', '[0-9]+');

        Route::post('offres/categories/store', 'OffresController@storeCats')->name('offres.cats.store');
        Route::post('offres/categories/update', 'OffresController@updateCats')->name('offres.cats.update');

    //MEDIATHEQUE

        //ALBUM PHOTO
        Route::get('albums', 'MediasController@album')->name('albums');
        Route::get('albums/delete/{id}', 'MediasController@deletAlbum')->name('albums.delete')->where('id', '[0-9]+');
        Route::get('albums/{id}/edit', 'MediasController@editAlbum')->name('albums.edit')->where('id', '[0-9]+');
        Route::get('albums/deletes', 'MediasController@allDeletAlbum')->name('albums.deletes');

        Route::post('albums/store', 'MediasController@storeAlbum')->name('albums.store');
        Route::post('albums/update', 'MediasController@updateAlbum')->name('albums.update');

        //PHOTO
        Route::get('photos/{id}', 'MediasController@photo')->name('photos')->where('id', '[0-9]+');
        Route::get('photos/{id}/{type}', 'MediasController@updatePhoto')->name('photos.update')->where(['id'=>'[0-9]+','type' => '[a-z]+']);
        Route::get('photos/delete/{id}', 'MediasController@deletPhoto')->name('photos.delete')->where('id', '[0-9]+');
        Route::get('photos/deletes', 'MediasController@allDeletPhoto')->name('photos.deletes');
        Route::get('photos/active', 'MediasController@allActPhoto')->name('photos.actives');
        Route::get('photos/inactive', 'MediasController@allInactPhoto')->name('photos.inactives');

        Route::post('photos/store', 'MediasController@storePhoto')->name('photos.store');

        //VIDEO
        Route::get('videos', 'MediasController@videos')->name('videos');
        Route::get('videos/delete/{id}', 'MediasController@deletVideo')->name('videos.delete')->where('id', '[0-9]+');
        Route::get('videos/deletes', 'MediasController@allDeletVideo')->name('videos.deletes');

        Route::post('videos/store', 'MediasController@storeVideo')->name('videos.store');

        //SECTION SORO
        //SECTION SORO
        //SECTION SORO
        //SECTION SORO
        //SECTION SORO

        // formations
        Route::get('formations', 'FormationsController@index')->name('formations');
        Route::get('formations/search', 'FormationsController@search')->name('formations.search');
        Route::get('formations/create', 'FormationsController@create')->name('formations.create');
        Route::post('formations/store', 'FormationsController@store')->name('formations.store');
        Route::get('formations/edit/{id}', 'FormationsController@edit')->name('formations.edit');
        Route::post('formations/update/{id}', 'FormationsController@update')->name('formations.update');
        Route::get('formations/online/{id}', 'FormationsController@online')->name('formations.online');
        Route::get('formations/delete/{id}', 'FormationsController@delet')->name('formations.delete');
        Route::get('formations/deletes', 'FormationsController@allDelet')->name('formations.deletes');

            //CATEGORIE FORMATION
            Route::get('formations/categories', 'FormationsController@indexCats')->name('formations.cats');
            Route::get('formations/categories/{id}/edit', 'FormationsController@editCats')->name('formations.cats.edit')->where('id', '[0-9]+');
            Route::get('formations/categories/delete/{id}', 'FormationsController@deletCats')->name('formations.cats.delete')->where('id', '[0-9]+');

            Route::post('formations/categories/store', 'FormationsController@storeCats')->name('formations.cats.store');
            Route::post('formations/categories/update', 'FormationsController@updateCats')->name('formations.cats.update');

        // demande de formation
        Route::get('formations/demandes', 'FormationsController@demandes')->name('formations.demandes');
        Route::get('formations/demandes/show/{id}', 'FormationsController@showDemandes')->name('formations.demandes.show');
        Route::get('formations/demandes/edit/{id}', 'FormationsController@editDemandes')->name('formations.demandes.edit');
        //Route::post('formations/demandes/update', 'FormationsController@updateDemandes')->name('formations.demandes.update');
        Route::post('formations/demandes/confirm/{id}', 'FormationsController@confirmDemandes')->name('formations.demandes.confirm');
        Route::get('formations/demandes/delete/{id}', 'FormationsController@deleteDemandes')->name('formations.demandes.delete');
        Route::get('formations/demandes/deletes', 'FormationsController@multiDeleteDemandes')->name('formations.demandes.deletes');

        //etudiants
        Route::get('etudiants', 'EtudiantsController@index')->name('etudiants');
        Route::get('etudiants/show/{id}', 'EtudiantsController@show')->name('etudiants.show');
        Route::get('etudiants/create', 'EtudiantsController@create')->name('etudiants.create');
        Route::post('etudiants/store', 'EtudiantsController@store')->name('etudiants.store');
        Route::get('etudiants/import', 'EtudiantsController@import')->name('etudiants.import');
        Route::get('etudiants/modeleimport', function () { return Storage::download('public/Modele_Import_etudiant.xlsx'); })->name('etudiants.modeleimport');
        Route::post('etudiants/storeimport', 'EtudiantsController@storeimport')->name('etudiants.storeimport');
        Route::get('etudiants/{id}/edit', 'EtudiantsController@edit')->name('etudiants.edit');
        Route::post('etudiants/updateprofil', 'EtudiantsController@updateProfil')->name('etudiants.updateprofil');
        Route::post('etudiants/updatepass', 'EtudiantsController@updatePass')->name('etudiants.updatepass');
        Route::post('etudiants/updateavatar', 'EtudiantsController@updateAvatar')->name('etudiants.updateavatar');
        Route::get('etudiants/delete/{id}', 'EtudiantsController@delete')->name('etudiants.delete');
        Route::get('etudiants/deletes', 'EtudiantsController@allDelete')->name('etudiants.deletes');
        Route::get('etudiants/disable', 'EtudiantsController@disable')->name('etudiants.disable');
        Route::get('etudiants/enable/{id}', 'EtudiantsController@enable')->name('etudiants.enable');
        // etudiant indicateurs de performance
        Route::get('etudiants/indicateurs', 'EtudiantsController@indicateurs')->name('etudiants.indicateurs');
        Route::get('etudiants/{id}/indicateurs', 'EtudiantsController@indicateurs')->name('etudiants.getindicateurs');
        Route::post('etudiants/{id}/indicateurs/store', 'EtudiantsController@storeIndicateurs')->name('etudiants.indicateurs.store');
        Route::get('etudiants/{id}/indicateurs/{idInd}/edit', 'EtudiantsController@editIndicateurs')->name('etudiants.indicateurs.edit')->where(['id' => '[0-9]+','idInd' => '[0-9]+']);
        Route::post('etudiants/{id}/indicateurs/update', 'EtudiantsController@updateIndicateurs')->name('etudiants.indicateurs.update');
        Route::get('etudiants/{id}/indicateurs/{idInd}/delete', 'EtudiantsController@deletIndicateurs')->name('etudiants.indicateurs.delete')->where(['id' => '[0-9]+','idInd' => '[0-9]+']);
        //enseignants
        Route::get('enseignants', 'EnseignantsController@index')->name('enseignants');
        Route::get('enseignants/show/{id}', 'EnseignantsController@show')->name('enseignants.show');
        Route::get('enseignants/create', 'EnseignantsController@create')->name('enseignants.create');
        Route::post('enseignants/store', 'EnseignantsController@store')->name('enseignants.store');
        Route::get('enseignants/{id}/edit', 'EnseignantsController@edit')->name('enseignants.edit');
        Route::post('enseignants/update', 'EnseignantsController@update')->name('enseignants.update');
        Route::get('enseignants/delete/{id}', 'EnseignantsController@delete')->name('enseignants.delete');
        Route::get('enseignants/deletes', 'EnseignantsController@allDelete')->name('enseignants.deletes');
        Route::get('enseignants/disable', 'EnseignantsController@disable')->name('enseignants.disable');
        Route::get('enseignants/enable/{id}', 'EnseignantsController@enable')->name('enseignants.enable');
        //COURSES

});


/* ================== ROUTE SORO  ================== */
require __DIR__.'/web_soro.php';
