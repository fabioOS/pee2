<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles(){
        return $this->belongsTo('App\Role','role_id');
    }

    public function formations_ens(){
        return $this->hasMany('App\Formation','professeur_id');
    }
    public function demandes_etus(){
        return $this->hasMany('App\Formation_demande','etudiant_id');
    }

    public function countarticles(){
        return $this->articles()->count();
    }

    public function etudiant(){
        return $this->belongsTo('App\Etudiant','id');
    }

}
