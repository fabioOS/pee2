<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActuCategorie extends Model
{
    protected $table = 'actualites_categories';
    public $timestamps = false;

    public function categorie()
    {
        return $this->belongsTo('App\Categorie','cat_id');
    }

    public function actu()
    {
        return $this->belongsTo('App\Actu','actu_id');
    }
}
