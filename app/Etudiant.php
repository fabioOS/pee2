<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etudiant extends Model
{
    /*
    $table->string('civilite',20)->nullable();
    $table->string('niveau',20)->nullable();
    $table->text('formation')->nullable();
    $table->date('datenaiss')->nullable();
    $table->text('lieunaiss')->nullable();
    $table->string('nationalite',50)->nullable();
    $table->text('adresse')->nullable();
    $table->string('ville',100)->nullable();
    $table->string('contact1',20);
    $table->string('contact2',20)->nullable();
    $table->string('etablissement')->nullable();
     */
    protected $fillable = [
        'user_id',
        'civilite',
        'niveau',
        'formation',
        'datenaiss',
        'lieunaiss',
        'nationalite',
        'adresse',
        'ville',
        'contact1',
        'contact2',
        'etablissement',
        'cv',
        'type',
        'specialite'
    ];
    /**
     * relationship
     */
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
