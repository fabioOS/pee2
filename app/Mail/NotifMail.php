<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    /* public function __construct()
    {
        //
    } */
    public function __construct(Array $data)
    {
        $this->mail = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->from(['address' => $this->mail['email'], 'name' => $this->mail['name'] ])
            ->subject($this->mail['subject'])
            ->view($this->mail['view'])->with('data',$this->mail);
    }
}
