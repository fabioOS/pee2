<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formation_favori extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'etudiant_id',
        'formation_id',
    ];
    /**
     * relationship
     */
    public function etudiant() {
        return $this->belongsTo('App\User', 'etudiant_id');
    }
    public function formation() {
        return $this->belongsTo('App\Formation');
    }
}
