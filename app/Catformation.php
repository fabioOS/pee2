<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Catformation extends Model
{
    use SoftDeletes;
    protected $fillable = ['libelle', 'description', 'image', 'statut'];
    protected $dates = ['deleted_at'];
}
