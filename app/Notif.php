<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notif extends Model
{
    protected $fillable = [
        'user_id',
        'titre',
        'message',
        'lien',
        'lu',
    ];
    /**
     * relationship
     */
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
