<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OffreCats extends Model
{
    use SoftDeletes;
    protected $table="offres_categorie";

    public function listoffre()
    {
        return $this->hasMany('App\Offre','cat_id');
    }

    public function offre()
    {
        return $this->listoffre()->where('datelimite','>=',date('Y-m-d'));
    }
}
