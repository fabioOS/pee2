<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offre extends Model
{
    use SoftDeletes;

    public function categorie()
    {
        return $this->belongsTo('App\OffreCats','cat_id');
    }
}
