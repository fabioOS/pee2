<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cat_format extends Model
{
    use SoftDeletes;

    protected $fillable = ['catformation_id', 'formation_id'];
    //relation avec la table formation
    public function formation()
    {
        return $this->belongsTo('App\Formation');
    }
    //relation avec la table catformation
    public function catformation()
    {
        return $this->belongsTo('App\Catformation');
    }
}
