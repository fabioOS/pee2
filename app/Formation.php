<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Formation extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'titre',
        'description',
        //'categorie',
        'niveau',
        'prerequis',
        'lien',
        'salle',
        'prix',
        'prixpromo',
        'img',
        'motscles',
        'statut',
        'debut',
        'duree',
        'typedelai',
        'datecloture',
        'slug',
        'professeur_id',
        'user_id',
        'etp_id',
        'cible',
        'objectif',
        'competence',
    ];

    /**
     * function $dates */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'debut',
        'datecloture',
    ];
    /**
     * relationship
     */
    public function etp() {
        return $this->belongsTo('App\Etp');
    }
    public function professeur() {
        return $this->belongsTo('App\User', 'professeur_id');
    }
    public function user() {
        return $this->belongsTo('App\User');
    }
    public function criteres() {
        return $this->belongsToMany('App\Critere');
    }
    public function demandes() {
        return $this->hasMany('App\Formation_demande');
    }
    public function categories() {
        return $this->hasMany('App\Cat_format');
    }
}
