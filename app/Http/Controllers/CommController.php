<?php

namespace App\Http\Controllers;

use App\Message;
use App\MessageUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $messages = Message::with('itemdes')->orderBy('id','desc')->take(30)->get();
        return view('messages.index',compact('messages'));
    }

    public function create()
    {
        $allstudents = User::where('role_id',4)->get();
        $allprofs = User::where('role_id',3)->get();

        return view('messages.create',compact('allstudents','allprofs'));
    }

    public function SendMail($listusers,$subject,$message,$fichiers=[])
    {
        foreach ($listusers as $user){
            //Send les mails
            $email = $user->email;
            $subjt=$subject;
            $data=array(
                "content"=>$message,
            );
            try{
                if(count($fichiers)>0){
                    $lisfiles = $fichiers;
                    Mail::send('emails.mail', $data, function ($message) use ($email,$subjt,$lisfiles) {
                        $message->to($email);
                        $message->subject($subjt);
                        foreach ($lisfiles as $fichier){
                            $message->attach($fichier);
                        }
                    });
                }else{
                    Mail::send('emails.mail', $data, function ($message) use ($email,$subjt) {
                        $message->to($email);
                        $message->subject($subjt);
                    });
                }
            }catch (\Exception $e){
                \Log::error($e->getMessage());
            }
        }
    }


    public function store(Request $request)
    {
        ini_set('max_execution_time', -1);
        //dd($request->all());

        Validator::make($request->all(),[
            'objet' =>'required',
            'destination' =>'required',
            'description' =>'required',
        ])->validate();

        $destinataires = [];

        if(isset($request->destination) and count($request->destination)>0){
            if($request->destination[0]=='0'){
                //select tous etudiant et enseignant
                $role =[4,3];
                $users = User::whereIn('role_id',$role)->pluck('id')->toArray();
                $destinataires = $users;

            }

            if(in_array('00',$request->destination)){
                //select tous etudiants
                $role = [4];
                $users = User::whereIn('role_id',$role)->pluck('id')->toArray();
                $destinataires = $users;
            }

            if(in_array('000',$request->destination)){
                //select tous profs
                $role = [3];
                $users = User::whereIn('role_id',$role)->pluck('id')->toArray();
                $destinataires = array_merge($users,$destinataires);
            }

            $users = User::whereIn('id',$request->destination)->pluck('id')->toArray();
            $destinataires = array_merge($users,$destinataires);
        }else{
            $destinataires = [];
        }

        $destinataires = array_unique($destinataires ?: []);

        if(count($destinataires)==0){
            return redirect()->back()->with('error','Veuillez renseigne un destinataire');
        }

        $message = new Message();
        $message->from_id = Auth::user()->id;
        $message->objet = $request->objet;
        $message->content = $request->description;
        $message->save();

        foreach ($destinataires as $destinataire){
            $mess = new MessageUser();
            $mess->message_id = $message->id;
            $mess->to_id = $destinataire;
            $mess->from_id = Auth::user()->id;
            $mess->save();
        }

        //Send NOtification
        $users = User::whereIn('id',$destinataires)->get();
        $this->SendMail($users,$request->objet,$request->description);
        return redirect()->route('message')->with('success','Le message a bien été envoyé.');
    }
}
