<?php

namespace App\Http\Controllers;

use App\Formation;
use App\Formation_demande;
use App\Offre;
use App\Partenaire;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $dt=[];
        $dt['etu'] = User::where('role_id',4)->count();
        $dt['ens'] = User::where('role_id',3)->count();
        $dt['form'] = Formation::all()->count();
        $dt['part'] = Partenaire::all()->count();
        $dt['stag'] = Offre::where('cat_id',1)->count();
        $dt['empl'] = Offre::where('cat_id',2)->count();
        $dt['finan'] = Offre::where('cat_id',3)->count();
        $dt['demand'] = Formation_demande::where('statut',0)->count();
        return view('home', compact('dt'));
    }
}
