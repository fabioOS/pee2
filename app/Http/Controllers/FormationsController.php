<?php

namespace App\Http\Controllers;

use App\Cat_format;
use App\Catformation;
use App\Formation;
use App\Formation_demande;
use App\Mail\NotifMail;
use App\Notif;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class FormationsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    public function index(){
        //get formations grouped by statut
        $statuts = [
            0 => 'Brouillons',
            1 => 'En ligne',
        ];
        $data = [];
        foreach ($statuts as $key=>$statut) {
            $data[$statut] = Formation::where('statut',$key)->get();//->where('debut','>=',date('Y-m-d'))
        }
        $data['Terminées'] = Formation::where('debut','<',date('Y-m-d'))->get();
        $search='';
        return view('formations.index',compact('data', 'search'));
    }

    public function search(Request $request){
        if(request('search') == null) return redirect()->route('formations');
        //get formations by search
        $statuts = [
            0 => 'Brouillons',
            1 => 'En ligne',
        ];
        $data = [];
        foreach ($statuts as $key=>$statut) {
            $data[$statut] = Formation::where('statut',$key)->where(function($query) {
                $query->where('titre', 'like', '%'.request('search').'%')
                    ->orWhere('description', 'like', '%'.request('search').'%')
                    ->orWhere('motscles', 'like', '%'.request('search').'%')
                    ->orWhere('prerequis', 'like', '%'.request('search').'%');
            })->get();
        }
        $data['Terminées'] = Formation::where('debut','<',date('Y-m-d'))->where(function($query) {
            $query->where('titre', 'like', '%'.request('search').'%')
                ->orWhere('description', 'like', '%'.request('search').'%')
                ->orWhere('motscles', 'like', '%'.request('search').'%')
                ->orWhere('prerequis', 'like', '%'.request('search').'%');
        })->get();
        $search = request('search');
        return view('formations.index',compact('data', 'search'));
    }

    public function create(){
        return view('formations.create');
    }

    /**
     * store
     */
    public function store(Request $request){
        $request->validate([
            'titre' => 'required',
            'categories' => 'required',
            //'motscles' => 'required',
            'statut' => 'required',
            //'debut' => 'required',
            'professeur' => 'required',
            'cible' => 'required',
        ]);
        $formation = new Formation();
        $formation->titre = $request->titre;
        $formation->description = $request->description;
        $formation->prerequis = $request->prerequis;
        $formation->lien = $request->lien;
        $formation->salle = $request->salle;
        $formation->prix = ($request->prix != '') ? str_replace(' ','',$request->prix) : 0;
        //$formation->prixpromo = $request->prixpromo;
        $formation->motscles = $request->motscles;
        $formation->statut = $request->statut;
        $formation->debut = $request->debut;
        $formation->duree = $request->duree;
        $formation->typedelai = $request->typedelai;
        $formation->datecloture = Carbon::createFromFormat('d/m/Y', $request->datecloture)->format('Y-m-d');
        $formation->slug = Str::slug($request->titre);
        $formation->professeur_id = $request->professeur;
        $formation->user_id = Auth::user()->id;
        $formation->etp_id = $request->etp_id;

        $formation->cible = $request->cible;
        $formation->objectif = $request->objectif;
        $formation->competence = $request->competence;

        $formation->img = '';
        $file = $request->file('file');
        if($file != null) {
            $folderName = 'storage/app/public/formations/';
            if(!is_dir($folderName)){
                mkdir($folderName, 0777, true);
            }
            $old_name = $file->getClientOriginalName();
            //stop if type id not in allowed array list
            $allowed = array('jpg', 'jpeg', 'png', 'gif','tiff');
            $ext = $file->getClientOriginalExtension();
            if(!in_array($ext, $allowed)){
                $message = "Le format du fichier join n'est pas pris en charge !";
                return back()->with('error', $message);
            }
            //stop if file size it's greater than 5MB : 5242880
            if($file->getSize() > 10485760){
                $message = "La taille du fichier join dépace la limite de 10M !";
                return back()->with('error', $message);
            }
            $new_name  = $formation->slug.'_'.date('Ymd_His').'.'.$ext;
            $file->move($folderName,$new_name);
            $formation->img = $folderName.'/'.$new_name;
        }
        $formation->save();
        //categories
        if(count($request->categories)>0) {
            foreach ($request->categories as $key => $value) {
                Cat_format::create([
                    'formation_id' => $formation->id,
                    'catformation_id' => $value,
                ]);
            }
        }
        //send notif to all etudiant (user role = 4)
        $users = User::where('role_id',4)->get();
        foreach ($users as $key => $user) {
            Notif::create([
                'user_id' => $user->id,
                'titre' => 'Nouvelle formation',
                'message' => 'Une nouvelle formation a été ajoutée. Voir les détails.',
                'lien' => route('w.formation.show', $formation->slug),
            ]);
        }
        // send email to all etudiant (user role = 4)
        foreach ($users as $key => $user) {
            $data = [
                'name' => env('APP_NAME'),
                'email' => env('MAIL_FROM_ADDRESS'),
                'subject' => 'Nouvelle formation | '.env('APP_NAME'),
                'message' => 'Une nouvelle formation a été ajoutée. Voir les détails.',
                'view'  => 'emails.newformation',
                'data'  => [$formation],
                'link' => route('w.formation.show', $formation->slug),
                'attachment' => ''
            ];
            Mail::to($user->email)->send(new NotifMail($data));
        }
        return redirect()->route('formations')->with('success','Formation ajoutée avec succès');
    }


    public function edit($id){
        $id = decrypt($id);
        $item = Formation::findOrFail($id);
        //$catlistes = Cat_format::selectRaw('catformation_id AS cat')->where('formation_id', $id)->pluck('cat');
        //dd($formation,$catlistes);
        return view('formations.edit', compact('item'));
    }

    /**
     * update
     */
    public function update(Request $request, $id) {
        $id = decrypt($id);
        $request->validate([
            'titre' => 'required',
            'categories' => 'required',
            //'motscles' => 'required',
            'statut' => 'required',
            //'debut' => 'required',
            'professeur' => 'required',
            'cible' => 'required',
        ]);
        $formation = Formation::findOrFail($id);
        $formation->titre = $request->titre;
        $formation->description = $request->description;
        $formation->prerequis = $request->prerequis;
        $formation->lien = $request->lien;
        $formation->salle = $request->salle;
        $formation->prix = ($request->prix != '') ? str_replace(' ','',$request->prix) : 0;
        //$formation->prixpromo = $request->prixpromo;
        $formation->motscles = $request->motscles;
        $formation->statut = $request->statut;
        $formation->debut = $request->debut;
        $formation->duree = $request->duree;
        $formation->typedelai = $request->typedelai;
        $formation->datecloture = ($request->datecloture!='')?Carbon::createFromFormat('d/m/Y', $request->datecloture)->format('Y-m-d'):null;
        $formation->slug = Str::slug($request->titre);
        $formation->professeur_id = $request->professeur;
        $formation->user_id = Auth::user()->id;
        $formation->etp_id = $request->etp_id;

        $formation->cible = $request->cible;
        $formation->objectif = $request->objectif;
        $formation->competence = $request->competence;

        $formation->img = '';
        $file = $request->file('file');
        if($file != null) {
            $folderName = 'storage/app/public/formations/';
            if(!is_dir($folderName)){
                mkdir($folderName, 0777, true);
            }
            $old_name = $file->getClientOriginalName();
            //stop if type id not in allowed array list
            $allowed = array('jpg', 'jpeg', 'png', 'gif','tiff');
            $ext = $file->getClientOriginalExtension();
            if(!in_array($ext, $allowed)){
                $message = "Le format du fichier join n'est pas pris en charge !";
                return back()->with('error', $message);
            }
            //stop if file size it's greater than 5MB : 5242880
            if($file->getSize() > 10485760){
                $message = "La taille du fichier join dépace la limite de 10M !";
                return back()->with('error', $message);
            }
            $new_name  = $formation->slug.'_'.date('Ymd_His').'.'.$ext;
            $file->move($folderName,$new_name);
            $formation->img = $folderName.'/'.$new_name;
        }
        $formation->save();
        //categories
        if(count($request->categories)>0) {
            //delete old categories
            Cat_format::where('formation_id', $formation->id)->delete();
            foreach ($request->categories as $key => $value) {
                Cat_format::create([
                    'formation_id' => $formation->id,
                    'catformation_id' => $value,
                ]);
            }
        }
        $message = 'Formation ajoutée avec succès !';
        //send notif to all etudiant (user role = 4)
        $users = User::where('role_id',4)->whereIn('id', Formation_demande::where('formation_id', $formation->id)->pluck('etudiant_id'))->get();
        if(count($users)>0) {
            foreach ($users as $key => $user) {
                Notif::create([
                    'user_id' => $user->id,
                    'titre' => 'Formation mise à jour',
                    'message' => 'Une formation a été mise à jour. Voir les détails.',
                    'lien' => route('w.formation.show', $formation->slug),
                ]);
            }
            // send email to all etudiant (user role = 4)
            foreach ($users as $key => $user) {
                $data = [
                    'name' => env('APP_NAME'),
                    'email' => env('MAIL_FROM_ADDRESS'),
                    'subject' => 'Formation mise à jour | '.env('APP_NAME'),
                    'message' => 'Une formation a été mise à jour. Voir les détails.',
                    'view'  => 'emails.newformation',
                    'data'  => [$formation],
                    'link' => route('w.formation.show', $formation->slug),
                    'attachment' => ''
                ];
                Mail::to($user->email)->send(new NotifMail($data));
            }
            $message.= ' Et les étudiants concernés ont été notifiés.';
        }
        return redirect()->route('formations')->with('success',$message);
    }

    public function online($id){
        try {
            $id = decrypt($id);
            $formation = Formation::findOrFail($id);
            $formation->statut = 1;
            $formation->save();
            return redirect()->route('formations')->with('success','Formation mise en ligne avec succès');
        }
        catch (\Exception $e) {
            return redirect()->route('formations')->with('error','Erreur lors de la mise en ligne de la formation');
        }

    }

    public function delet($id){
        try {
            $id = decrypt($id);
            $formation = Formation::findOrFail($id);
            $formation->delete();
            return redirect()->route('formations')->with('success','Formation supprimée avec succès');
        }
        catch (\Exception $e) {
            return redirect()->route('formations')->with('error','Erreur lors de la suppression de la formation');
        }
    }

    /**
     * demandes de formation
     */
    public function demandes(Request $request){
        $demandF = Notif::where('user_id',Auth::user()->id)->where('lu',0)->get();
        if(count($demandF)>0){
            foreach ($demandF as $key => $value) {
                $value->lu = 1;
                $value->save();
            }
        }
        $demandes = Formation_demande::all();
        $statuts = [
            0 => 'En attente',
            1 => 'Validée',
            2 => 'Refusée',
        ];
        $search=(isset($request->search))?$request->search:'';
        $data = [];
        $total=0;
        foreach ($statuts as $key=>$statut) {
            if($search != null) {
                $re = Formation_demande::where('formation_demandes.statut',$key)->join('etudiants', 'etudiants.id', '=', 'formation_demandes.etudiant_id')
                ->join('users', 'users.id', '=', 'etudiants.user_id')
                ->join('formations', 'formations.id', '=', 'formation_demandes.formation_id')
                ->where(function($query) use ($search) {
                    $query->where('name', 'like', '%'.$search.'%')
                    ->orWhere('titre', 'like', '%'.$search.'%')
                    ->orWhere('description', 'like', '%'.$search.'%')
                    ->orWhere('prerequis', 'like', '%'.$search.'%')
                    ->orWhere('salle', 'like', '%'.$search.'%')
                    ->orWhere('prix', 'like', '%'.$search.'%')
                    ->orWhere('prixpromo', 'like', '%'.$search.'%')
                    ->orWhere('motscles', 'like', '%'.$search.'%')
                    ->orWhere('formations.niveau', 'like', '%'.$search.'%')

                    ->orWhere('email', 'like', '%'.$search.'%')
                    ->orWhere('civilite', 'like', '%'.$search.'%')
                    ->orWhere('formation', 'like', '%'.$search.'%')
                    ->orWhere('datenaiss', 'like', '%'.$search.'%')
                    ->orWhere('lieunaiss', 'like', '%'.$search.'%')
                    ->orWhere('nationalite', 'like', '%'.$search.'%')
                    ->orWhere('adresse', 'like', '%'.$search.'%')
                    ->orWhere('ville', 'like', '%'.$search.'%')
                    ->orWhere('contact1', 'like', '%'.$search.'%')
                    ->orWhere('contact2', 'like', '%'.$search.'%')
                    ->orWhere('etablissement', 'like', '%'.$search.'%');
                })->pluck('formation_demandes.id')->toArray();
                if(count($re)>0) {
                    $data[$statut] = Formation_demande::where('statut',$key)->whereIn('id',$re)->get();
                    $total += count($data[$statut]);
                }
                else
                    $data[$statut] = [];
            }
            else {
                $data[$statut] = Formation_demande::where('statut',$key)->get();
                $total += count($data[$statut]);
            }
        }
        return view('formations.demandes',compact('data', 'search', 'total'));
    }
    /**
     * show demande de formation
     */
    public function showDemandes($id){
        $id = decrypt($id);
        $item = Formation_demande::findOrFail($id);
        return view('formations.demandes_show',compact('item'));
    }
     /**
     * edit demandes de formation
     */
    public function editDemandes($id){
        $demande = Formation_demande::findOrFail($id);
        return view('formations.editdemande',compact('demande'));
    }
    /**
     * confirmDemandes demandes de formation
     */
    public function confirmDemandes(Request $request, $id){
        $id = decrypt($id);
        $demande = Formation_demande::findOrFail($id);
        $demande->statut = $request->statut;
        $demande->save();
        if($request->statut == 1) {
            $message = 'La demande de formation a été acceptée.';
            $titre = 'Demande de formation acceptée';
            $notif_message = 'Votre demande de formation a été acceptée. Voir les détails.';
            $mss = 'Votre demande de formation a été acceptée. Veuillez vous connectez pour les détails.';
        }
        else {
            $message = 'La demande de formation a été refusée.';
            $titre = 'Demande de formation refusée';
            $notif_message = 'Votre demande de formation a été refusée. Voir les détails.';
            $mss = 'Votre demande de formation a été refusée. Veuillez vous connectez pour les détails.';
        }
        // user to notify
        $user = User::findOrFail($demande->etudiant_id);
        //send notif to user
        Notif::create([
            'user_id' => $user->id,
            'titre' => $titre,
            'message' => $notif_message,
            'lien' => route('etu.formations.show', $demande->formation->slug),
        ]);
        // send mail to admin
        $commentaire = $request->commentaire;
        $data = [
            'name' => env('APP_NAME'),
            'email' => env('MAIL_FROM_ADDRESS'),
            'subject' => $titre.' | '.env('APP_NAME'),
            'message' => $mss,
            'view'  => 'emails.traitdemande',
            'data'  => [$demande,$user,$commentaire],
            'link' => route('etu.formations.show', $demande->formation->slug),
            'attachment' => ''
        ];
        Mail::to($user->email)->send(new NotifMail($data));
        return redirect()->route('formations.demandes')->with('success',$message);
    }

    /**
     * delete une demande de formation
     */
    public function deleteDemandes($id){
        try {
            $formation = Formation_demande::findOrFail($id);
            $formation->delete();
            return redirect()->route('formations.demandes')->with('success','Demande supprimée avec succès');
        } catch (\Exception $e) {
            return redirect()->route('formations.demandes')->with('error','Erreur lors de la suppression de la demande');
        }
    }
    /**
     * delete multi demande de formation
     */
    public function multiDeleteDemandes(Request $request){
        try {
            $ids = $request->ids;
            Formation_demande::whereIn('id',explode(",",$ids))->delete();
            return response()->json(['status'=>true,'message'=>"Demandes supprimées avec succès."]);
        } catch (\Exception $e) {
            return response()->json(['status'=>false,'message'=>"Erreur lors de la suppression des demandes."]);
        }
    }



    // CATEGORIE FORMATION

    public function indexCats(){
        $cats = Catformation::all();
        return view('formations.categories.index',compact('cats'));
    }

    public function storeCats(Request $request){
        //dd($request->all());
        Validator::make($request->all(),[
            'nom' =>'required',
            'file'=>'required',
        ])->validated();


        $cat = new Catformation();
        $cat->libelle = $request->nom ;
        $slug= Str::slug($request->nom);
        $cat->image = '';

        if($request->hasFile('file')) {
            $file = $request->file('file');
            $folderName = 'storage/app/public/formations/cats/';
            if(!is_dir($folderName)){
                mkdir($folderName, 0777, true);
            }
            $allowed = array('jpg', 'jpeg', 'png', 'gif','tiff');
            $ext = $file->getClientOriginalExtension();
            if(!in_array($ext, $allowed)){
                $message = "Le format du fichier join n'est pas pris en charge !";
                return redirect()->back()->with('error', $message);
            }
            //stop if file size it's greater than 5MB : 5242880
            if($file->getSize() > 10485760){
                $message = "La taille du fichier join dépace la limite de 10M !";
                return redirect()->back()->with('error', $message);
            }
            $new_name  = $slug.'_'.date('Ymd_His').'.'.$ext;
            $file->move($folderName,$new_name);
            $cat->image = $folderName.'/'.$new_name;
        }

        $cat->save();

        return redirect()->route('formations.cats')->with('success','✔ Félicitation ! vous venez d\'ajouter une catégorie');
    }

    public function editCats($id){
        $cat = Catformation::find($id);
        return view('formations.categories.edit',compact('cat'));
    }

    public function updateCats(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required|min:6',
            'slug'=>'required'
        ])->validate();


        $cat = Catformation::where('id',$request->slug)->first();
        $cat->libelle = $request->nom ;
        $slug= Str::slug($request->nom);
        if($request->hasFile('fileTestimonial')) {
            $file = $request->file('fileTestimonial');
            $folderName = 'storage/app/public/formations/cats/';
            if(!is_dir($folderName)){
                mkdir($folderName, 0777, true);
            }
            $allowed = array('jpg', 'jpeg', 'png', 'gif','tiff');
            $ext = $file->getClientOriginalExtension();
            if(!in_array($ext, $allowed)){
                $message = "Le format du fichier join n'est pas pris en charge !";
                return redirect()->back()->with('error', $message);
            }
            //stop if file size it's greater than 5MB : 5242880
            if($file->getSize() > 10485760){
                $message = "La taille du fichier join dépace la limite de 10M !";
                return redirect()->back()->with('error', $message);
            }
            $new_name  = $slug.'_'.date('Ymd_His').'.'.$ext;
            $file->move($folderName,$new_name);
            $cat->image = $folderName.'/'.$new_name;
        }
        $cat->save();

        return redirect()->route('formations.cats')->with('success','Vous venez de modifier une catégorie');
    }

    public function deletCats($id){
        //dd($id);
        if($id){
            $cat = Catformation::findOrFail($id);
            $cat->delete();
            return redirect()->route('formations.cats')->with('success','Vous venez de supprimer une catégorie');
        }
        return redirect()->back();
    }

}
