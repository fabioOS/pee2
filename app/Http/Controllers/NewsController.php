<?php

namespace App\Http\Controllers;

use App\Actu;
use App\ActuCategorie;
use App\Categorie;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $actus = Actu::with('categories')->with('author')->orderBy('created_at','desc')->get();
        $countPublie = Actu::where('status','1')->count();
        $countBrouille = Actu::where('status','0')->count();
        $countDel = DB::table('actualites')->where('deleted_at','<>',null)->count();
        //dd($countDel);
        return view('news.index',compact('actus','countPublie','countBrouille','countDel'));
    }

    public function showtype($id){
        if($id=="publies"){
            $actus = Actu::with('categories')->where('status','1')->with('author')->orderBy('created_at','desc')->get();
            $countPublie = Actu::where('status','1')->count();
            $countBrouille = Actu::where('status','0')->count();
            $countDel = DB::table('actualites')->where('deleted_at','<>',null)->count();
            $countAllActus = Actu::count();
            return view('news.showtype',compact('actus','countPublie','countBrouille','countDel','countAllActus','id'));

        }elseif ($id=="corbeille"){
            //$actus = DB::table('actualites')->orderBy('created_at','desc')->where('deleted_at','<>',null)->get();
            $actus = Actu::with('categories')->withTrashed()->orderBy('created_at','desc')->where('deleted_at','<>',null)->get();
            $countPublie = Actu::where('status','1')->count();
            $countBrouille = Actu::where('status','0')->count();
            $countDel = DB::table('actualites')->where('deleted_at','<>',null)->count();
            $countAllActus = Actu::count();
            return view('news.showtype',compact('actus','countPublie','countBrouille','countDel','countAllActus','id'));

        }elseif ($id=="brouillons"){
            $actus = Actu::with('categories')->where('status','0')->with('author')->orderBy('created_at','desc')->get();
            $countPublie = Actu::where('status','1')->count();
            $countBrouille = Actu::where('status','0')->count();
            $countDel = DB::table('actualites')->where('deleted_at','<>',null)->count();
            $countAllActus = Actu::count();
            return view('news.showtype',compact('actus','countPublie','countBrouille','countDel','countAllActus','id'));

        }else{
            return redirect()->route('news');
        }
    }

    public function create(){
        $cats = Categorie::all();
        return view('news.create',compact('cats'));
    }

    public function edit($id){
        //dd('edit');
        if($id){
            $actu = Actu::with('categories')->find($id);
            $cats = Categorie::all();
            return view('news.edit',compact('cats','actu'));
        }

        return back();

    }

    public function delet($id){
        //dd($id);
        if($id){
            $actu = Actu::findOrFail($id);
            $actu->delete();
            return redirect()->route('news')->with('success','Vous venez de supprimer un article');
        }
        return redirect()->back();
    }

    public function allDelet(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        foreach ($ids as $id){
            $actu = Actu::findOrFail($id);
            $actu->delete();
        }
        echo $data = '1';
    }

    public function allBrouillons(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        foreach ($ids as $id){
            $actu = Actu::findOrFail($id);
            $actu->status= '0';
            $actu->save();
        }
        echo $data = '1';
    }

    public function allPublies(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        foreach ($ids as $id){
            $actu = Actu::findOrFail($id);
            $actu->status= '1';
            $actu->save();
        }
        echo $data = '1';
    }

    public function allRetablirs(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        foreach ($ids as $id){
            DB::table('actualites')->where('id',$id)->update(['deleted_at' => null]);
        }
        echo $data = '1';
    }

    public function apercu($id){
        //dd($id);
        $actu = Actu::where('id',$id)->first();
        return view("news.apercu",compact('actu'));
    }

    public function show($slug){
        //dd($slug);
        $actu = Actu::with('categories')->where('slug',$slug)->first();
        return view("news.show",compact('actu'));
    }

    public function retablir($id){
        if($id){
            DB::table('actualites')->where('id',$id)->update(['deleted_at' => null]);
            return redirect()->back()->with('success','Vous venez de rétablir un article');
        }
        return redirect()->back();
    }

    public function store(Request $request){
        //dd(json_encode($request->catid));
        if($request->type_article=='text'){
            Validator::make($request->all(),[
                'title' =>'required',
                'description' =>'required',
                'catid' =>'required',
                'fileUser' =>'required',
                'date' =>'required',
            ])->validate();
        }else{
            Validator::make($request->all(),[
                'title' =>'required',
                'description' =>'required',
                'catid' =>'required',
                'linkvideo' =>'required',
                'date' =>'required',
            ])->validate();
        }

        $actus = new Actu();
        $actus->slug= Str::slug($request->title);
        $actus->title = $request->title;
        $actus->type_article = $request->type_article;
        $actus->des = $request->description;
        $actus->auteur = $request->auteur;
        $actus->date = $request->date;
        //$actus->cat_id = json_encode($request->catid);

        if($request->type_article == 'text'){
            $file=$request->file('fileUser');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = 'articles/';
            $picture = Str::random(8).'.'. $extension;
            $file->move($folderName,$picture);
            $actus->img = $picture;
        }else{
            $actus->video = $request->linkvideo;
        }

        //SAVOIR LE TYPE DE BTN SUBMIT
        if($request->btn =='brouillon'){
            $actus->status = '0';
            $actus->save();
        }

        if($request->btn =='apercu'){
            $actus->status = '0';
            $actus->save();

            return redirect()->route('news.apercu',$actus);
        }

        if($request->btn =='publier'){
            $actus->status = '1';
            $actus->save();
        }

        foreach ($request->catid as $cat){
            $actCat = new ActuCategorie();
            $actCat->cat_id = $cat;
            $actCat->actu_id = $actus->id;
            $actCat->save();
        }

        return redirect()->route('news')->with('success','✔ Félicitation ! vous venez d\' ajoute un article');

    }

    public function update(Request $request){
        //dd($request->all());

        if($request->type_article=='text'){
            $valider = Validator::make($request->all(),[
                'title' =>'required',
                'description' =>'required',
                'catid' =>'required',
                'date' =>'required',
            ]);
        }else{
            $valider = Validator::make($request->all(),[
                'title' =>'required',
                'description' =>'required',
                'catid' =>'required',
                'linkvideo' =>'required',
                'date' =>'required',
            ]);
        }

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{

            $actus = Actu::where('slug',$request->idActu)->first();
            $actus->title = $request->title;
            $actus->type_article = $request->type_article;
            $actus->des = $request->description;
            $actus->auteur = $request->auteur;
            $actus->date = $request->date;
            //$actus->cat_id = json_encode($request->catid);

            if($request->type_article == 'text'){
                if($request->hasFile('fileUser')){
                    $file=$request->file('fileUser');
                    $extension = $file->getClientOriginalExtension() ?: 'png';
                    $folderName = 'articles/';
                    $picture = Str::random(8).'.'. $extension;

                    if (!empty($actus->img)) {
                        unlink($folderName.$actus->img);
                    }

                    $file->move($folderName,$picture);
                    $actus->img = $picture;
                }
            }else{
                if($request->linkvideo != null){
                    $actus->video = $request->linkvideo;
                }
            }

            $actus->status = '1';
            $actus->save();
            DB::table('actualites_categories')->where('actu_id',$actus->id)->delete();
            foreach ($request->catid as $cat){
                $actCat = new ActuCategorie();
                $actCat->cat_id = $cat;
                $actCat->actu_id = $actus->id;
                $actCat->save();
            }


            return redirect()->route('news')->with('success','✔ Félicitation ! vous venez de modifier un article');
        }

    }


    public function previewStore(Request $request){
        //dd($request->all());
        if($request->btn=='update'){
            //Lien de modification
            return redirect()->route('news.edit',$request->idactu);
        }else{
            $actu = Actu::find($request->idactu);
            $actu->status = '1';
            $actu->save();

            return redirect()->route('news')->with('success','✔ Félicitation ! vous venez d\' ajoute un article');
        }
    }


    public function getCatlibell($id){
        // $id = json_decode($id);
        $ids = ActuCategorie::where('actu_id',$id)->pluck('cat_id')->toArray();
        $cat = Categorie::whereIn('id',$ids)->select('libelle')->get();
        return $cat;

    }

    public function getAuthActu($id){
        $cat = User::find($id);
        //dd($cat->name);
        return $cat->name;

    }

}
