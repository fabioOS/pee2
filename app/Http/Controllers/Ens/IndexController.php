<?php

namespace App\Http\Controllers\Ens;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notif;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index(){
        return view('ens.index');
    }
    /**
     * Notifications
     */
    public function notif(){
        $notifies = Notif::where('user_id', Auth::user()->id)->where('lu',0)->get();
        return view('ens.notif', compact('notifies'));
    }
    /**
     * Faire les CRUD de formation
     */
}
