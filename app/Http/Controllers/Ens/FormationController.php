<?php

namespace App\Http\Controllers\Ens;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isEnseignant']);
    }

    public function index(){
        return view('ens.formations.index');
    }
}
