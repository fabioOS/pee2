<?php

namespace App\Http\Controllers;

use App\Offre;
use App\OffreCats;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class OffresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $offres = Offre::orderBy('id','desc')->get();
        return view('offres.index',compact('offres'));
    }

    public function create(){
        $cats = OffreCats::orderBy('libelle','asc')->get();
        return view('offres.create',compact('cats'));
    }

    public function codeOffre()
    {
        $count = Offre::withoutTrashed()->count();
        $count = $count ? $count + 1 : 1;
        $num=sprintf("%05d",$count);
        $val = $num;

        return $val;
    }

    public function store(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(),[
            'poste' =>'required',
            'niveau' =>'required',
            'experience' =>'required',
            'lieu' =>'required',
            'datepublie' =>'required',
            'datelimite' =>'required',
            'description' =>'required',
            'types' =>'required',
            'catid' =>'required',
        ])->validate();

        $offre = new Offre();
        $offre->slug = date('dis').Str::slug($request->poste);
        $offre->code = $this->codeOffre();
        $offre->poste = $request->poste;
        $offre->cat_id = $request->catid;
        $offre->type = $request->types;
        $offre->niveau = $request->niveau;
        $offre->experience = $request->experience;
        $offre->lieu = $request->lieu;
        $offre->datepublie = $request->datepublie;
        $offre->datelimite = $request->datelimite;
        $offre->description = $request->description;
        $offre->save();


        return redirect()->route('offres')->with('success','✔ Félicitation ! vous venez d\'ajouter une offre');

    }

    public function edit($id)
    {
        //dd($id);
        $offre = Offre::where('id',$id)->first();
        $cats = OffreCats::orderBy('libelle','asc')->get();
        return view('offres.edit',compact('cats','offre'));
    }

    public function update(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(),[
            'poste' =>'required',
            'niveau' =>'required',
            'experience' =>'required',
            'lieu' =>'required',
            'datepublie' =>'required',
            'datelimite' =>'required',
            'description' =>'required',
            'types' =>'required',
            'catid' =>'required',
            'slug' =>'required',
        ])->validate();

        $offre = Offre::where('id',$request->slug)->first();
        $offre->poste = $request->poste;
        $offre->cat_id = $request->catid;
        $offre->type = $request->types;
        $offre->niveau = $request->niveau;
        $offre->experience = $request->experience;
        $offre->lieu = $request->lieu;
        $offre->datepublie = $request->datepublie;
        $offre->datelimite = $request->datelimite;
        $offre->description = $request->description;
        $offre->save();


        return redirect()->route('offres')->with('success','✔ Félicitation ! vous venez de modifer une offre');
    }

    public function delet($id){
        //dd($id);
        if($id){
            $cat = Offre::findOrFail($id);
            $cat->delete();
            return redirect()->route('offres')->with('success','Vous venez de supprimer une offre');
        }
        return redirect()->back();
    }


    public function indexCats(){
        $cats = OffreCats::all();
        return view('offres.categories.index',compact('cats'));
    }

    public function storeCats(Request $request){
        //dd($request->all());
        Validator::make($request->all(),[
            'nom' =>'required',
        ])->validated();


        $cat = new OffreCats();
        $cat->libelle = $request->nom ;
        $cat->slug= Str::slug($request->nom);
        // $cat->description = $request->description ;
        $cat->save();

        return redirect()->route('offres.cats')->with('success','✔ Félicitation ! vous venez d\'ajouter une catégorie');
    }

    public function editCats($id){
        $cat = OffreCats::find($id);
        return view('offres.categories.edit',compact('cat'));
    }

    public function updateCats(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required|min:6',
            'idActu'=>'required'
        ])->validate();


        $cat = OffreCats::where('slug',$request->idActu)->first();
        $cat->libelle = $request->nom ;
        $cat->slug= Str::slug($request->nom);
        // $cat->description = $request->description ;
        $cat->save();

        return redirect()->route('offres.cats')->with('success','Vous venez de modifier une catégorie');
    }

    public function deletCats($id){
        //dd($id);
        if($id){
            $cat = OffreCats::findOrFail($id);
            $cat->delete();
            return redirect()->route('offres.cats')->with('success','Vous venez de supprimer une catégorie');
        }
        return redirect()->back();
    }
}
