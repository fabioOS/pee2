<?php

namespace App\Http\Controllers\Etu;

use App\Cat_format;
use App\Catformation;
use App\Etudiant;
use App\Formation;
use App\Formation_demande;
use App\Formation_favori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\NotifMail;
use App\Notif;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class FormationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isEtudiant']);
    }

    public function index(Request $request){
        $search = '';
        $categs = Catformation::orderBy('libelle','asc')->where('statut',1)->get();
        $total=0;
        $validDemand = Formation_demande::where('etudiant_id', Auth::user()->id)->where('statut', 1)->pluck('formation_id')->toArray();
        $formations = [];
        if(count($validDemand)>0) {
            $qry = Formation::whereIn('id', $validDemand)->where('statut',1)->orderBy('debut','desc')->where('datecloture', '>=', date('Y-m-d'));
            if($request->has('search') && $request->search!=null) {
                $qry->where(function($query) {
                    $query->where('titre', 'like', '%'.request('search').'%')
                        ->orWhere('description', 'like', '%'.request('search').'%')
                        ->orWhere('motscles', 'like', '%'.request('search').'%')
                        ->orWhere('prerequis', 'like', '%'.request('search').'%');
                });
                $search = request('search');
            }
            if($request->has('catego') && $request->catego!=null) {
                $cat_id = $request->catego;
                $liste_forma_id = Cat_format::selectRaw('DISTINCT(formation_id) AS formation')->where('catformation_id',$cat_id)->pluck('formation')->toArray();
                $qry->whereIn('id', $liste_forma_id);
            }
            $total = $qry->count();
            $formations = $qry->paginate(5);
        }
        return view('etu.formations.index',compact('formations','categs','total','search'));
    }

    public function show($slug){
        try {
            $item = Formation::where('slug',$slug)->first();
            if($item == null) {
                $demandF = Notif::where('user_id',Auth::user()->id)->where('lu',0)->get();
                if(count($demandF)>0){
                    foreach ($demandF as $key => $value) {
                        $value->lu = 1;
                        $value->save();
                    }
                }
                return redirect(route('etu.formations'))->with('error', 'La formation est clôturée !');
            }
            $prof = $item->professeur;
            $ins = [
                'format' =>$prof->formations->where('datecloture', '<', date('Y-m-d'))->count(),
                'etu' =>DB::table('formation_demandes')->where('formation_id',$item->id)->count(),
            ];
            return view('etu.formations.show', compact('item', 'ins'));
        } catch (\Exception $e) {
            return $e->getMessage();
            return back()->with('error', 'Erreur lors de l\'affichage de la formation !');
        }
    }
    //favoris
    public function favoris(){
        $favoris = Formation_favori::where('etudiant_id', Auth::user()->id)->pluck('formation_id')->toArray();
        $formations = Formation::whereIn('id', $favoris)->where('statut',1)->get();
        return view('etu.formations.favoris', compact('formations'));
    }

    public function favoris_show($slug){
        try {
            $item = Formation::where('slug',$slug)->first();
            $prof = $item->professeur;
            $ins = [
                'format' =>$prof->formations->where('datecloture', '<', date('Y-m-d'))->count(),
                'etu' =>DB::table('formation_demandes')->where('formation_id',$item->id)->count(),
            ];
            return view('etu.formations.favoris_show', compact('item', 'ins'));
        } catch (\Exception $e) {
            return $e->getMessage();
            return back()->with('error', 'Erreur lors de l\'affichage de la formation !');
        }
    }

    public function savefavoris($id){
        try {
            $formation_id = decrypt($id);
            $favoris = Formation_favori::where('etudiant_id', Auth::user()->id)->where('formation_id', $formation_id)->first();
            if($favoris != null) {
                $favoris->delete();
                $message = 'Retiré avec succès !';
                return response()->json(['status' => 'success', 'message' => '<br>Retirée avec succès !']);
            }
            $formation = new Formation_favori();
            $formation->etudiant_id = Auth::user()->id;
            $formation->formation_id = $formation_id;
            $formation->save();
            return response()->json(['status' => 'success', 'message' => '<br>Ajoutée avec succès !']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => '<br>Erreur lors de l\'ajout de la formation aux favoris !']);
        }
    }

    public function savedemande($id){
        try {
            $formation_id = decrypt($id);
            $demande = Formation_demande::where('etudiant_id', Auth::user()->id)->where('formation_id', $formation_id)->first();
            if($demande == null) {
                if(Auth::user()->status == 0) {
                    return response()->json(['status' => 'error', 'message' => '<br>Votre compte est suspendu, veuillez contacter l\'administration pour en savoir plus !']);
                }
                // check profil (etudiant
                $check_profils = Etudiant::where('user_id', Auth::user()->id)->first();
                if($check_profils == null) {
                    return response()->json(['status' => 'error', 'message' => '<br>Veuillez compléter votre profil avant de vous inscrire à une formation  ! <a class="btn btn-dark" href="'.route('etu.profiles').'">Compléter mon profil</a>']);
                }
                $demande = new Formation_demande();
                $demande->etudiant_id = Auth::user()->id;
                $demande->formation_id = $formation_id;
                $demande->statut = 0;
                $demande->save();
                //users to notify
                $to = User::whereIn('role_id',[1,2])->where('status',1)->get();
                //send notif to admin
                foreach($to as $r) {
                    Notif::create([
                        'user_id' => $r->id,
                        'titre' => 'Nouvelle demande de formation',
                        'message' => 'Vous avez une nouvelle demande de formation. Veuillez la traiter.',
                        'lien' => route('formations.demandes.edit', encrypt($demande->id)),
                    ]);
                }
                // send mail to admin
                $mss = 'Vous avez une nouvelle demande de formation. Veuillez vérifier les informations.';
                $data = [
                    'name' => env('APP_NAME'),
                    'email' => env('MAIL_FROM_ADDRESS'),
                    'subject' => 'Nouvelle demande de formation | '.env('APP_NAME'),
                    'message' => $mss,
                    'view'  => 'emails.newdemande',
                    'data'  => [$demande,Auth::user()],
                    'link' => route('formations.demandes.edit', encrypt($demande->id)),
                    'attachment' => ''
                ];
                foreach($to as $t) {
                    Mail::to($t->email)->send(new NotifMail($data));
                }
            }
            return response()->json(['status' => 'success', 'message' => '<br>Votre demande a bien été enregistrée, elle sera traitée dans les meilleurs délais !']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Une erreur c\'est produite !'.$e->getMessage()]);
        }
    }
}
