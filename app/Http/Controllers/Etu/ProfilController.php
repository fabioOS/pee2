<?php

namespace App\Http\Controllers\Etu;

use App\Etudiant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'isEtudiant']);
    }

    public function index(){
        $user = Auth::user();
        $etu = Etudiant::where('user_id',Auth::user()->id)->first();
        return view('etu.profiles.index', compact('user','etu'));
    }

    public function profilupdate(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required'
        ]);
        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors())->with('error','Désolé ! Veuillez remplir tous les champs obligatoires');
        }else{
            if(Auth::user()->role_id != 4) {
                $user = User::where('id',Auth::user()->id)->firstOrFail();
                $user->name = $request->nom;
                $user->sexe = $request->sexe;
                $user->save();
            }
            else {
                $user = User::where('id',Auth::user()->id)->firstOrFail();
                $user->name = $request->nom;
                $user->sexe = $request->civilite;
                $user->save();
                if($request->contact1 != null && $request->civilite != '' && $request->niveau != '' && $request->datenaiss != '' && $request->nationalite != '') {
                    $check_etu = Etudiant::where('user_id',Auth::user()->id)->first();
                    if($check_etu != null) {
                        //update
                        $etu = Etudiant::where('user_id',Auth::user()->id)->firstOrFail();
                    }
                    else {
                        //create
                        $etu = new Etudiant();
                        $etu->id = Auth::user()->id;
                        $etu->user_id = Auth::user()->id;
                    }
                    $etu->civilite = $request->civilite;
                    $etu->niveau = $request->niveau;
                    $etu->formation = $request->formation;
                    $etu->datenaiss = $request->datenaiss;
                    $etu->lieunaiss = $request->lieunaiss;
                    $etu->nationalite = $request->nationalite;
                    //$etu->adresse = $request->adresse;
                    $etu->ville = $request->ville;
                    $etu->contact1 = $request->contact1;
                    $etu->contact2 = $request->contact2;
                    $etu->etablissement = $request->etablissement;
                    $etu->type = $request->typeinvest;
                    $cv = '';
                    $file = $request->file('cvfile');
                    if($file != null) {
                        if($file->getSize() > 10485760){
                            $message = "La taille du fichier join dépace la limite de 10M !";
                            return back()->with('error', $message);
                        }
                        //stop if type id not in allowed array list/
                        $allowed = array('pdf', 'jpg', 'jpeg', 'png', 'doc','docx');
                        $ext = $file->getClientOriginalExtension();
                        if(!in_array($ext, $allowed)){
                            $message = "Le format du fichier join n'est pas pris en charge !";
                            return back()->with('error',$message);
                        }
                        $folderName ='assets/userCv/';
                        $fichier = Str::random(8).'.'. $ext;
                        if (!empty($etu->cv)) unlink($folderName.$etu->cv);
                        $cv = $fichier;
                        $file->move($folderName,$fichier);
                    }
                    $etu->cv = $cv;
                    $etu->save();
                }
                else {
                    return redirect()->back()->with('error','Désolé ! Veuillez remplir tous les champs obligatoires');
                }
            }
            return redirect()->back()->with('success','✔ Profil Modifié avec succès !');
        }
    }

    public function profileditpass(Request $request){
        //dd($request->all());
        $reponse = $request->except(['_token']);
        $valider = Validator::make($request->all(),[
            'motpass' =>'max:255|min:6|required',
            'newpass' =>'max:255|min:6|required',
            'confirmpass' =>'max:255|min:6|required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $oldpass = $reponse['motpass'];
            $newpass = $reponse['newpass'];
            $newpassconfirm = $reponse['confirmpass'];
            $passUser = Auth::user()->password;
            if($newpass===$newpassconfirm)
            {
                if(Hash::check($oldpass,$passUser) ){

                    $user = User::find(Auth::user()->id);
                    $user->password = Hash::make($newpass);
                    $user->save();

                    return redirect()->back()->with('success','Félicitation votre mot de passe a été mise a jour');
                }else{
                    return redirect()->back()->with('error','Désolé ! votre mot de passe actuel est erronée');
                }
            }else{
                return redirect()->back()->with('error','Les mots de passe sont différents');
            }

        }
    }

    public function profileditavatar(Request $request){
        //dd($request->file('fileUser'));
        $file=$request->file('fileUser');
        $fileSize=$file->getSize();
        //dd($fileSize);
        try{
            if($fileSize <= 3000263):
                $extension = $file->getClientOriginalExtension() ?: 'png';
                //$folderName = '../public_html/piges/panneau';
                $folderName ='assets/userAvatar/';
                $picture = Str::random(8).'.'. $extension;

                if (!empty(Auth::user()->img)) {
                    unlink($folderName.Auth::user()->img);
                }
                $users= User::find(Auth::user()->id);
                $users->img = $picture;
                $users->save();

                $file->move($folderName,$picture);

                return redirect()->back()->with('success','Félicitation ! votre avatar a été mise à jours');
            else:
                return redirect()->back()->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 3Mb');
            endif;

        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }
}
