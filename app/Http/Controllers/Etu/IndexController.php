<?php

namespace App\Http\Controllers\Etu;

use App\Formation_demande;
use App\Formation_favori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notif;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index(){
        //mes formations
        $dt['form'] = Formation_demande::where('etudiant_id', Auth::user()->id)->where('statut',1)->count();
        $dt['deman'] = Formation_demande::where('etudiant_id', Auth::user()->id)->where('statut',0)->count();
        $dt['favo'] = Formation_favori::where('etudiant_id', Auth::user()->id)->count();
        return view('etu.index', compact('dt'));
    }
    /**
     * Notifications
     */
    public function notif(){
        $notifies = Notif::where('user_id', Auth::user()->id)->where('lu',0)->get();
        $nt = Notif::where('user_id',Auth::user()->id)->where('lu',0)->get();
        if(count($nt)>0){
            foreach ($nt as $key => $value) {
                $value->lu = 1;
                $value->save();
            }
        }
        return view('etu.notif', compact('notifies'));
    }
    /**
     * Faire les CRUD de formation
     */
}
