<?php

namespace App\Http\Controllers;

use App\Partenaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PartenairesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //dd('dd');
        $partenaires = Partenaire::get();
        return view('partenaires.index',compact('partenaires'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'titre' =>'required',
            'fileUser' =>'required',
        ]);

        if($valider->fails()){
            //dd($valider->errors());
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $alb = new Partenaire();
            $alb->titre = $request->titre ;
            $alb->lien = $request->lien ?? null;

            $file=$request->file('fileUser');
            $picture = date('his').$file->getClientOriginalName();
            $folderName = 'assets/partenaires/';
            $file->move($folderName,$picture);
            $alb->img = $picture;

            $alb->save();

            return redirect()->route('partnaire')->with('success',"Le partenaire a bien été ajouté.");
        }
    }


    public function edit($id)
    {
        // $id = decrypt($id);
        $partenaire = Partenaire::find($id);
        return view('partenaires.edit',compact('partenaire'));
    }

    public function update(Request $request){
        //dd($request->all());
        Validator::make($request->all(),[
            'titre' =>'required',
            'slug' =>'required',
        ])->validate();

        //$id = decrypt($request->slug);
        $album = Partenaire::find($request->slug);
        $album->titre = $request->titre;
        $album->lien = $request->lien ?? null ;

        if($request->hasFile('fileUser')){
            $file=$request->file('fileUser');
            $picture = date('his').$file->getClientOriginalName();
            $folderName = 'assets/partenaires/';

            if (file_exists($folderName.$album->img)) {
                unlink($folderName.$album->img);
            }

            $file->move($folderName,$picture);
            $album->img = $picture;
        }

        $album->save();

        return redirect()->route('partnaire')->with('success',"Le partenaire a bien été modifié.");
    }

    public function delet($id)
    {
        //$id = decrypt($id);
        $photo = Partenaire::where('id',$id)->first();
        if($photo){
            $folderName = 'assets/partenaires/';
            if (file_exists($folderName.$photo->img)) {
                unlink($folderName.$photo->img);
            }
            $photo->delete();
            return redirect()->back()->with('success',"Le partenaire a bien été supprimé");
        }
        return redirect()->back();
    }

}
