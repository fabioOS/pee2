<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class EnseignantsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    public function index(){
        $data = User::where('role_id',3)->with('roles')->orderBy('id','desc')->get();
        return view('enseignants.index',compact('data'));
    }

    public function create(){
        return view('enseignants.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'nom' => 'required|string|max:255',
            'grade' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
        ]);

        if ($validator->fails()) {
            return redirect()->route('enseignants.create')->withErrors($validator->errors());
        }else{
            $user = new User();
            $user->name=$request->nom;
            $user->grade=$request->grade;
            $user->email=$request->email;
            $user->password=Hash::make($request->email);
            $user->role_id=3;
            $user->save();
            //send email to etudiant with password
            $data = array(
                'name' => $request->nom,
                'grade' => $request->grade,
                'email' => $request->email,
                'password' => $request->email,
            );
            Mail::send('emails.welcomeetudiant', $data, function($message) use ($data) {
                $message->to($data['email'], $data['name'])
                    ->subject('Bienvenue sur notre plateforme');
                $message->from(env('MAIL_USERNAME'),'Plateforme de gestion des stages');
            });
            //the content of mail view
            //emails.welcome
            //<!DOCTYPE html>
            //<html lang="en">
            //<head>
            //    <meta charset="UTF-8">
            //    <title>Bienvenue</title>
            //</head>
            //<body>
            //    <h1>Bienvenue sur notre plateforme</h1>
            //    <p>Voici vos identifiants de connexion</p>
            //    <p>Email : {{$email}}</p>
            //    <p>Mot de passe : {{$password}}</p>
            //</body>
            //</html>

            return redirect()->route('enseignants')->with('success','✔ Félicitation ! vous venez d\' ajoute un étudiant');
        }

    }

    public function edit($id){
        $user = User::find($id);
        return view('enseignants.edit',compact('user'));
    }

    public function update(Request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'nom' => 'required|string|max:255',
            'grade' => 'required|string',
            'email' => 'required|string|email|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }else{
            $user = User::find($request->iduser);
            $user->name=$request->nom;
            $user->grade=$request->grade;
            $user->email=$request->email;

            if($request->password != null){
                $user->password=Hash::make($request->password);
            }
            $user->save();

            return redirect()->route('enseignants')->with('success','Vous venez de modifier un étudiant');
        }
    }

    public function delete($id){
        if($id){
            $user = User::findOrFail($id);
            $user->delete();
            return redirect()->route('enseignants')->with('success','Vous venez de supprimer un étudiant');
        }
        return redirect()->back();
    }

    public function allDelet(Request $request){
        dd($request->all());
        $ids = explode(',',$request->value);
        //dd($ids);
        foreach ($ids as $id){
            $user = User::findOrFail($id);
            $user->delete();
        }
        echo $data = '1';
    }
}
