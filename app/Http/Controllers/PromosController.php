<?php

namespace App\Http\Controllers;

use App\Promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PromosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $promos = Promotion::orderBy('created_at','desc')->get();
        return view('promo.index',compact('promos'));
    }

    public function create(){
        return view('promo.create');
    }

    public function edit($id){
        //dd('edit');
        if($id){
            $actu = Promotion::find($id);
            return view('promo.edit',compact('actu'));
        }

        return back();
    }

    public function delet($id){
        //dd($id);
        if($id){
            $actu = Promotion::findOrFail($id);
            $actu->delete();
            return redirect()->route('promo')->with('success','Vous venez de supprimer une promotion');
        }
        return redirect()->back();
    }

    public function store(Request $request){
        //dd($request->all());
        Validator::make($request->all(),[
            'title' =>'required',
            'description' =>'required',
            'fileUser' =>'required',
            'date' =>'required',
            'auteur' =>'required',
        ])->validate();

        $actus = new Promotion();
        $actus->slug= Str::slug($request->title);
        $actus->titre = $request->title;
        $actus->description = $request->description;
        $actus->date = $request->date;
        $actus->auteur = $request->auteur;
        //$actus->cat_id = json_encode($request->catid);

        $file=$request->file('fileUser');
        $extension = $file->getClientOriginalExtension() ?: 'png';
        $folderName = 'promos/';
        $picture = Str::random(8).'.'. $extension;
        $file->move($folderName,$picture);
        $actus->img = $picture;
        $actus->save();

        return redirect()->route('promo')->with('success','✔ Félicitation ! vous venez d\'ajouter une promotion');

    }

    public function update(Request $request){
        //dd($request->all());

        $valider = Validator::make($request->all(),[
            'title' =>'required',
            'description' =>'required',
            'date' =>'required',
            'auteur' =>'required',
            'idActu'=>'required',
        ]);


        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{

            $actus = Promotion::where('slug',$request->idActu)->first();
            $actus->titre = $request->title;
            $actus->description = $request->description;
            $actus->date = $request->date;
            $actus->auteur = $request->auteur;
            //$actus->cat_id = json_encode($request->catid);

            if($request->hasFile('fileUser')){
                $file=$request->file('fileUser');
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName = 'promos/';
                $picture = Str::random(8).'.'. $extension;

                if (!empty($actus->img)) {
                    unlink($folderName.$actus->img);
                }

                $file->move($folderName,$picture);
                $actus->img = $picture;
            }


            $actus->status = '1';
            $actus->save();

            return redirect()->route('promo')->with('success','✔ Félicitation ! vous venez de modifier une promotion');
        }

    }


}
