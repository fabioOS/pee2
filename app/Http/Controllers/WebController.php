<?php

namespace App\Http\Controllers;

use App\Actu;
use App\ActuCategorie;
use App\Cat_format;
use App\Categorie;
use App\Catformation;
use App\Formation;
use App\Formation_demande;
use App\Livedemande_inst;
use App\Livedemande_part;
use App\Mail\NotifMail;
use App\Offre;
use App\OffreCats;
use App\Partenaire;
use App\Promotion;
use App\Testimonial;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class WebController extends Controller
{
    public function index()
    {
        $actus = Actu::with('categories')->where('status','1')->orderBy('date','desc')->take(10)->get();
        $actus->map(function($item){
            if($item->type_article=='video'){
                $idytbe = $item->video ? $this->getYoutubeIdFromUrl($item->video) : 'aRBKfvVkGhw';
                $item->img = "http://img.youtube.com/vi/".$idytbe."/maxresdefault.jpg";
            }
        });
        $dt['ins'] = User::where('role_id',3)->count();
        $dt['etu'] = User::where('role_id',4)->count();
        $dt['form'] = Formation::where('statut', 1)->count();
        $dt['demand'] = Formation_demande::where('statut', 1)->count();
        $dt['projet'] = Promotion::count();
        $dt['cert'] = 0;
        $partnaires = Partenaire::get();
        $temoignages = Testimonial::get();
        $catformations = Catformation::where('statut',1)->orderBy('libelle','asc')->get();
        $formations = Formation::where('statut',1)->orderBy('debut','desc')->where('datecloture', '>=', date('Y-m-d'))->limit(6)->get();
        return view('web.index',compact('actus','partnaires','temoignages','catformations','dt','formations'));
    }

    public function getActu($id){
        $cat = Actu::whereIn('cat_id',$id)->get();
        dd($cat);
        return $cat;
    }

    public function about()
    {
        return view('web.about');
    }

    public function formation(Request $request)
    {
        $qry = Formation::where('statut',1)->orderBy('debut','desc')->where('datecloture', '>=', date('Y-m-d'));
        $search = '';
        if($request->has('search') && $request->search!=null) {
            $qry->where(function($query) {
                $query->where('titre', 'like', '%'.request('search').'%')
                    ->orWhere('description', 'like', '%'.request('search').'%')
                    ->orWhere('motscles', 'like', '%'.request('search').'%')
                    ->orWhere('prerequis', 'like', '%'.request('search').'%');
            });
            $search = request('search');
        }
        if($request->has('catego') && $request->catego!=null) {
            $cat_id = $request->catego;
            $liste_forma_id = Cat_format::selectRaw('DISTINCT(formation_id) AS formation')->where('catformation_id',$cat_id)->pluck('formation')->toArray();
            $qry->whereIn('id', $liste_forma_id);
        }
        $total = $qry->count();
        //
        $formations = $qry->paginate(5);
        $grid=0;
        if($request->has('grid')) $grid = $request->grid;
        $categs = Catformation::orderBy('libelle','asc')->where('statut',1)->get();
        $profs = User::where('role_id',3)->orderBy('name','asc')->get();
        // count formation grouped by prix
        /* $form_by_prix=[
            'libre'=> Formation::where('prix',0)->where('statut',1)->where('datecloture', '>=', date('Y-m-d'))->count(),
            'payant'=> Formation::where('prix','>',0)->where('statut',1)->where('datecloture', '>=', date('Y-m-d'))->count(),
            ]; */
        return view('web.formations.index',compact('grid','formations','categs','profs','total','search'));
    }


    public function formation_show($slug) {
        $item = Formation::where('slug',$slug)->first();
        $prof = $item->professeur;
        $ins = [
            'format' =>$prof->formations->where('datecloture', '<', date('Y-m-d'))->count(),
            'etu' =>DB::table('formation_demandes')->where('formation_id',$item->id)->count(),
        ];
        return view('web.formations.show', compact('item','ins'));
    }


    public function formation_search(Request $request){
        $qry = Formation::where('statut',1)->where('datecloture', '>=', date('Y-m-d'))->orderBy('debut','desc');
        if($request->has('search') && $request->search!=null) {
            $qry->where(function($query) {
                $query->where('titre', 'like', '%'.request('search').'%')
                    ->orWhere('description', 'like', '%'.request('search').'%')
                    ->orWhere('motscles', 'like', '%'.request('search').'%')
                    ->orWhere('prerequis', 'like', '%'.request('search').'%');
            });
        }
        //by_cat
        if($request->has('by_cat') && $request->by_cat != null) {
            $liste_forma_id = Cat_format::selectRaw('DISTINCT(formation_id) AS formation')->where('catformation_id',$request->by_cat)->pluck('formation')->toArray();
            $qry->whereIn('id', $liste_forma_id);
        }
        //by_instruct
        if($request->has('by_instruct') && $request->by_instruct != null) {
            $qry->where('professeur_id', $request->by_instruct);
        }
        //by_cost
        if($request->has('by_cost') && $request->by_cost != null) {
            $cost = $request->by_cost;
            if($cost == 'free') {
                $qry->whereNull('prix');
            }
            elseif($cost == 'no-free') {
                $qry->whereNotNull('prix');
            }
            else {
                $qry->where('prix','>=', 0);
            }
        }
        $total = $qry->count();
        $formations = $qry->paginate(5);
        $grid = $request->grid;
        $search = '';
        if($grid == 1)
            return view('web.formations.data_grid', compact('formations','total','grid','search'));
        else
            return view('web.formations.data_list', compact('formations','total','grid','search'));
    }

    public function offre()
    {
        $offres = Offre::where('statut',1)->where('datelimite', '>=', date('Y-m-d'))->orderBy('datepublie','desc')->paginate(8);
        $cats = OffreCats::with('offre')->orderBy('libelle','asc')->get();
        $libel = null;

        return view('web.offre',compact('offres','cats','libel'));
    }

    public function offreType($slug)
    {
        switch ($slug) {
            case 'emplois':
                $id = 1;
                $libel = "Emplois";
                break;

            case 'stages':
                $id = 2;
                $libel = "Stages";

                break;

            case 'financement':
                $id = 3;
                $libel = "Financement";

                break;

            default:
                $id = 0;
                break;
        }

        $offres = Offre::where('statut',1)->where('datelimite','>=',date('Y-m-d'))->orderBy('datepublie','desc')->where('type',$id)->paginate(8);
        // $cats = OffreCats::with('offre')->orderBy('libelle','asc')->get();
        $cats = OffreCats::with('offre')->orderBy('libelle','asc')->get();
        $cats->map(function($item) use($id){
            $item->idd =$id;
        });

        return view('web.offre',compact('offres','cats','libel'));

    }

    public function offreItem($slug)
    {
        $offre = Offre::where('slug',$slug)->first();
        return view('web.offre_item',compact('offre'));
    }

    public function offre_search(Request $request){
        //dd($request->all());
        $qry = Offre::where('statut',1)->where('datelimite', '>=', date('Y-m-d'));

        if($request->has('type') && $request->type!=null){
            switch ($request->type) {
                case 'Emplois':
                    $id = 1;
                    break;
                case 'Stages':
                    $id = 2;
                    break;

                case 'Financement':
                    $id = 3;
                    break;

                default:
                    $id = 1;
                    break;
            }

            $qry->where('type',$id);
        }

        if($request->has('search') && $request->search!=null) {
            $qry->where(function($query) {
                $query->where('poste', 'like', '%'.request('search').'%')
                    ->orWhere('description', 'like', '%'.request('search').'%')
                    ->orWhere('niveau', 'like', '%'.request('search').'%')
                    ->orWhere('lieu', 'like', '%'.request('search').'%');
            });
        }
        //by_cat
        if($request->has('by_cat') && $request->by_cat != null) {
            $qry->where('cat_id',$request->by_cat);
        }

        //by_date
        if($request->has('orderdate') && $request->orderdate != null) {
            $cost = $request->orderdate;
            if($cost == 'publie') {
                //dd('dd');
                $qry->orderBy('datepublie','asc');
            }else {
                //limite date
                $qry->orderBy('datelimite','asc');
            }
        }
        $total = $qry->count();
        $offres = $qry->paginate(8);
        //dd($offres);
        $search = '';
        return view('web.offre_data_list', compact('offres','total','search'));
    }



    public function promo()
    {
        $promos = Promotion::orderBy('date','desc')->paginate(9);
        return view('web.promo',compact('promos'));
    }

    public function promoItem($slug)
    {
        $promo = Promotion::where('slug',$slug)->first();
        if(!$promo){
            return redirect()->back();
        }
        return view('web.promo_item',compact('promo'));
    }

    public function getYoutubeIdFromUrl($url) {
        $parts = parse_url($url);

        if(isset($parts['query'])){

            parse_str($parts['query'], $qs);

            if(isset($qs['v'])){
                return $qs['v'];
            }else if(isset($qs['vi'])){
                return $qs['vi'];
            }
        }
        if(isset($parts['path'])){
            $path = explode('/', trim($parts['path'], '/'));
            return $path[count($path)-1];
        }
        return false;
    }


    public function news()
    {
        $actus = Actu::with('categories')->where('status','1')->orderBy('date','desc')->paginate(5);
        $actus->map(function($item){
            if($item->type_article=='video'){
                $idytbe = $item->video ? $this->getYoutubeIdFromUrl($item->video) : 'aRBKfvVkGhw';
                $item->img = "http://img.youtube.com/vi/".$idytbe."/maxresdefault.jpg";
            }
        });

        $cats = Categorie::orderBy('libelle','asc')->get();

        return view('web.news',compact('actus','cats'));
    }

    public function newsItem($slug)
    {
        $item = Actu::with('categories')->where('status','1')->where('slug',$slug)->firstOrFail();
        if($item->type_article=='video'){
            $idytbe = $item->video ? $this->getYoutubeIdFromUrl($item->video) : 'aRBKfvVkGhw';
            $item->img = "http://img.youtube.com/vi/".$idytbe."/maxresdefault.jpg";
        }

        $cats = Categorie::orderBy('libelle','asc')->get();
        $recent3 = Actu::where('id','<>',$item->id)->where('status','1')->orderBy('date','desc')->take('3')->get();
        $recent3->map(function($item){
            if($item->type_article=='video'){
                $idytbe = $item->video ? $this->getYoutubeIdFromUrl($item->video) : 'aRBKfvVkGhw';
                $item->img = "http://img.youtube.com/vi/".$idytbe."/maxresdefault.jpg";
            }
        });

        return view('web.news_item',compact('item','cats','recent3'));
    }

    public function newsCat($slug)
    {
        $cat = Categorie::where('slug',$slug)->first();
        $liib = $cat->libelle;
        $catid = $cat->id;

        $actus = Actu::with('categories')->whereHas('categories',function (Builder $query) use ($catid){
            $query->where('actualites_categories.cat_id',$catid);
        })->where('status','1')->orderBy('date','desc')->paginate(5);
        $actus->map(function($item){
            if($item->type_article=='video'){
                $idytbe = $item->video ? $this->getYoutubeIdFromUrl($item->video) : 'aRBKfvVkGhw';
                $item->img = "http://img.youtube.com/vi/".$idytbe."/maxresdefault.jpg";
            }
        });

        $cats = Categorie::orderBy('libelle','asc')->get();

        return view('web.news_cats',compact('actus','cats','liib'));
    }

    public function contact()
    {
        return view('web.contact');
    }

    public function contact_send(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'nom' => ['required', 'string', 'min:2'],
            'contact' => ['email'],
            'objet' => ['required'],
            'message' => ['required','min:3'],
        ], [
            'required' => 'Le champ :attribute est obligatoire.',
            'min' => 'Le champ :attribute doit être de 6 caractères minimum',
        ])->validate();

        if($request['g-recaptcha-response'] != null){
            $secret = env('CAPTCHA_SECRET');
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$request['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);

            if($responseData->success){
                $emailrep = env('MAIL_FROM_ADDRESS');
                //send mail
                $data=array(
                    "nom"=>$request->nom,
                    "email"=>$request->email,
                    "objet"=>$request->objet,
                    "texts"=>$request->message,
                );

                Mail::send('web/mails/contact',$data,function ($message) use($emailrep){
                    $message->to($emailrep);
                    $message->subject('CONTACT - '.env('APP_NAME'));
                });

                return redirect()->route('w.contact')->with('success','Votre message a bien été envoyé');
            }else{
                return redirect()->back()->with('error','Merci de cochez le captcha.');
            }

        }else{
            return redirect()->back()->with('error','Merci de cochez le captcha.');
        }
    }

    public function demande($type)
    {
        if($type=='inst') {
            $catformations = Catformation::where('statut',1)->orderBy('libelle','asc')->get();
            return view('web.demande_inst', compact('type','catformations'));
        }
        elseif($type=='part')
            return view('web.demande_part', compact('type'));
        else
            return redirect()->back();
    }

    public function demande_send(Request $request)
    {
        if($request['g-recaptcha-response'] == null){
            return redirect()->back()->with('error','Merci de cochez le captcha.');
        }
        $type = $request->type;
        if($request->type != 'inst' && $request->type != 'part') {
            return redirect()->back()->with('error','Une erreur inatendu c\'est produite, veuillez réessayer plutard !');
        }
        if($request->type == 'inst') {
            //encadreur
            Validator::make($request->all(), [
                'nom' => ['required', 'string','min:6','max:250'],
                'competence' => ['required','max:200'],
                'encadrement' => ['required'],
                'contact' => ['required','max:30'],
                'email' => ['email','required','min:6','max:100']
            ])->validate();
        }
        if($request->type == 'part') {
            //partenaire
            Validator::make($request->all(), [
                'nom' => ['required', 'string','min:6','max:250'],
                'entreprise' => ['required','max:100'],
                'domaine' => ['required','max:100'],
                'contact' => ['required','max:30'],
                'email' => ['email','required','min:6','max:100']
            ])->validate();
        }
        $secret = env('CAPTCHA_SECRET');
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$request['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success){
            $emailrep = env('MAIL_FROM_ADDRESS');
            $mss='';
            if($request->type == 'inst') {
                //check before
                $chek = Livedemande_inst::where(function($query) {
                    $query->where('email',request('email'))
                        ->orWhere('contact',request('contact'));
                })->first();
                if($chek) {
                    $mss = "Vous avez déjà une demande d'encadrement est en cours de traitement. <br>Nous vous contacterons dans les plus brefs délais.";
                    return redirect()->route('w.demande',$type)->with('error',$mss)->withInput();
                }
                if(count($request->encadrement) == 0) {
                    $mss = "Merci de sélectionner au moins un encadrement.";
                    return redirect()->route('w.demande',$type)->with('error',$mss)->withInput();
                }
                if(count($request->encadrement) == 1 && $request->encadrement[0] == '') {
                    $mss = "Merci de sélectionner au moins un encadrement.";
                    return redirect()->route('w.demande',$type)->with('error',$mss)->withInput();
                }
                //storage
                $dm = new Livedemande_inst();
                $dm->nom = $request->nom;
                $dm->competence = $request->competence;
                $dm->encadrements = (count($request->encadrement)>0)?json_encode($request->encadrement):'';
                $dm->email = $request->email;
                $dm->contact = $request->contact;
                $dm->commentaire = $request->commentaire;
                $dm->save();
                //send mail
                $data=array(
                    'nom' => $request->nom,
                    'competence' => $request->competence,
                    'encadrement' => $request->encadrement,
                    'email' => $request->email,
                    "contact"=>$request->contact,
                    'commentaire' => $request->commentaire,
                    'text' => "Vous avez une nouvelle demande d'encadrement. <br>Voir les détails ci-dessous"
                );
                // to admin
                Mail::send('web/mails/mail_demande_inst',$data,function ($message) use($emailrep){
                    $message->to($emailrep);
                    $message->subject("Demande d'encadrement - ".env('APP_NAME'));
                });
                //to demander
                $data['text'] = "Votre demande d'encadrement a bien été envoyé. <br>Voir les détails ci-dessous.";
                $demandeur = $request->email;
                Mail::send('web/mails/mail_demande_inst',$data,function ($message) use($demandeur){
                    $message->to($demandeur);
                    $message->subject("Demande d'encadrement - ".env('APP_NAME'));
                });
                $mss = "Votre demande d'encadrement a bien été envoyé. <br>Nous vous contacterons dans les plus brefs délais. <br>Un email récapitulatif vous a été envoyé.";
            }
            if($request->type == 'part') {
                //check before
                $chek = Livedemande_part::where(function($query) {
                    $query->where('email',request('email'))
                        ->orWhere('contact',request('contact'));
                })->first();
                if($chek) {
                    $mss = "Vous avez déjà une demande de partenariat est en cours de traitement. <br>Nous vous contacterons dans les plus brefs délais.";
                    return redirect()->route('w.demande',$type)->with('error',$mss)->withInput();
                }
                //storage
                $dm = new Livedemande_part();
                $dm->nom = $request->nom;
                $dm->entreprise = $request->entreprise;
                $dm->domaine = $request->domaine;
                $dm->email = $request->email;
                $dm->contact = $request->contact;
                $dm->commentaire = $request->commentaire;
                $dm->save();
                //send mail
                $data=array(
                    "nom"=>$request->nom,
                    "entreprise"=>$request->entreprise,
                    "domaine"=>$request->domaine,
                    "email"=>$request->email,
                    "contact"=>$request->contact,
                    "commentaire"=>$request->commentaire,
                    'text' => "Vous avez une nouvelle demande de partenariat. <br>Voir les détails ci-dessous."
                );
                // to admin
                Mail::send('web/mails/mail_demande_part',$data,function ($message) use($emailrep){
                    $message->to($emailrep);
                    $message->subject("Demande de partenariat - ".env('APP_NAME'));
                });
                //to demander
                $data['text'] = "Votre demande de partenariat a bien été envoyé. <br>Voir les détails ci-dessous.";
                $demandeur = $request->email;
                Mail::send('web/mails/mail_demande_part',$data,function ($message) use($demandeur){
                    $message->to($demandeur);
                    $message->subject("Demande de partenariat - ".env('APP_NAME'));
                });
                $mss = "Votre demande de partenariat a bien été envoyé. <br>Nous vous contacterons dans les plus brefs délais. <br>Un email récapitulatif vous a été envoyé.";
            }
            return redirect()->route('w.demande',$type)->with('success',$mss);
        }else{
            return redirect()->back()->with('error','Merci de cochez le captcha.');
        }
    }

    public function terme()
    {
        return view('web.termes');
    }

    public function politique()
    {
        return view('web.politique');
    }

    public function store_user_etud(Request $request) {
        try {
            if($request['g-recaptcha-response'] != null){
                $secret = env('CAPTCHA_SECRET');
                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$request['g-recaptcha-response']);
                $responseData = json_decode($verifyResponse);

                if($responseData->success){
                    if( $request->nm1 != '' && $request->em2 != '' && $request->pass3 != '' ) {
                        $cheking = User::where('email',$request->em2)->first();
                        if($cheking!=null) {
                            $message = "Cette adresse email appatient déjà à un autre étudiant !" ;
                            return back()->with('erro', $message);
                        }
                        $usr = new User();
                        $usr->role_id = 4;//étudiant
                        $usr->name = ucwords($request->nm1);
                        $usr->email = $request->em2;
                        $usr->password = Hash::make($request->pass3);
                        $usr->save();
                        // send email to user
                        $data = array(
                            'name' => $usr->name,
                            'email' => env('MAIL_FROM_ADDRESS'),
                            'subject' => 'Inscription réussie | '.env('APP_NAME'),
                            'message' => 'Votre compte a été créé avec succès. Veuillez-vous connecter pour continuer !',
                            'view' => 'emails.newetudiant',
                            'user'  => $usr,
                            'link' => route('login'),
                            'attachment' => ''
                        );
                        Mail::to($usr->email)->send(new NotifMail($data));
                        // send email to admin
                        $to = User::whereIn('role_id',[1,2])->where('status',1)->pluck('email');
                        $mss = 'Un nouveau étudiant vient de s\'inscrire sur le site. Veuillez vérifier ses informations.';
                        $data = [
                            'name' => env('APP_NAME'),
                            'email' => env('MAIL_FROM_ADDRESS'),
                            'subject' => 'Nouveau étudiant | '.env('APP_NAME'),
                            'message' => $mss,
                            'view'  => 'emails.newetudiant',
                            'user'  => $usr,
                            'link' => route('login'),
                            'attachment' => ''
                        ];
                        foreach($to as $r) {
                            Mail::to($r)->send(new NotifMail($data));
                        }

                        $message = "<h3>Bienvenue ".$usr->name." ! </h3> Votre compte a été créé avec succès. <br> Veuillez-vous <a href='".route('login')."'>connecter</a> pour continuer ! <br> <a href='".route('login')."' class='btn btn-info'>Connexion</a> " ;
                        return back()->with('success', $message);
                    }
                    else {

                    }

                    return redirect()->route('w.contact')->with('success','Votre message a bien été envoyé');
                }else{
                    return redirect()->back()->with('error','Merci de cochez le captcha.');
                }

            }else{
                return redirect()->back()->with('error','Merci de cochez le captcha.');
            }

        }
        catch(\Exception $e) {
            $message = $e->getMessage();
            return back()->with('error', $message);
        }

    }

    public function search(Request $request)
    {
        //dd($request->all());
        Validator::make($request->all(), [
            'q' => ['required'],
            'type' => ['required'],
        ])->validate();

        $search = $request->q;
        $type = $request->type;
        $searchValue = explode(' ',$search);
        if(!empty($searchValue)){
            if($type != 0){
                switch ($type) {
                    case '1':
                        //Forma
                        $datas = Formation::where('statut',1)->where('datecloture','>=',date('Y-m-d'));
                        foreach($searchValue as $seach){
                            $trim = trim(htmlentities($seach));
                            if (!empty($trim)){
                                $datas->where('titre', 'LIKE', '%'. $trim .'%')
                                    ->orWhere('description', 'LIKE', '%'. $trim .'%');
                            }
                        }
                        $datas = [
                            "formas"=>$datas->get(),
                        ];
                        break;
                    case '2':
                        //Emploie
                        $datas = Offre::where('statut',1)->where('type','1')->where('datelimite','>=',date('Y-m-d'));
                        foreach($searchValue as $seach){
                            $trim = trim(htmlentities($seach));
                            if (!empty($trim)){
                                $datas->where('poste', 'LIKE', '%'. $trim .'%')
                                    ->orWhere('niveau', 'LIKE', '%'. $trim .'%')
                                    ->orWhere('metier', 'LIKE', '%'. $trim .'%')
                                    ->orWhere('description', 'LIKE', '%'. $trim .'%');
                            }
                        }
                        $datas = [
                            "offres"=>$datas->get(),
                        ];

                        break;
                    case '3':
                        //Stage
                        $datas = Offre::where('statut',1)->where('type','2')->where('datelimite','>=',date('Y-m-d'));
                        foreach($searchValue as $seach){
                            $trim = trim(htmlentities($seach));
                            if (!empty($trim)){
                                $datas->where('poste', 'LIKE', '%'. $trim .'%')
                                    ->orWhere('niveau', 'LIKE', '%'. $trim .'%')
                                    ->orWhere('metier', 'LIKE', '%'. $trim .'%')
                                    ->orWhere('description', 'LIKE', '%'. $trim .'%');
                            }
                        }

                        $datas = [
                            "offres"=>$datas->get(),
                        ];
                        break;
                    case '4':
                        //Finance
                        $datas = Offre::where('statut',1)->where('type','3')->where('datelimite','>=',date('Y-m-d'));
                        foreach($searchValue as $seach){
                            $trim = trim(htmlentities($seach));
                            if (!empty($trim)){
                                $datas->where('poste', 'LIKE', '%'. $trim .'%')
                                    ->orWhere('niveau', 'LIKE', '%'. $trim .'%')
                                    ->orWhere('metier', 'LIKE', '%'. $trim .'%')
                                    ->orWhere('description', 'LIKE', '%'. $trim .'%');
                            }
                        }
                        $datas = [
                            "offres"=>$datas->get(),
                        ];

                        break;

                    default:
                        //Actu
                        $datas = Actu::where('status',1);
                        foreach($searchValue as $seach){
                            $trim = trim(htmlentities($seach));
                            if (!empty($trim)){
                                $datas->where('title', 'LIKE', '%'. $trim .'%')
                                    ->orWhere('des', 'LIKE', '%'. $trim .'%');
                            }
                        }
                        $datas = [
                            "actus"=>$datas->get(),
                        ];

                        break;
                }

            }else{
                //FOrmation
                $formas = Formation::where('statut',1)->where('datecloture','>=',date('Y-m-d'));
                foreach($searchValue as $seach){
                    $trim = trim(htmlentities($seach));
                    if (!empty($trim)){
                        $formas->where('titre', 'LIKE', '%'. $trim .'%')
                            ->orWhere('description', 'LIKE', '%'. $trim .'%');
                    }
                }
                $formas = $formas->get();

                //Offre
                $offres = Offre::where('statut',1)->where('datelimite','>=',date('Y-m-d'));
                foreach($searchValue as $seach){
                    $trim = trim(htmlentities($seach));
                    if (!empty($trim)){
                        $offres->where('poste', 'LIKE', '%'. $trim .'%')
                            ->orWhere('niveau', 'LIKE', '%'. $trim .'%')
                            ->orWhere('metier', 'LIKE', '%'. $trim .'%')
                            ->orWhere('description', 'LIKE', '%'. $trim .'%');
                    }
                }
                $offres = $offres->get();

                //Actu
                $actus = Actu::where('status',1);
                foreach($searchValue as $seach){
                    $trim = trim(htmlentities($seach));
                    if (!empty($trim)){
                        $actus->where('title', 'LIKE', '%'. $trim .'%')
                            ->orWhere('des', 'LIKE', '%'. $trim .'%');
                    }
                }
                $actus = $actus->get();

                $datas = [
                    "formas"=>$formas,
                    "offres"=>$offres,
                    "actus"=>$actus,
                ];
            }

            // dump($datas);
            return view('web.search',compact('search','type','datas'));
        }

        return redirect()->back();

    }
}
