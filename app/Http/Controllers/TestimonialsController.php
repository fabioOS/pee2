<?php

namespace App\Http\Controllers;

use App\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TestimonialsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listTestimonial()
    {
        $testimonials = Testimonial::all();
        return view('temoignage.index', compact('testimonials'));
    }

    public function createTestimonial()
    {
        return view('temoignage.create');
    }

    public function storeTestimonial(Request $request)
    {
        $valider = Validator::make($request->all(), [
            'nom' => 'required',
            'fonction' => 'required',
            'contenu' => 'required'
        ])->validate();


        $testimonials = new Testimonial();
        $testimonials->nom = $request->nom;
        $testimonials->fonction = $request->fonction;
        $testimonials->contenu  = $request->contenu ;

        /* UPLOAD FILE */
        $file = $request->file('fileTestimonial');
        $extension = $file->getClientOriginalExtension() ?: 'png';
        $folderName = 'assets/temoignages/';
        $picture = Str::random(8) . '.' . $extension;
        $file->move($folderName, $picture);
        $testimonials->photo = $picture;
        /* END UPLOAD FILE */

        $testimonials->save();
        return redirect()->route('testimonials')->with('success', '✔ Félicitation ! vous venez d\' ajoute un article');
    }

    public function editTestimonial($id)
    {
        $testi = Testimonial::find($id);
        if ($testi){
            return view('temoignage.edit',compact('testi'));
        }
        return redirect()->back();
    }

    public function updateTestimonial(Request $request)
    {
        $valider = Validator::make($request->all(), [
            'nom' => 'required',
            'fonction' => 'required',
            'contenu' => 'required',
            'slug' => 'required'
        ])->validate();


        $testimonials = Testimonial::where('id',$request->slug)->first();
        $testimonials->nom = $request->nom;
        $testimonials->fonction = $request->fonction;
        $testimonials->contenu  = $request->contenu ;

        if($request->hasFile('fileTestimonial')){
            /* UPLOAD FILE */
            $file = $request->file('fileTestimonial');
            $extension = $file->getClientOriginalExtension() ?: 'png';
            $folderName = 'assets/temoignages/';
            $picture = Str::random(7) . '.' . $extension;

            if (!empty($testimonials->photo)) {
                unlink($folderName . $testimonials->photo);
            }

            $file->move($folderName, $picture);
            $testimonials->photo = $picture;
            /* END UPLOAD FILE */
        }
        $testimonials->save();
        return redirect()->route('testimonials')->with('success', '✔ Félicitation ! vous venez de modifier un témoignage');
    }

    public function deleteTestimonial($id)
    {
        if($id){
            $testi = Testimonial::find($id);
            if (!empty($testi->photo)) {
                unlink('assets/temoignages/'.$testi->photo);
            }
            $testi->delete();
            return redirect()->route('testimonials')->with('success','Vous venez de supprimer un témoignage');
        }
        return redirect()->back();
    }
}
