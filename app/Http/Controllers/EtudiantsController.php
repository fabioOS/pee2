<?php

namespace App\Http\Controllers;

use App\Etudiant;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Imports\ImportClient;
use App\Imports\ImportEtudiant;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class EtudiantsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    public function index(){
        $data = User::where('role_id',4)->join('etudiants', 'etudiants.user_id', '=', 'users.id')->orderBy('users.id','desc')->get();
        return view('etudiants.index',compact('data'));
    }

    public function create(){
        return view('etudiants.create');
    }


    public function store(Request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'nom' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ]);

        if ($validator->fails()) {
            return redirect()->route('etudiants.create')->withErrors($validator->errors());
        }else{
            $user = new User();
            $user->name=$request->nom;
            $user->email=$request->email;
            $user->password=Hash::make($request->email);
            $user->role_id=4;
            $user->save();
            //send email to etudiant with password
            $data = array(
                'name' => $request->nom,
                'email' => $request->email,
                'password' => $request->email,
            );
            Mail::send('emails.welcomeetudiant', $data, function($message) use ($data) {
                $message->to($data['email'], $data['name'])
                    ->subject('Bienvenue sur notre plateforme');
                $message->from(env('MAIL_USERNAME'),'Plateforme de gestion des stages');
            });
            //the content of mail view
            //emails.welcome
            //<!DOCTYPE html>
            //<html lang="en">
            //<head>
            //    <meta charset="UTF-8">
            //    <title>Bienvenue</title>
            //</head>
            //<body>
            //    <h1>Bienvenue sur notre plateforme</h1>
            //    <p>Voici vos identifiants de connexion</p>
            //    <p>Email : {{$email}}</p>
            //    <p>Mot de passe : {{$password}}</p>
            //</body>
            //</html>

            return redirect()->route('etudiants')->with('success','✔ Félicitation ! vous venez d\' ajoute un étudiant');
        }

    }


    public function import(){
        return view('etudiants.import');
    }

    public function extention() {
        return [
            'xls', 'xlsx', 'csv'
        ];
    }

    public function storeimport(Request $request)
    {
        $ext = $request->file('importfile')->extension();
        if(!in_array($ext, $this->extention())) {
            $response = "Ce type de fichier (<b>.{$ext}</b>) n'est pas autorisé !";
        }
        else {
            try {
                // store imported file before
                $file = $request->file('importfile');
                $file_extension = $file->getClientOriginalExtension();
                //$file_name = $request->file('importfile')->getClientOriginalName();
                $file_name = date('Ymd_His').'_'.Str::random(8).'.'. $file_extension;
                $file_path = $request->file('importfile')->storeAs('importEtudiants', $file_name);
                /// import file content into database
                /**
                 * Import data from excel file
                 */
                $filedata = Excel::toCollection(new ImportEtudiant, $request->file('importfile'));
                $filedata = $filedata[0]->toArray();
                //dd($filedata);
                if(count($filedata)>0) {
                    $format_error = 0 ;
                    $inserted  = $updated = $old = 0;
                    $error_array = [];
                    foreach($filedata as $key=>$row ){
                        //NOM ET PRENOMS///E-MAIL///CONTACT 1///CONTACT 2///DATE DE NAISSANCE///LIEU DE NAISSANCE///NATIONALITE
                        //ADRESSE///NIVEAU D'ETUDE///FORMATION///SPECIALITE///CATEGORIE D'ENTREPRENEUR
                        $nomprenom = trim($row[0], ' ');
                        $email = trim($row[1], ' ');
                        $contact1 = trim($row[2], ' ');
                        $contact2 = trim($row[3], ' ');
                        $date_naissance = trim($row[4], ' ');
                        $lieu_naissance = trim($row[5], ' ');
                        $nationalite = trim($row[6], ' ');
                        $sexe = trim($row[7], ' ');
                        $niveau_etude = trim($row[8], ' ');
                        $formation = trim($row[9], ' ');
                        $specialite = trim($row[10], ' ');
                        $categorie = trim($row[11], ' ');
                        if($key == 0) {
                            if( $nomprenom!='NOM ET PRENOMS' || $email!='E-MAIL' || $contact1!='CONTACT 1' || $contact2!='CONTACT 2' ||
                                $date_naissance!='DATE DE NAISSANCE' || $lieu_naissance!='LIEU DE NAISSANCE' || $nationalite!='NATIONALITE' ||
                                $sexe!='SEXE' || $niveau_etude!="NIVEAU D'ETUDE" || $formation!='FORMATION' || $specialite!='SPECIALITE' ||
                                $categorie!="CATEGORIE D'ENTREPRENEUR" ) {
                                    $format_error = 1;
                                    // exit error
                                    $error = " Erreur : le format de fichier n'est pas conforme au format de base !";
                                return back()->with('error',$error);
                            }
                        }
                        else {
                            if($nomprenom!='' && $email!='' && $contact1!=''){
                                $niveau_etude = strtoupper($niveau_etude);
                                // check if etudiant exist
                                $check_usr = User::where('email', $email)->first();
                                $check_etu = Etudiant::where('contact1', $contact1)->first();

                                    if($check_usr == null && $check_etu == null) {
                                        //insert dossier
                                        $user = User::create(
                                            [
                                                'name' => $nomprenom,
                                                'email' => $email,
                                                'sexe' => $sexe,
                                                'password' => Hash::make($email),
                                                'role_id' => 4,
                                                'status' => 1
                                            ]
                                        );
                                        if($user) {
                                            $inserted++;
                                            if($contact2 != '' || $date_naissance != '' || $lieu_naissance != '' || $nationalite != '' || $sexe != '' || $niveau_etude != '' || $formation != '' || $specialite != '' || $categorie != '') {
                                                if($date_naissance != null && $date_naissance != '') {
                                                    if( is_numeric($date_naissance))
                                                        $date_naissance = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date_naissance))->format('Y-m-d');
                                                    else
                                                        $date_naissance = Carbon::createFromFormat('d/m/Y', $date_naissance)->format('Y-m-d');

                                                }
                                                else
                                                    $date_naissance = null;
                                                $etudiant = Etudiant::create(
                                                    [
                                                        'user_id' => $user->id,
                                                        'civilite' => $sexe,
                                                        'niveau' =>$niveau_etude,
                                                        'formation' => $formation,
                                                        'specialite' => $specialite,
                                                        'datenaiss' => $date_naissance,
                                                        'lieunaiss' => $lieu_naissance,
                                                        'nationalite'=> $nationalite,
                                                        'contact1' => $contact1,
                                                        'contact2' => $contact2,
                                                        'type' => $categorie
                                                    ]
                                                );
                                            }

                                        }
                                    }
                                    else { //insert dossier
                                        $old++;

                                    }
                            }
                            else {
                                $error_array[] = $key+1;
                            }
                        }
                    }
                }
                else {
                    $response = "Votre fichier est vide !";
                }
                if($inserted > 0 || $old > 0 || count($error_array)>0) {
                    if($old>0) $upd = '<p class="text-primary">'.$old.' ancien(s) étudiant(s) ignoré(s).</p>';
                    else $upd='';
                    $response = "Votre fichier a été importé avec succès !<br>";
                    $response .= "Rapport : ";
                    $response .= ($inserted<2) ? "<b>".$inserted."</b> nouveau étudiant importé " : "<b>".$inserted."</b> nouveaux étudiants importés ";
                    $response .= (count($error_array)>0) ? "<b>".count($error_array)."</b> ligne(s) non correctement renseignée(s)" :'';
                    $response  = '<div>'.$response.$upd."</div>";
                    return back()->with('success',$response);
                }
                else {
                    if($response == '')
                        $response = "<div>Une erreur inconnue c'est produite, veuillez réessayer !</div>";
                    else
                        $response = "<div>".$response."</div>";
                    //return
                    return back()->with('error',$response);
                }
            }
            catch(\Exception $e) {
                $response = $e->getMessage();
                return back()->with('error',$response);
            }
        }
    }



    public function show($email){
        $email = decrypt($email);
        $user = User::where('email',$email)->first();
        $demandes = count($user->demandes_etus->where('statut',0));
        $formations = count($user->demandes_etus->where('statut',1));
        $user->join('etudiants', 'etudiants.user_id', '=', 'users.id')->orderBy('users.id','desc')->first();
        // formations effectuées
        return view('etudiants.show',compact('user','demandes','formations'));
    }



    public function edit($email){
        $email = decrypt($email);
        $user = User::where('email',$email)->first();
        $etu = Etudiant::where('user_id',$user->id)->first();
        //return view('etu.profiles.index', compact('user'));
        return view('etudiants.edit',compact('user','etu'));
    }

    public function updateProfil(Request $request){
        //dd($request->all());
        $valider = Validator::make($request->all(),[
            'nom' =>'required'
        ]);
        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors())->with('error','Désolé ! Veuillez remplir tous les champs obligatoires');
        }else{
            $id = decrypt($request->etu);
            $user = User::findOrFail($id);
            $user->name = $request->nom;
            $user->sexe = $request->civilite;
            $user->save();
            if($request->contact1 != null && $request->civilite != '' && $request->niveau != '' && $request->datenaiss != '' && $request->nationalite != '') {
                $check_etu = Etudiant::where('user_id',$id)->first();
                if($check_etu != null) {
                    //update
                    $etu = Etudiant::where('user_id',$id)->firstOrFail();
                }
                else {
                    //create
                    $etu = new Etudiant();
                    $etu->id = $id;
                    $etu->user_id = $id;
                }
                $etu->civilite = $request->civilite;
                $etu->niveau = $request->niveau;
                $etu->formation = $request->formation;
                $etu->datenaiss = $request->datenaiss;
                $etu->lieunaiss = $request->lieunaiss;
                $etu->nationalite = $request->nationalite;
                //$etu->adresse = $request->adresse;
                $etu->ville = $request->ville;
                $etu->contact1 = $request->contact1;
                $etu->contact2 = $request->contact2;
                $etu->etablissement = $request->etablissement;
                $etu->type = $request->typeinvest;
                $cv = '';
                $file = $request->file('cvfile');
                if($file != null) {
                    if($file->getSize() > 10485760){
                        $message = "La taille du fichier join dépace la limite de 10M !";
                        return back()->with('error', $message);
                    }
                    //stop if type id not in allowed array list/
                    $allowed = array('pdf', 'jpg', 'jpeg', 'png', 'doc','docx');
                    $ext = $file->getClientOriginalExtension();
                    if(!in_array($ext, $allowed)){
                        $message = "Le format du fichier join n'est pas pris en charge !";
                        return back()->with('error',$message);
                    }
                    $folderName ='assets/userCv/';
                    $fichier = Str::random(8).'.'. $ext;
                    if (!empty($etu->cv)) unlink($folderName.$etu->cv);
                    $cv = $fichier;
                    $file->move($folderName,$fichier);
                }
                $etu->cv = $cv;
                $etu->save();
            }
            else {
                return redirect()->back()->with('error','Veuillez remplir tous les champs obligatoires');
            }
            return redirect()->route('etudiants')->with('success','✔ Etudiant Modifié avec succès !');
        }
    }


    public function updatePass(Request $request){
        $reponse = $request->except(['_token']);
        $valider = Validator::make($request->all(),[
            'newpass' =>'max:255|min:6|required',
            'confirmpass' =>'max:255|min:6|required',
        ]);

        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }else{
            $newpass = $reponse['newpass'];
            $newpassconfirm = $reponse['confirmpass'];
            if($newpass!='' && $newpass==$newpassconfirm)
            {
                $id = decrypt($request->etu);
                $user = User::findOrFail($id);
                $user->password = Hash::make($newpass);
                $user->save();
                return redirect()->back()->with('success','Mot de passe modifié avec succès !');
            }else{
                return redirect()->back()->with('error','Les mots de passe doivent être indsont différents !');
            }

        }
    }

    public function updateAvatar(Request $request){
        $valider = Validator::make($request->all(),[
            'fileUser' =>'required|image|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ],
        [
            'fileUser.required' => 'Veuillez choisir une image',
            'fileUser.image' => 'Le fichier doit être une image',
            'fileUser.mimes' => 'Le fichier doit être une image de type jpeg, png, jpg, gif, svg',
            'fileUser.max' => 'La taille du fichier ne doit pas dépasser 5Mo',
        ]
        );
        if($valider->fails()){
            return redirect()->back()->withErrors($valider->errors());
        }
        $file=$request->file('fileUser');
        $fileSize=$file->getSize();
        try{
            if($fileSize <= 5000440):
                $extension = $file->getClientOriginalExtension() ?: 'png';
                $folderName ='assets/userAvatar/';
                $picture = Str::random(8).'.'. $extension;
                $id = decrypt($request->etu);
                $user = User::findOrFail($id);
                if (!empty($user->img)) unlink($folderName.$user->img);
                $users= User::find($user->id);
                $users->img = $picture;
                $users->save();
                $file->move($folderName,$picture);
                return redirect()->back()->with('success','Photos de profil modifiée avec succès !');
            else:
                return redirect()->back()->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 5Mb');
            endif;

        }catch (\Exception $e){
            dd($e->getMessage());
        }
    }


    public function delete($email){
        $email = decrypt($email);
        $user = User::where('email',$email)->first();
        if($user != null){
            $user->delete();
            return redirect()->route('etudiants')->with('success','Vous venez de supprimer un étudiant');
        }
        return redirect()->back();
    }

    public function allDelet(Request $request){
        //dd($request->all());
        $ids = explode(',',$request->value);
        //dd($ids);
        foreach ($ids as $id){
            $user = User::findOrFail($id);
            $user->delete();
        }
        echo $data = '1';
    }
}
