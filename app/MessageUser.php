<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageUser extends Model
{
    protected $table ="messages_users";

    public function getmessages(){
        return $this->belongsTo('App\Message','message_id');
    }

    public function toUser(){
        return $this->belongsTo('App\User','to_id');
    }

    public function fromUser(){
        return $this->belongsTo('App\User','from_id');
    }
}
