<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ImportEtudiant implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public $message;
    public function collection(Collection $collection)
    {
        return $collection;
    }
}
