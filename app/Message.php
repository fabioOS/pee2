<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;

    public function itemdes(){
        return $this->hasMany('App\MessageUser','message_id');
    }

    public function fromUser(){
        return $this->belongsTo('App\User','from_id');
    }
}
