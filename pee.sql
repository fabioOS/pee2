-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 24 avr. 2023 à 23:52
-- Version du serveur : 10.4.22-MariaDB
-- Version de PHP : 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `pee`
--

-- --------------------------------------------------------

--
-- Structure de la table `actualites`
--

CREATE TABLE `actualites` (
  `id` int(6) NOT NULL,
  `slug` varchar(225) NOT NULL,
  `title` varchar(225) NOT NULL,
  `des` longtext NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `type_article` varchar(100) NOT NULL,
  `img` varchar(100) DEFAULT NULL,
  `video` varchar(225) DEFAULT NULL,
  `cat_id` varchar(225) DEFAULT NULL,
  `auteur` varchar(100) DEFAULT NULL,
  `date` date DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `actualites`
--

INSERT INTO `actualites` (`id`, `slug`, `title`, `des`, `status`, `type_article`, `img`, `video`, `cat_id`, `auteur`, `date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'mon-titre', 'Mon titre', '<p>Mon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titreMon titre</p>', '0', 'text', 'QkPrtLbt.jpeg', NULL, '[\"2\",\"3\"]', '1', '2023-04-23', '2019-10-18 17:34:13', '2019-12-12 06:20:05', '2019-12-12 06:20:05'),
(2, 'mon-tritre-deux', 'Mon Tritre deux edit', '<p>Mon Tritre deuxMon Tritre deuxMon Tritre deuxMon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux</p>\r\n<p>Mon Tritre deuxMon Tritre deuxMon Tritre deuxMon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux</p>\r\n<p>Mon Tritre deuxMon Tritre deuxMon Tritre deuxMon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux&nbsp;Mon Tritre deux</p>', '1', 'text', 'HKsPHT51.jpg', NULL, '[\"1\",\"4\"]', '1', '2023-04-23', '2019-10-18 18:27:46', '2019-12-12 06:20:05', '2019-12-12 06:20:05'),
(3, 'live-43-nouvelles-version-de-grafikartfr-questions-diverses', 'Live #43 : Nouvelles version de Grafikart.fr & questions diverses', '<p>Je n\'ai pas encore le sujet ;)</p>', '1', 'video', NULL, 'https://www.youtube.com/watch?v=ZoFZl4R-ANI', '[\"3\"]', '1', '2023-04-23', '2019-10-18 19:36:50', '2019-12-12 06:20:05', '2019-12-12 06:20:05'),
(4, 'the-fed-just-printed-more-money-than-bitcoins-entire-market-cap', 'The Fed Just Printed More Money Than Bitcoin’s Entire Market Cap', '<p dir=\"ltr\">Bitcoin (<a href=\"https://cointelegraph.com/bitcoin-price-index\">BTC</a>) proponents are voicing fresh alarm after the United States&nbsp;<a href=\"https://cointelegraph.com/tags/federal-reserve\">Federal Reserve</a>&nbsp;printed more than its entire market cap in new money this month.</p>\r\n<h2>Fed balance sheet approaches $4T</h2>\r\n<p>As noted by cryptocurrency social media pundit&nbsp;<a href=\"https://twitter.com/Xentagz/status/1186180739206991872\" target=\"_blank\" rel=\"noopener nofollow\">Dennis Parker</a>&nbsp;on Oct. 21, since mid-September, the Fed has injected $210 billion into the economy.&nbsp;</p>\r\n<p>Part of its newly-revitalized quantitative easing (QE) strategy, the move dwarfs the total market cap of Bitcoin, which stands at $148 billion.&nbsp;</p>\r\n<p>QE refers to the buying up of government bonds in order to provide economic stimulus. The Fed&rsquo;s balance sheet, Parker notes, jumped from $3.77 trillion last month to $3.97 trillion. It had previously been higher, while the Fed&rsquo;s own projections call for a balance sheet worth&nbsp;<a href=\"https://www.reuters.com/article/us-usa-fed-balancesheet/feds-balance-sheet-could-end-up-higher-than-4-trillion-projections-idUSKCN1VO21U\" target=\"_blank\" rel=\"noopener nofollow\" data-amp=\"https://mobile-reuters-com.cdn.ampproject.org/c/s/mobile.reuters.com/article/amp/idUSKCN1VO21U\">$4.7 trillion</a>&nbsp;by 2025.&nbsp;</p>\r\n<h2>World &ldquo;sleepwalking&rdquo; into the next financial crisis</h2>\r\n<p>For holders of assets that cannot have their supply inflated, such as&nbsp;<a href=\"https://cointelegraph.com/tags/gold\">gold</a>&nbsp;and Bitcoin, money printing has regularly sparked calls to decrease reliance on&nbsp;<a href=\"https://cointelegraph.com/tags/fiat-money\">fiat currency</a>.&nbsp;</p>\r\n<p>Parker&rsquo;s suggestion that investors should buy BTC now came amid warnings from even the fiat establishment itself about the ailing health of the banking system.&nbsp;</p>\r\n<p>In a&nbsp;<a href=\"https://www.theguardian.com/business/2019/oct/20/world-sleepwalking-to-another-financial-crisis-says-mervyn-king\" target=\"_blank\" rel=\"noopener nofollow\" data-amp=\"https://amp-theguardian-com.cdn.ampproject.org/c/s/amp.theguardian.com/business/2019/oct/20/world-sleepwalking-to-another-financial-crisis-says-mervyn-king\">speech</a>&nbsp;at the International Monetary Fund&rsquo;s general meeting last week, former Bank of England governor Mervyn King told attendees the world was &ldquo;sleepwalking&rdquo; into a financial crisis even worse than that of 2008.</p>\r\n<p>&ldquo;By sticking to the new orthodoxy of monetary policy and pretending that we have made the banking system safe, we are sleepwalking towards that crisis,&rdquo; he summarized.</p>\r\n<p>The concept that interventionist economic practices on the part of governments and central banks leads to financial destruction forms one of the&nbsp;<a href=\"https://cointelegraph.com/news/ex-cftc-chairman-us-must-create-an-independent-blockchain-dollar\">central tenets</a>&nbsp;of Saifedean Ammous&rsquo; &ldquo;The Bitcoin Standard.&rdquo;&nbsp;</p>\r\n<p>Released in March 2018, the book focuses on Bitcoin as it compares to fiat currency and commodities such as gold.&nbsp;</p>\r\n<p>As Cointelegraph&nbsp;<a href=\"https://cointelegraph.com/news/bitcoin-is-already-at-40-of-average-fiat-currency-lifespan-10-years\">noted</a>, at ten years old, Bitcoin has now lasted 40% of the average fiat currency&rsquo;s lifespan.</p>', '1', 'text', 'IsqLwI0D.jpg', NULL, '[\"1\",\"4\"]', '1', '2023-04-23', '2019-10-21 08:55:01', '2019-12-12 06:20:05', '2019-12-12 06:20:05'),
(5, 'joel-robuchon-le-chef-le-plus-etoile-du-monde', 'JOËL ROBUCHON LE CHEF LE PLUS ÉTOILÉ DU MONDE !', '<p>Nomm&eacute; &laquo;&nbsp;cuisinier du si&egrave;cle&nbsp;&raquo; par le Gault et Millau en 1990, Jo&euml;l Robuchon est &agrave; ce jour le chef qui d&eacute;tient le plus d&rsquo;&eacute;toiles au monde au Guide Michelin. &Agrave; la t&ecirc;te de nombreux restaurants en France et &agrave; l&rsquo;&eacute;tranger,&nbsp; il est sans conteste(1),&nbsp; l&rsquo;un des meilleurs repr&eacute;sentants de la cuisine fran&ccedil;aise &agrave; travers le monde.</p>\r\n<p>&Agrave; ses d&eacute;buts, rien ne pr&eacute;destinait(2) le jeune homme &agrave; cette carri&egrave;re extraordinaire. Fils de ma&ccedil;on, ce Poitevin(3) n&eacute; &agrave; la fin de la Seconde guerre mondiale, pense d&rsquo;abord consacrer sa vie &agrave; Dieu. Il d&eacute;couvre la cuisine aupr&egrave;s des religieuses. Ayant besoin de trouver un travail pour vivre, il s&rsquo;oriente finalement vers cette discipline qui le d&eacute;tend et le passionne. Embauch&eacute; comme apprenti de cuisine au Relais de Poitiers &agrave; quinze ans, il devient en 1966 Compagnon du Tour de France. L&rsquo;occasion pour lui d&rsquo;am&eacute;liorer sa technique aupr&egrave;s des plus grands. Il parvient ainsi &agrave; devenir chef de cuisine &agrave; l&rsquo;H&ocirc;tel Concorde Lafayette de Paris, alors qu&rsquo;il n&rsquo;a que vingt-neuf ans. Deux ans plus tard, en 1976, il d&eacute;croche le titre tr&egrave;s convoit&eacute;(4) de &laquo;&nbsp;Meilleur ouvrier de France&nbsp;&raquo;. Sa carri&egrave;re est d&eacute;sormais lanc&eacute;e&nbsp;!<br />Apr&egrave;s avoir dirig&eacute; le restaurant de l&rsquo;H&ocirc;tel Nikko &agrave; Paris, il ouvre son premier restaurant en 1981, Le Jamin, qui lui donne, d&egrave;s sa premi&egrave;re ann&eacute;e d&rsquo;existence, une &eacute;toile au Michelin. Une &eacute;toile qui se transforme en deux puis en trois, les deux ann&eacute;es suivantes. Du jamais vu(5) dans l&rsquo;histoire de la Gastronomie&nbsp;! C&rsquo;est s&ucirc;r, l&rsquo;homme a un don(6). Sans m&ecirc;me avoir besoin d&rsquo;&eacute;crire une recette, il s&eacute;duit les gourmets les plus exigeants en travaillant avec des produits de qualit&eacute; qu&rsquo;il arrive &agrave; transcender(7), comme sa c&eacute;l&egrave;bre pur&eacute;e de pomme de terre truff&eacute;e, son bar (poisson) po&ecirc;l&eacute; &agrave; la citronnelle ou sa mythique gel&eacute;e de caviar &agrave; la cr&egrave;me de chou-fleur.</p>\r\n<p><strong>Une cuisine ouverte au grand public</strong></p>\r\n<p>Souhaitant faire profiter ses talents au plus grand nombre, il accepte, &agrave; partir de 1987, de devenir conseiller technique chez un grand groupe industriel&nbsp;: Fleury Michon. Un d&eacute;fi plus difficile &agrave; relever qu&rsquo;on ne le croit. &laquo;&nbsp;C&rsquo;est beaucoup plus dur pour moi de faire un plat pour Fleury Michon que dans un trois-&eacute;toiles&nbsp;&raquo; d&eacute;clarait-il r&eacute;cemment dans une interview, mais c&rsquo;est aussi une immense satisfaction car cela lui permet de toucher le grand public, en d&eacute;mocratisant(8) son art. Meilleur exemple de ce succ&egrave;s populaire&nbsp;: son fameux parmentier de canard qui&nbsp; atteint les 20 000 portions par semaine&nbsp;!</p>\r\n<p>Apr&egrave;s un passage &agrave; Tokyo o&ugrave; il cuisine au Ch&acirc;teau Restaurant Taillevent-Robuchon, il d&eacute;cide en 1994 d&rsquo;ouvrir un nouveau restaurant &agrave; Paris qui est imm&eacute;diatement d&eacute;sign&eacute; par l&rsquo;International Herald Tribune comme le &laquo;&nbsp;Meilleur restaurant du monde&nbsp;&raquo;. Rien que &ccedil;a&nbsp;!</p>\r\n<p><strong>Un infatigable cr&eacute;ateur de nouveaux concepts</strong></p>\r\n<p>Toujours en d&eacute;placement pour contr&ocirc;ler et assurer la r&eacute;putation de ses diff&eacute;rents restaurants &agrave; travers le monde, Jo&euml;l Robuchon cr&eacute;e au d&eacute;but des ann&eacute;es 2000 un nouveau concept de restaurants&nbsp;: L&rsquo;Atelier.<br />Sur une dominante de noir et de rouge, la cuisine y est ouverte sur la salle. Limit&eacute; &agrave; une trentaine de places, le lieu permet aux clients de voir les plats qui se pr&eacute;parent, ce qui donne un caract&egrave;re tr&egrave;s convivial(9) &agrave; l&rsquo;endroit. Devant le succ&egrave;s, Jo&euml;l Robuchon d&eacute;cline rapidement le concept sur tous les continents en s&rsquo;adaptant &agrave; chaque fois aux traditions culinaires de chaque pays o&ugrave; il s&rsquo;installe. En 2004, il propose une nouvelle id&eacute;e&nbsp;:&nbsp;la Table de Jo&euml;l Robuchon qui offre ses grands classiques en petites ou grandes portions. Jamais &agrave; court d&rsquo;id&eacute;es, il ouvre aussi de nouveaux restaurants gastronomiques trois-&eacute;toiles dans un ch&acirc;teau &agrave; la fran&ccedil;aise en plein c&oelig;ur de Tokyo, &agrave; Las Vegas, au MGM Grand, &agrave; Londres, Hong Kong, Nagoya,<br />Monaco, Taipei&hellip;</p>\r\n<p>Les prochaines &eacute;tapes&nbsp;: Bangkok, New York, Mumba&iuml; et Bordeaux&hellip; Assur&eacute;ment, la cuisine du chef fran&ccedil;ais n&rsquo;a pas fini de conqu&eacute;rir le monde&nbsp;!</p>', '1', 'text', '6p49DkWq.jpg', NULL, '[\"5\"]', '3', '2023-04-23', '2019-10-22 15:10:14', '2019-12-12 06:20:05', '2019-12-12 06:20:05'),
(6, 'voici-pourquoi-nos-collegiens-ont-progresse', 'Voici pourquoi nos collégiens ont progressé', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'h1u3nI9h.jpg', NULL, '[\"1\"]', '1', '2023-04-23', '2019-12-12 06:30:59', '2019-12-12 06:30:59', NULL),
(7, 'toulepleu-un-gyneco-pas-comme-les-autres', 'Toulepleu Un gynéco pas comme les autres', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'h41aCIFQ.jpg', NULL, '[\"2\"]', '1', '2023-04-23', '2019-12-12 06:31:57', '2019-12-12 06:31:57', NULL),
(8, '4-pistes-pour-augmenter-le-pouvoir-dachat-des-ivoiriens', '4 pistes pour augmenter le pouvoir d’achat des Ivoiriens.', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'UZADDrHR.jpg', NULL, '[\"3\"]', '1', '2023-04-23', '2019-12-12 06:32:45', '2019-12-12 06:32:45', NULL),
(9, 'abidjan-la-nuit-comment-la-police-veille-sur-nous', 'Abidjan la nuit : comment la police veille sur nous.', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'Yi5x2ScF.jpg', NULL, '[\"4\"]', '1', '2023-04-23', '2019-12-12 06:33:32', '2019-12-12 06:33:32', NULL),
(10, 'unite-nationale-ce-que-les-chefs-coutumiers-ont-a-nous-apprendre', 'Unité nationale Ce que les chefs coutumiers ont à nous apprendre', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'y97BWTZj.jpg', NULL, '[\"5\"]', '1', '2023-04-23', '2019-12-12 06:34:17', '2019-12-12 06:34:17', NULL),
(11, 'il-ny-a-pas-de-petits-metiers', 'Il n’y a pas de « petits » métiers.', '<pre>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis magni vitae sit inventore consequuntur voluptate</pre>', '1', 'text', 'hzYVR1Lf.jpg', NULL, '[\"6\"]', 'Ourega Fabien', '2023-04-23', '2019-12-12 06:35:01', '2023-04-09 23:56:46', NULL),
(12, 'learn-webs-applications-development-from-experts', 'Learn Webs Applications Development from Experts', '<p>When working on a new piece of software, it&rsquo;s essential to craft a software design document to create a clear and precise vision of the client&rsquo;s problem and your proposed solution. Software design documents are important for detailing expectations between the software engineer and the client. They help to streamline the coding process before any code is written.</p>\r\n<p>Read on to learn how to write great software design documents that improve communication between you and your client, ensuring that everyone is on the same page when working on a project together.</p>\r\n<h4>What Is a Software Design Document?</h4>\r\n<p>Before starting a new project with the client, the planning stage involves having a clear vision and agreeing upon design goals. These goals should be laid out in a technical specification document called a software design document.</p>\r\n<p>A software developer is usually responsible for creating the software design document. The developer will gather information from a client who wants a new piece of software built and then they will make a document that details the proposed solutions to the client\'s problems.</p>\r\n<h4>What to Include in Your Software Design Document</h4>\r\n<p>Problems within the coding world tend to vary, and engineering teams and developers often write design documents differently. However, there is a certain software design document template that you can follow to help you include all of the essential pieces of information.</p>\r\n<p>Here are some factors you should include:</p>\r\n<p><strong>Key information:</strong>&nbsp;The title of the document, who&rsquo;s working on the project, and the date the document was last updated.</p>\r\n<p><strong>Overview:</strong>&nbsp;A complete high-level summary of the project that helps the company decide if they should continue reading the rest of the document.</p>\r\n<p><strong>Pain points:</strong>&nbsp;A complete description of the problems and challenges, alongside why this project is important. Furthermore, it&rsquo;s essential to include how it fits in with product and technical strategies.</p>\r\n<p><strong>Goals:</strong>&nbsp;Accurately describe the users of the project and their impact. In certain cases, the users may be another engineering team. It&rsquo;s also important to be clear on how to measure the success of metrics in conjunction with goals and which KPIs will be used to track success. Lastly, it&rsquo;s essential to state which problems you won&rsquo;t be fixing so that everyone who has read the document understands what you will and will not be working on.</p>\r\n<p><strong>Project milestones:</strong>&nbsp;Milestones in any project serve as a list of measurable checkpoints, helping to break the entire project down into small parts. A timeline and milestones can be used internally to help keep engineers on track and show the client how the project will progress towards completion.</p>\r\n<p><strong>Prioritization:</strong>&nbsp;After breaking the project down into smaller features, it&rsquo;s good to rank them in order of priority. The simplest way to do this is to plot each feature in the project onto a prioritization matrix based on urgency and impact.</p>\r\n<p><strong>Solutions:</strong>&nbsp;Detail the current and proposed solutions to the client&rsquo;s problem. In this section, you&rsquo;ll want to include what the existing solution is (if any) and how users interact with that solution. The second part of this section is to outline your proposed solution in as much detail as possible. It must be easy to understand&mdash;another engineer should be able to read and build your proposed solution without any prior knowledge of the project.</p>\r\n<h4>How to Create a Software Design Document</h4>\r\n<p>Keeping the above criteria in mind when creating your software design document is a great start. To really maximize efficiency and communication, there are a few best practices to implement.</p>\r\n<p>Firstly, keep your language as simple as possible. The key is clarity &mdash; especially when it comes to detailing technical points. Include visuals into your document, helping readers accurately understand the points and data you&rsquo;re trying to convey. Diagrams, charts, and other timelines are a great way to communicate information.</p>\r\n<p>Send a draft of your document to the client, so they can catch parts you may have missed or areas that are unclear and need fleshing out. Lastly, it&rsquo;s important to update your document as the project progresses, as you and other team members should be consistently referencing the document.</p>\r\n<p>When working on a new piece of software, it&rsquo;s essential to craft a software design document to create a clear and precise vision of the client&rsquo;s problem and your proposed solution. Software design documents are important for detailing expectations between the software engineer and the client. They help to streamline the coding process before any code is written.</p>\r\n<p class=\"mb-0\">Read on to learn how to write great software design documents that improve communication between you and your client, ensuring that everyone is on the same page when working on a project together.</p>', '1', 'text', 'DSY08E7G.png', NULL, NULL, 'Ruby Perrin', '2023-04-14', '2023-04-23 18:42:49', '2023-04-23 19:02:16', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `actualites_categories`
--

CREATE TABLE `actualites_categories` (
  `id` int(6) NOT NULL,
  `cat_id` int(6) NOT NULL,
  `actu_id` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `actualites_categories`
--

INSERT INTO `actualites_categories` (`id`, `cat_id`, `actu_id`) VALUES
(2, 1, 11),
(3, 6, 11),
(6, 2, 12),
(7, 6, 12);

-- --------------------------------------------------------

--
-- Structure de la table `albums`
--

CREATE TABLE `albums` (
  `id` int(6) NOT NULL,
  `title` varchar(225) NOT NULL,
  `slug` varchar(225) NOT NULL,
  `cover` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `albums`
--

INSERT INTO `albums` (`id`, `title`, `slug`, `cover`, `created_at`, `updated_at`) VALUES
(1, 'Naruto&Boruto', 'narutoboruto', 'JdaijdaD.png', '2019-10-25 15:30:37', '2019-10-25 16:25:02');

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(6) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `libelle` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `slug`, `libelle`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'education', 'ÉDUCATION', 'EDUCATION', '2019-10-18 14:35:05', '2019-12-12 06:26:15', NULL),
(2, 'sante', 'SANTÉ', 'SANTE', '2019-10-18 14:35:05', '2019-12-12 06:25:50', NULL),
(3, 'pourvoir-dachat', 'POURVOIR D\'ACHAT', 'POURVOIR D\'ACHAT', '2019-10-18 14:36:04', '2019-12-12 06:24:36', NULL),
(4, 'securite', 'SÉCURITÉ', 'SÉCURITÉ', '2019-10-18 14:36:04', '2019-12-12 06:25:05', NULL),
(5, 'paix-et-unite-nationale', 'PAIX ET UNITÉ NATIONALE', 'PAIX ET UNITÉ NATIONALE', '2019-10-21 12:19:40', '2019-12-12 06:27:03', NULL),
(6, 'emploi', 'EMPLOI', 'EMPLOI', '2019-12-12 06:27:57', '2019-12-12 06:27:57', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `catformations`
--

CREATE TABLE `catformations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statut` tinyint(4) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `cat_formats`
--

CREATE TABLE `cat_formats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `catformation_id` bigint(20) UNSIGNED NOT NULL,
  `formation_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `criteres`
--

CREATE TABLE `criteres` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valeur` double NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `etps`
--

CREATE TABLE `etps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `etp` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `formations`
--

CREATE TABLE `formations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `titre` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `niveau` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prerequis` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lien` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salle` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prix` double DEFAULT NULL,
  `prixpromo` double DEFAULT NULL,
  `img` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motscles` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `statut` tinyint(4) NOT NULL DEFAULT 0,
  `debut` date DEFAULT NULL,
  `duree` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typedelai` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datecloture` date DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `professeur_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `etp_id` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `formation_demandes`
--

CREATE TABLE `formation_demandes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `etudiant_id` bigint(20) UNSIGNED NOT NULL,
  `formation_id` bigint(20) UNSIGNED NOT NULL,
  `statut` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `login_activities`
--

CREATE TABLE `login_activities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_at` datetime NOT NULL,
  `logout_at` datetime DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2023_04_02_072805_adds_last_loginfos_to_users_table', 2),
(4, '2023_04_02_072859_create_login_activities_table', 2),
(5, '2023_04_02_154828_adds_status_to_users_table', 2),
(6, '2023_04_05_035133_create_etps_table', 2),
(7, '2023_04_06_033822_create_criteres_table', 2),
(8, '2023_04_06_033934_create_formations_table', 2),
(9, '2023_04_06_034134_create_formation_demandes_table', 2),
(10, '2023_04_23_050948_create_catformations_table', 2),
(11, '2023_04_23_214153_create_cat_formats_table', 2);

-- --------------------------------------------------------

--
-- Structure de la table `partenaires`
--

CREATE TABLE `partenaires` (
  `id` int(11) NOT NULL,
  `titre` varchar(225) NOT NULL,
  `img` varchar(225) NOT NULL,
  `lien` varchar(225) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `partenaires`
--

INSERT INTO `partenaires` (`id`, `titre`, `img`, `lien`, `created_at`, `updated_at`) VALUES
(4, 'Kayss', '090945banniereADS.png', 'https://www.google.fr/', '2023-04-24 09:09:45', '2023-04-24 09:16:10');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

CREATE TABLE `photos` (
  `id` int(6) NOT NULL,
  `img` varchar(225) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `album_id` int(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `photos`
--

INSERT INTO `photos` (`id`, `img`, `status`, `album_id`, `created_at`, `updated_at`) VALUES
(1, 'lFEb3vIo.jpg', '1', 1, '2019-10-24 10:37:45', '2019-10-24 11:53:49'),
(2, 'TBwtf5Q0.jpg', '1', 1, '2019-10-24 10:40:09', '2019-10-24 11:53:49');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(6) NOT NULL,
  `libelle` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `libelle`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Administrateur', '2019-10-21 15:56:23', '2019-10-21 15:56:23', NULL),
(2, 'Editeur', '2019-10-21 15:56:23', '2019-10-21 15:56:23', NULL),
(3, 'Auteur', '2019-10-21 15:56:55', '2019-10-22 09:45:55', NULL),
(4, 'Superviseur', '2019-10-21 16:59:27', '2019-10-22 09:43:24', '2019-10-22 09:43:24');

-- --------------------------------------------------------

--
-- Structure de la table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(6) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `fonction` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `contenu` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `testimonials`
--

INSERT INTO `testimonials` (`id`, `nom`, `fonction`, `photo`, `contenu`, `created_at`, `updated_at`) VALUES
(7, 'Signo Joseph K.', 'Lauréat du prix PEE de l\'innovation 2022', 'gmzHBmEn.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '2023-04-24 19:52:44', '2023-04-24 19:52:44'),
(8, 'Silvain P. N\'GORAN', 'Fondateur de la technologie Awesomeux', 'SXKo0d09.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '2023-04-24 19:53:18', '2023-04-24 19:53:18'),
(9, 'Achille Gaccia', 'Créateur de la machine à café intéligeante', 'FzAGEeCD.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '2023-04-24 19:53:52', '2023-04-24 19:53:52');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexe` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(6) NOT NULL,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `last_logout_at` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1:Actif, 0:Inactif'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `sexe`, `email_verified_at`, `password`, `remember_token`, `role_id`, `img`, `created_at`, `updated_at`, `deleted_at`, `last_login_ip`, `last_login_at`, `last_logout_at`, `status`) VALUES
(1, 'Ourega Fabien', 'test@test.com', 'Homme', NULL, '$2y$10$UNLTWg.GQ1TPQhkHZBOVS.WFk6Mud3RPYtVtVxWlFzydgi4PzoPUy', NULL, 1, 'ba4qBAJ0.jpeg', '2019-10-17 17:52:35', '2019-10-22 14:27:01', NULL, NULL, NULL, NULL, 1),
(2, 'Editeur', 'edit@edit.com', NULL, NULL, '$2y$10$PPkw7efBEQEU/vIdny2WO.BJAhNbD7uF.YYokfqhACBdRw6HQoIi6', NULL, 2, NULL, '2019-10-21 16:25:39', '2019-10-22 09:54:42', NULL, NULL, NULL, NULL, 1),
(3, 'Auteur', 'use@use.com', NULL, NULL, '$2y$10$uMapB3uBpC5ft1XLj/GrouKHZ2IYc9daGwY0kwLjgLfWl4oNPmTqy', NULL, 3, NULL, '2019-10-21 16:50:17', '2019-10-22 09:54:42', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `videos`
--

CREATE TABLE `videos` (
  `id` int(6) NOT NULL,
  `title` varchar(225) NOT NULL,
  `img` varchar(225) NOT NULL,
  `lien` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `videos`
--

INSERT INTO `videos` (`id`, `title`, `img`, `lien`, `created_at`, `updated_at`) VALUES
(1, 'NARUTO | LA VRAIE PUISSANCE DE MINATO ET SES TECHNIQUES EXPLIQUÉES !', 'https://i.ytimg.com/vi/YW34jyl5Ufw/hqdefault.jpg', 'https://www.youtube.com/watch?v=YW34jyl5Ufw', '2019-10-24 15:04:27', '2019-10-24 15:04:27'),
(2, 'How does a Tank work? (M1A2 Abrams)', 'https://i.ytimg.com/vi/SdL55HWNPRM/hqdefault.jpg', 'https://www.youtube.com/watch?v=SdL55HWNPRM', '2023-01-26 17:54:20', '2023-01-26 17:54:20');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `actualites`
--
ALTER TABLE `actualites`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `actualites_categories`
--
ALTER TABLE `actualites_categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `catformations`
--
ALTER TABLE `catformations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `cat_formats`
--
ALTER TABLE `cat_formats`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `criteres`
--
ALTER TABLE `criteres`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `etps`
--
ALTER TABLE `etps`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `formations`
--
ALTER TABLE `formations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `formation_demandes`
--
ALTER TABLE `formation_demandes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `login_activities`
--
ALTER TABLE `login_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `login_activities_user_id_index` (`user_id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `partenaires`
--
ALTER TABLE `partenaires`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Index pour la table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `actualites`
--
ALTER TABLE `actualites`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `actualites_categories`
--
ALTER TABLE `actualites_categories`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `catformations`
--
ALTER TABLE `catformations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `cat_formats`
--
ALTER TABLE `cat_formats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `criteres`
--
ALTER TABLE `criteres`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `etps`
--
ALTER TABLE `etps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `formations`
--
ALTER TABLE `formations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `formation_demandes`
--
ALTER TABLE `formation_demandes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `login_activities`
--
ALTER TABLE `login_activities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `partenaires`
--
ALTER TABLE `partenaires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
